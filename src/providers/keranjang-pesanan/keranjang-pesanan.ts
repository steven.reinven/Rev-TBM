import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the KeranjangPesananProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class KeranjangPesananProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello KeranjangPesananProvider Provider');
  }

  public showms_product_cart(){
    return new Promise((resolve, reject) => {
    this.storage=new SQLite;
    this.storage.create({
        name:"data.db",
        location:"default"
    }).then((storage:SQLiteObject)=>{
        storage.executeSql("SELECT * FROM ms_product_cart", []).then((data)=>{
            let msproduct=[];
            if(data.rows.length > 0) {
                for(let i = 0; i < data.rows.length; i++) {
                    msproduct.push({
                        id:      data.rows.item(i).id,
                        jsonnya:      data.rows.item(i).jsonnya,
                    });
                }
            }resolve(msproduct);
            console.log("di provider keranjang-pesanan.ts Table ms_product_cart opened");
            },(error)=>{
                reject(error);
                console.log("di provider keranjang-pesanan.ts Unable to open database ms_product_cart");
            }
        )});
    });
  }

  public putms_product_cart(id,jsonnya){
      return new Promise((resolve, reject) => {
          this.storage = new SQLite;
          this.storage.create({
              name: "data.db",
              location: "default"
          }).then((storage: SQLiteObject) => {
              storage.executeSql("INSERT OR REPLACE INTO ms_product_cart (id,jsonnya) VALUES (?,?)", [id,jsonnya]).then((data) => {
                  resolve(data);
                  console.log("di provider keranjang-pesanan.ts new cart has been post into ms_product_cart");
              }, (error) => {
                  reject(error);
                  console.log("di provider keranjang-pesanan.ts Unable to insert into database ms_product_cart");
              }
              )
          });
      });
  }

}
