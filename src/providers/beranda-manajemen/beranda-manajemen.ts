import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
/*
  Generated class for the BerandaManajemenProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BerandaManajemenProvider {

  private storage:SQLite;
  // public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello BerandaManajemenProvider Provider');
  }
  
    // berandaManajemenData(){
    //   if(!this.isOpen){
    //     this.storage = new SQLite();
    //     this.storage.create({
    //       name: "data.db",
    //       location: "default"
    //     }).then((storage: SQLiteObject)=> {
    //       storage.executeSql("CREATE TABLE IF NOT EXISTS beranda_manajemen (dataid INTEGER PRIMARY KEY AUTOINCREMENT, recordpemesanan TEXT);", {}).then((data) => {
    //         console.log("Table beranda_manajemen has been created: ", data);
    //         this.isOpen = true;
    //       }, (error)=> {
    //         console.log("Unable to execute sql", error);
    //       })
    //     }, (error)=> {
    //       console.log("Unable to open database", error)
    //     });
    //   }
    // }

    
    public putberanda_manajemen(dataid, recordpemesanan){
      // this.berandaManajemenData();
      return new Promise((resolve, reject)=> {
        this.storage = new SQLite;
        this.storage.create({
          name: "data.db",
          location: "default"
        }).then((storage: SQLiteObject)=> {
          storage.executeSql("INSERT OR REPLACE INTO beranda_manajemen(dataid, recordpemesanan) VALUES(?, ?)", [dataid, recordpemesanan]). then((data) => {
            resolve(data);
            console.log("new product has been post into beranda_manajemen");

            //show data
              // this.showberanda_manajemen();
            //
          }, (error)=> {
            reject(error);
            console.log("Unable to insert into database beranda_manahemen");
          });
        });
      })
    }

        public showberanda_manajemen(){
          return new Promise((resolve, reject) => {
            this.storage = new SQLite;
            this.storage.create({
              name: "data.db",
              location: "default"
            }).then((storage:SQLiteObject)=>{
              storage.executeSql("SELECT * FROM beranda_manajemen", []).then((data)=>{
                let berandamanajemen=[];
                if(data.rows.length > 0){
                  for(let i=0; i<data.rows.length; i++){
                    berandamanajemen.push({
                      dataid:             data.rows.item(i).dataid,
                      recordpemesanan:    data.rows.item(i).recordpemesanan
                    });
                  }
                }resolve(berandamanajemen);
                console.log("Table beranda_manajemen opened");
                console.log("===bendara_manajemen===", JSON.stringify (berandamanajemen));
              },(error)=>{
                reject(error);
                console.log("Unable to open database beranda_manajemen");
              });
            });
          });
        }

}
