import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the PesananUserListProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PesananUserListProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello PesananUserListProvider Provider');
  }

  public showms_pesanan_list_list(id){
    return new Promise((resolve, reject) => {
    this.storage=new SQLite;
    this.storage.create({
        name:"data.db",
        location:"default"
    }).then((storage:SQLiteObject)=>{
        storage.executeSql("SELECT * FROM ms_pesanan_list where id == "+id+" ", []).then((data)=>{
            let msproduct=[];
            if(data.rows.length > 0) {
                for(let i = 0; i < data.rows.length; i++) {
                    msproduct.push({
                        id:      data.rows.item(i).id,
                        jsonnya:      data.rows.item(i).jsonnya,
                    });
                }
            }resolve(msproduct);
            console.log("Table ms_pesanan_list opened");
            },(error)=>{
                reject(error);
                console.log("Unable to open database ms_pesanan_list");
            }
        )});
    });
  }

  public putms_pesanan_list(id,jsonnya){
      return new Promise((resolve, reject) => {
          this.storage = new SQLite;
          this.storage.create({
              name: "data.db",
              location: "default"
          }).then((storage: SQLiteObject) => {
              storage.executeSql("INSERT OR REPLACE INTO ms_pesanan_list (id,jsonnya) VALUES (?,?)", [id,jsonnya]).then((data) => {
                  resolve(data);
                  console.log("di provider pesanan-user-list.ts new pesanan_list has been post into ms_pesanan_list");
              }, (error) => {
                  reject(error);
                  console.log("di provider pesanan-user-list.ts Unable to insert into database ms_pesanan_list");
              }
              )
          });
      });
  }

}
