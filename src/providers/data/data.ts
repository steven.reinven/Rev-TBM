import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { LocalNotifications } from '@ionic-native/local-notifications';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DataProvider {

  //notif di user 
  notif: number;

  //sqlite
  dataptTBMdaridatabase;
  tampungdataptTBMdaridatabase;

  ptTBMdaridatabase;
  formatJsonptTBMdaridatabase=[];

  orderptTBMdaridatabase;
  orderformatJsonptTBMdaridatabase=[];

  produkpesananptTBMdaridatabase;
  produkpesananformatJsonptTBMdaridatabase=[];


  distributordaridatabase;
  formatJsondistributor=[];

  datadistributordaridatabase;
  tampungdatadistributordaridatabase;


  datalogindaridatabase;
  tampungdatalogindaridatabase;
  TOKEN;

  datapesananpttbmdariapi;
  tampungdatapesananpttbmdariapi;
  tampungf5datapesananpttbmdariapi;
  tampungformatJsontampungf5datapesananpttbmdariapi=[];
  formatJsontampungf5datapesananpttbmdariapi=[];

  //sqlite


//====DATA PEMESANAN PRODUK UNTUK USER====
  pesanan=[
    // {
    //   "nama": "produk1",
    //   "jumlah": 2
    // },

    // {
    //   "nama": "produk2",
    //   "jumlah": 3
    // }
  ]
// =======================================

//====JSON UNTUK MENU PEMESANAN USER====
  pesananMenu=[
    {
      "icon": "ios-clock",
      "color": "buttonPesan",
      "name": "Pesanan dalam proses",
      "jumlah": 0
    },
    {
      "icon": "md-close",
      "color": "danger",
      "name": "Pesanan ditolak",
      "jumlah": 0
    },
    {
      "icon": "ios-paper-plane",
      "color": "secondary",
      "name": "Pesanan dikirim",
      "jumlah": 0
    },
    {
      "icon": "md-done-all",
      "color": "primary",
      "name": "Pesanan terkirim",
      "jumlah": 0
    }
  ]
//======================================

//====INI ISI DARI MENU PESANAN USER====

  //=====ISI DARI MENU PESANAN TERKIRIM====
    pesananTerkirim=[
      {
        "icon": "md-done-all",
        "color": "primary",
        "SO": [
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 21},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 33}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 16},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 37}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 71},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 13}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          }
        ]
      }
    ]
  //=======================================

  //====ISI DARI MENU PESANAN DIKIRIM====
    pesananDikirim=[
      {
        "icon": "ios-paper-plane",
        "color": "secondary",
        "SO": [
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 12},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 32}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ002",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 11},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 32}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ003",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 11},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 23}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ004",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 13},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 43}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          }
        ]
      }
    ]
  //=====================================

  //====ISI DARI MENU PESANAN DALAM PROSES====
    pesananDalamProses=[
      {
        "icon": "ios-clock",
        "color": "buttonPesan",
        "SO": [
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 5},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 5},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 15}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          },
          {
            "soCode": "SO-1707-PJ003",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 5},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 15}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          }
        ]
      }
    ]
  //==========================================

  //====ISI DARI MENU PESANAN DITOLAK====
    pesananDitolak=[
      {
        "icon": "md-close",
        "color": "danger",
        "SO": [
          {
            "soCode": "SO-1707-PJ001",
            "detail": [
              {"namaProduk": "BENSIDA 70WP", "jumlah": 1},
              {"namaProduk": "BENSIDA 70WP", "jumlah": 3}
            ],
            "catatan": "Lorem ipsum is simply dummy text of the printing"
          }
        ]
      }
    ]
  //=====================================

//======================================//

//========================================DATA BERANDA MANAJEMEN===========================================//
  
  // dataManajemen =
  // [
  //   {
  //     "f1":[
  //       {"category":"Tahun ini","jumlah":"150"},
  //       {"category":"Bulan ini","jumlah":"50"},
  //       {"category":"Hari ini","jumlah":"5"}
  //     ],
  //     "f2":[
  //       {"produk":"Besvidor 25 WP - 100gr","total":"150 Box"},
  //       {"produk":"Besvidor 200 SL - 100ml","total":"125 Box"},
  //       {"produk":"Besgrimex 36 EC - 200ml","total":"100 Box"},
  //       {"produk":"Besromil 35 WP - 100gr","total":"90 Box"},
  //       {"produk":"Beshipo 500 SL - 200ml","total":"80 Box"}
  //     ],
  //     "f3":[
  //       {"pelanggan":"PT. ABC","total":"150 Box"},
  //       {"pelanggan":"PT. Jaya Prima","total":"125 Box"},
  //       {"pelanggan":"PT. Permai Nusantara","total":"100 Box"},
  //       {"pelanggan":"PT. Berjaya Satu","total":"90 Box"},
  //       {"pelanggan":"PT. Indah Permai Utama","total":"75 Box"}
  //     ]
  //   }
  // ]

  dataManajemen = 
  [{"f1":[{"category":"Tahun ini","jumlah":"5"},{"category":"Bulan ini","jumlah":"5"},{"category":"Hari ini","jumlah":"5"}],"f2":[{"produk":"","total":"25"},{"produk":"Besconil 80 WP - 500 gr","total":"20"},{"produk":"Besvidor 200 SL - 100 ml","total":"18"},{"produk":"Beshipo 500 SL - 1 Ltr","total":"15"},{"produk":"Besclaim 30 EC - 400 ml","total":"11"}],"f3":[{"pelanggan":"","total":"109"},{"pelanggan":"PT. ABC","total":"15"}]}]
  
//=========================================================================================================//



  ptTBM : any;//UNTUK TAMPUNG DATA JSON PTTBM
  distributor: any;//UNTUK TAMPUNG DATA JSON DISTRIBUTOR
  
  totalPemesanan: number=0;//
  temptTrigger: boolean=false;//trigger untuk cart
  notificationManajemenTransaksi: number;//UNTUK NOTIF SEGMENT MANAJEMEN TRANSAKSI
  notifTab: any;//UNTUK NOTIF PADA TAB

  constructor(public http: Http) {

    //====VARIABEL UNTUK HITUNG TOTAL BOX BERDASARKAN LENGTH UNTUK MENU PEMESANAN USER====
      this.pesananMenu[0].jumlah = this.pesananDalamProses[0].SO.length;
      this.pesananMenu[1].jumlah = this.pesananDitolak[0].SO.length;
      this.pesananMenu[2].jumlah = this.pesananDikirim[0].SO.length;
      this.pesananMenu[3].jumlah = this.pesananTerkirim[0].SO.length;
    //====================================================================================

    //====FLAG INDEX UNTUK MENAMPUNG DATA ptTBM====
      this.flagTempPesananDalamProses = 0;
      this.flagTempPesananDikirim = 0;
      this.flagTempPesananDitolak = 0;
      this.flagTempPesananTerkirim = 0;
    //=============================================
  
     

      this.distributor = [
        {
          "id": 1,
          "nama": "PT. XYZ",
          "tanggal": "30-Jul-2017",
          "status": "Pesanan dalam proses",
          "totalBox": 34,
          "order": [
              {
                "noOrder": "PO/1707/097",
                "produkPesanan": [
                  {"namaProduk": "Produk A", "jumlah": 12},
                  {"namaProduk": "Produk B", "jumlah": 17},
                  {"namaProduk": "Produk C", "jumlah": 5}
                ]
              }
            ],
            "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          },
          {
            "id": 2,
            "nama": "PT. Nusa Dua",
            "tanggal": "30-Jul-2017",
            "status": "Pesanan terkirim",
          "totalBox": 49,
            "order": [
              {
                "noOrder": "PO/1707/098",
                "produkPesanan": [
                  {"namaProduk": "Produk A", "jumlah": 19},
                  {"namaProduk": "Produk B", "jumlah": 30}
                ]
              }
            ],
            "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          },
          {
            "id": 3,
            "nama": "PT. Nusa Dua",
            "tanggal": "30-Jul-2017",
            "status": "Pesanan dikirim",
          "totalBox": 39,
            "order": [
              {
                "noOrder": "PO/1707/098",
                "produkPesanan": [
                  {"namaProduk": "Produk A", "jumlah": 19},
                  {"namaProduk": "Produk B", "jumlah": 20}
                ]
              }
            ],
            "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          },
          {
            "id": 4,
            "nama": "PT. Nusa Tiga",
            "tanggal": "30-Jul-2017",
            "status": "Pesanan ditolak",
            "totalBox": 29,
            "order": [
              {
                "noOrder": "PO/1707/098",
                "produkPesanan": [
                  {"namaProduk": "Produk A", "jumlah": 19},
                  {"namaProduk": "Produk B", "jumlah": 10}
                ]
              }
            ],
            "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          },
          {
          "id": 5,
          "nama": "PT. Prima",
          "tanggal": "30-Jul-2017",
          "status": "Pesanan dalam proses",
          "totalBox": 34,
          "order": [
              {
                "noOrder": "PO/1707/097",
                "produkPesanan": [
                  {"namaProduk": "Produk A", "jumlah": 12},
                  {"namaProduk": "Produk B", "jumlah": 17},
                  {"namaProduk": "Produk C", "jumlah": 5}
                ]
              }
            ],
            "catatan": "",
          }
      ]
    
    
    console.log("isi data pesanan dalam proses: ", this.tempPesananDalamProses);
//json local
    // data this.ptTBM sudah ganti dengan this.ptTBMdaridatbase;
    // this.ptTBM = [
        //   {
        //     "id": 1,
        //     "nama": "PT. ABC",
        //     "tanggal": "30-Jul-2017",
        //     "status": "Pesanan dikirim",
        //     "totalBox": 29,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/091",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 12},
        //           {"namaProduk": "Produk B", "jumlah": 12},
        //           {"namaProduk": "Produk C", "jumlah": 5}
        //         ]
        //       }
        //     ],
        //     "catatan": ""
        //   },
        //   {
        //     "id": 2,
        //     "nama": "PT. DEF",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan dalam proses",
        //     "totalBox": 18,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/092",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 1},
        //           {"namaProduk": "Produk B", "jumlah": 17}
        //         ]
        //       }
        //     ],
        //     "catatan": "",
        //   },
        //   {
        //     "id": 3,
        //     "nama": "PT. Persada",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan ditolak",
        //     "totalBox": 37,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/093",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 30},
        //           {"namaProduk": "Produk B", "jumlah": 7}
        //         ]
        //       }
        //     ],
        //     "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        //   },
        //   {
        //     "id": 4,
        //     "nama": "PT. DEF",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan terkirim",
        //     "totalBox": 17,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/094",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 10},
        //           {"namaProduk": "Produk B", "jumlah": 7}
        //         ]
        //       }
        //     ],
        //     "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        //   },
        //    {
        //     "id": 5,
        //     "nama": "PT. Ceria",
        //     "tanggal": "30-Jul-2017",
        //     "status": "Pesanan dikirim",
        //     "totalBox": 19,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/091",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 12},
        //           {"namaProduk": "Produk B", "jumlah": 2},
        //           {"namaProduk": "Produk C", "jumlah": 5}
        //         ]
        //       }
        //     ],
        //     "catatan": ""
        //   },
        //   {
        //     "id": 6,
        //     "nama": "PT. Genta Prima",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan dalam proses",
        //     "totalBox": 8,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/092",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 1},
        //           {"namaProduk": "Produk B", "jumlah": 7}
        //         ]
        //       }
        //     ],
        //     "catatan": "",
        //   },
        //   {
        //     "id": 7,
        //     "nama": "PT. Utama",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan ditolak",
        //     "totalBox": 17,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/093",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 10},
        //           {"namaProduk": "Produk B", "jumlah": 7}
        //         ]
        //       }
        //     ],
        //     "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        //   },
        //   {
        //     "id": 8,
        //     "nama": "PT. Bintang timur",
        //     "tanggal": "29-Jul-2017",
        //     "status": "Pesanan terkirim",
        //     "totalBox": 107,
        //     "order": [
        //       {
        //         "noOrder": "PO/1707/094",
        //         "produkPesanan": [
        //           {"namaProduk": "Produk A", "jumlah": 100},
        //           {"namaProduk": "Produk B", "jumlah": 7}
        //         ]
        //       }
        //     ],
        //     "catatan": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        //   },
      // ]
    //  this.jsonPtTBMProceddByStatus();//UNTUK MEMECAH DATA JSON PTTBM 
    //  diganti dengan this.ambildataproductdaridatabase();
    // this.ambildataproductdaridatabase();
//======================================
    this.notificationManajemenTransaksi = this.tempPesananDalamProses.length;//UNTUK NOTIFIKASI DI MANAJEMEN TRANSAKSI
  }

  // manajemenTransaksiLocalNotif(){
  //   let length = this.tempPesananDalamProses.length;
  //   for(let x=0; x < length; x++){
  //     this.localNotifications.schedule({
  //       id: this.tempPesananDalamProses[x].id,
  //       title: 'TBM Mobile',
  //       text: this.tempPesananDalamProses[x].nama + ' mengajukan pemesanan',
  //       icon: 'assets/img/icon.png'
  //     });
  //     console.log("id local notif: ", this.tempPesananDalamProses[x].id);
  //   }
    
  // }

//================================FUNCTION FILTER=================================//
  filterPesananDalamProses(searchInput){
    return this.tempPesananDalamProses.filter((y)=>{
      return y.nama.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
    });
  }

  filterPesananDitolak(searchInput){
    return this.tempPesananDitolak.filter((y)=>{
      return y.nama.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
    });
  }

  filterPesananDikirim(searchInput){
    return this.tempPesananDikirim.filter((y)=>{
      return y.nama.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
    });
  }

  filterPesananTerkirim(searchInput){
    return this.tempPesananTerkirim.filter((y)=>{
      return y.nama.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
    });
  }

  filterDistributor(searchInput){
    return this.distributor.filter((y)=> {
      return y.nama.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
    });
  }
//===============================================================================//

//======================FUNCTION UNTUK MENGOLAH JSON MANAJEMEN TRANSAKSI==============//
    dataTemp;
    tempPesananDalamProses=[]; //UNTUK TAMPUNG DATA HASIL FILTER BEDASARKAN STATUS
    flagTempPesananDalamProses: number; //UNTUK INDEX PENAMPUNG
    tempPesananDitolak=[];
    flagTempPesananDitolak: number;
    tempPesananDikirim=[];
    flagTempPesananDikirim: number;
    tempPesananTerkirim=[];
    flagTempPesananTerkirim: number;
    id = 0;
    jsonPtTBMProceddByStatus(){
      
      this.dataTemp = this.ptTBMdaridatabase;
      console.log("dataTemp:", this.dataTemp);

      let length = this.dataTemp.length;
      console.log("length: ", length);

      
      let x
      for(x=0; x < length; x++){
        if(this.dataTemp[x].status == "Pesanan ditolak"){
          console.log("pesanan ditolak: ", this.dataTemp[x]);
          this.tempPesananDitolak[this.flagTempPesananDitolak] = this.dataTemp[x];
          this.flagTempPesananDitolak ++;
        }
        else if(this.dataTemp[x].status == "Pesanan dalam proses"){
          console.log("pesanan dalam proses: ", this.dataTemp[x]);
          this.tempPesananDalamProses[this.flagTempPesananDalamProses] = this.dataTemp[x];
          this.flagTempPesananDalamProses ++;
        }
        else if(this.dataTemp[x].status == "Pesanan dikirim"){
          console.log("pesanan dikirim: ", this.dataTemp[x]);
          this.tempPesananDikirim[this.flagTempPesananDikirim] = this.dataTemp[x];
          this.flagTempPesananDikirim++;
        }
        else{
          console.log("pesanan terkirim: ", this.dataTemp[x]);
          this.tempPesananTerkirim[this.flagTempPesananTerkirim] = this.dataTemp[x];
          this.flagTempPesananTerkirim ++;
        }
      }

      console.log("suntik json pesanan ditolak: ", this.tempPesananDitolak);
      console.log("suntik json pesanan dalam proses: ", this.tempPesananDalamProses);
      console.log("suntik json pesanan dikirim: ", this.tempPesananDikirim);
      console.log("suntik json pesanan terkirim: ", this.tempPesananTerkirim);
      
    }
//====================================================================================//


//============================RESET DATA MANAJEMEN TRANSAKSI=======================//
  //NOTE: UNTUK RESET SEMUA PENAMPUNG(CTH: tempPesananDalamProses), DAN TRIGER INDEX DATA(CTH: flagTempPesananDalamProses)
  //      SUPAYA DATA TIDAK KETIMPA
  resetData(){
    let temp = [];
    temp[0] = this.tempPesananDalamProses.length;
    temp[1] = this.tempPesananDikirim.length;
    temp[2] = this.tempPesananDitolak.length;
    temp[3] = this.tempPesananTerkirim.length;
    this.flagTempPesananDalamProses = 0;
    this.flagTempPesananDikirim = 0;
    this.flagTempPesananDitolak = 0;
    this.flagTempPesananTerkirim = 0;

    let x;
    for(x=0; x < temp.length; x++){
      this.tempPesananDalamProses.splice(0, temp[x]);
      this.tempPesananDikirim.splice(0, temp[x]);
      this.tempPesananDitolak.splice(0, temp[x]);
      this.tempPesananTerkirim.splice(0, temp[x]);
    }

  }
//=================================================================================//




}
