import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';

/*
  Generated class for the PesananUserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PesananUserProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  flagAPIPesanan: boolean = false; //triger tembak API kalau pesamesanan telah selesai oleh user


  constructor(public http: Http) {
    console.log('Hello PesananUserProvider Provider');
  }

  public showms_pesanan(){
    return new Promise((resolve, reject) => {
    this.storage=new SQLite;
    this.storage.create({
        name:"data.db",
        location:"default"
    }).then((storage:SQLiteObject)=>{
        storage.executeSql("SELECT * FROM ms_pesanan", []).then((data)=>{
            let msproduct=[];
            if(data.rows.length > 0) {
                for(let i = 0; i < data.rows.length; i++) {
                    msproduct.push({
                        id:      data.rows.item(i).id,
                        jsonnya:      data.rows.item(i).jsonnya,
                    });
                }
            }resolve(msproduct);
            console.log("Table ms_pesanan opened");
            },(error)=>{
                reject(error);
                console.log("Unable to open database ms_pesanan");
            }
        )});
    });
  }

  public putms_pesanan(id,jsonnya){
      return new Promise((resolve, reject) => {
          this.storage = new SQLite;
          this.storage.create({
              name: "data.db",
              location: "default"
          }).then((storage: SQLiteObject) => {
              storage.executeSql("INSERT OR REPLACE INTO ms_pesanan (id,jsonnya) VALUES (?,?)", [id,jsonnya]).then((data) => {
                  resolve(data);
                  console.log("di provider pesanan-user.ts new pesanan has been post into ms_pesanan");
              }, (error) => {
                  reject(error);
                  console.log("di provider pesanan-user.ts Unable to insert into database ms_pesanan");
              }
              )
          });
      });
  }


}
