import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoginProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  flag: string; //untuk jika user ama manajemen
  flag2: string;
  password; //untuk di page ubah kata sandi
  kodeapi;
  kodeapiberandauser;
  kodeapiberandamanajemen;

  constructor(public http: Http) {
    console.log('Hello LoginProvider Provider');
    
  }

    //ms_user
        //untuk edit password dan masukan data user dari json
          public putms_user(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi, distributor, tokenExp,token,updateby,updatedate){
              return new Promise((resolve, reject) => {
                  this.storage = new SQLite;
                  this.storage.create({
                      name: "data.db",
                      location: "default"
                  }).then((storage: SQLiteObject) => {
                      storage.executeSql("INSERT OR REPLACE INTO ms_user (userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor, tokenExp,token,updateby,updatedate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor, tokenExp,token,updateby,updatedate]).then((data) => {
                          resolve(data);
                          console.log("di provider login.ts new product has been post into ms_user");
                          this.showms_user();
                      }, (error) => {
                          reject(error);
                          console.log("di provider login.ts Unable to insert into database ms_user");
                      }
                      )
                  });
              });
          }
        //untuk edit password dan masukan data user dari json

        //untuk show data user
          public showms_user(){
              return new Promise((resolve, reject) => {
                  this.storage=new SQLite;
                  this.storage.create({
                      name:"data.db",
                      location:"default"
                  }).then((storage:SQLiteObject)=>{
                      storage.executeSql("SELECT * FROM ms_user ", []).then((data)=>{
                          let msuser=[];
                          console.log("===data.rows.length: ", data.rows.length);
                          if(data.rows.length > 0) {
                              for(let i = 0; i < data.rows.length; i++) {
                                  msuser.push({
                                      userid:       data.rows.item(i).userid,
                                      username:     data.rows.item(i).username,
                                      titleid:      data.rows.item(i).titleid,
                                      password:     data.rows.item(i).password,
                                      custid:       data.rows.item(i).custid,
                                      namapt:       data.rows.item(i).namapt,
                                      agronomis:    data.rows.item(i).agronomis,
                                      fa:           data.rows.item(i).fa,
                                      sejak:        data.rows.item(i).sejak,
                                      pemakai:      data.rows.item(i).pemakai,
                                      jabatan:      data.rows.item(i).jabatan,
                                      lokasi:       data.rows.item(i).lokasi,
                                      distributor:  data.rows.item(i).distributor,
                                      tokenExp:     data.rows.item(i).tokenExp,
                                      token:        data.rows.item(i).token,
                                      updateby:     data.rows.item(i).updateby,
                                      updatedate:   data.rows.item(i).updatedate,
                                  });
                              }
                          }resolve(msuser);
                          console.log("di provider login.ts Table ms_user opened");  
                          },(error)=>{
                            reject(error);
                            console.log("di provider login.ts Unable to open database ms_user");
                          }
                      )});
                  });
          }
        //untuk show data user

    //ms_user


}
