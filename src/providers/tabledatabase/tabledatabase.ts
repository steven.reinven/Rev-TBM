import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';

/*
  Generated class for the DatabasefromjsonProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TabledatabaseProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  flag: string; //untuk jika user ama manajemen
  password; //untuk di page ubah kata sandi
  
  constructor(public http: Http) {
    console.log('Hello TabledatabaseProvider Provider');

    this.inisemuatable();
    
  }

    inisemuatable(){
        if(!this.isOpen) {
        this.storage = new SQLite();
        this.storage.create({
            name: "data.db",
            location: "default"
        }).then((storage:SQLiteObject) => {
            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_user (userid INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, titleid INTEGER, password TEXT, custid TEXT, namapt TEXT, agronomis TEXT, fa TEXT, sejak TEXT, pemakai TEXT, jabatan TEXT, lokasi TEXT, distributor TEXT, tokenExp TEXT, token TEXT, updateby TEXT, updatedate TEXT);", {}).then((data) => {
                console.log("Table ms_user has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql ms_user", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS beranda_manajemen (dataid INTEGER PRIMARY KEY AUTOINCREMENT, recordpemesanan TEXT);", {}).then((data) => {
                console.log("Table beranda_manajemen has been created: ", data);
                this.isOpen = true;
            }, (error)=> {
                console.log("Unable to execute sql", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_product (id INTEGER PRIMARY KEY AUTOINCREMENT, jsonnya TEXT);", {}).then((data) => {
                console.log("Table ms_product has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql ms_user", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_product_cart (id INTEGER PRIMARY KEY AUTOINCREMENT, jsonnya TEXT);", {}).then((data) => {
                console.log("Table ms_product_cart has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql ms_user", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_pesanan (id INTEGER PRIMARY KEY AUTOINCREMENT, jsonnya TEXT);", {}).then((data) => {
                console.log("Table ms_pesanan has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql ms_user", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_pesanan_list (id INTEGER PRIMARY KEY AUTOINCREMENT, jsonnya TEXT);", {}).then((data) => {
                console.log("Table ms_pesanan_list has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql ms_user", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_transaksi_TBM (data_id INTEGER PRIMARY KEY AUTOINCREMENT, data_json TEXT);", {}).then((data) => {
                console.log("Table ms_transaksi_TBM has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_transaksi_distributor (data_id INTEGER PRIMARY KEY AUTOINCREMENT, data_json TEXT);", {}).then((data) => {
                console.log("Table ms_transaksi_distributor has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql", error);
            })

            storage.executeSql("CREATE TABLE IF NOT EXISTS ms_slider (id INTEGER PRIMARY KEY AUTOINCREMENT, kodeupdate INTEGER, jsonnya TEXT, imgoffline TEXT);", {}).then((data) => {
                console.log("Table ms_slider has been created: ", data); 
                this.isOpen = true;
            }, (error) => {
                console.error("Unable to execute sql", error);
            })

        }, (error) => {
            console.error("Unable to open database", error);   
        });
        }
    }

}
