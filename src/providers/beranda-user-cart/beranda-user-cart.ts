import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the BerandaUserCartProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BerandaUserCartProvider {


  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello BerandaUserCartProvider Provider');
  }

  public showms_product_cart(){
    return new Promise((resolve, reject) => {
    this.storage=new SQLite;
    this.storage.create({
        name:"data.db",
        location:"default"
    }).then((storage:SQLiteObject)=>{
        storage.executeSql("SELECT * FROM ms_product_cart", []).then((data)=>{
            let msproduct=[];
            if(data.rows.length > 0) {
                for(let i = 0; i < data.rows.length; i++) {
                    msproduct.push({
                        id:      data.rows.item(i).id,
                        jsonnya:      data.rows.item(i).jsonnya,
                    });
                }
            }resolve(msproduct);
            console.log("di provider beranda-user-cart.ts Table ms_product_cart opened");
            },(error)=>{
                reject(error);
                console.log("di provider beranda-user-cart.ts Unable to open database ms_product_cart");
            }
        )});
    });
  }

  public putms_product_cart(id,jsonnya){
      return new Promise((resolve, reject) => {
          this.storage = new SQLite;
          this.storage.create({
              name: "data.db",
              location: "default"
          }).then((storage: SQLiteObject) => {
              storage.executeSql("INSERT OR REPLACE INTO ms_product_cart (id,jsonnya) VALUES (?,?)", [id,jsonnya]).then((data) => {
                  resolve(data);
                  console.log("di provider beranda-user-cart.ts new cart has been post into ms_product_cart");
              }, (error) => {
                  reject(error);
                  console.log("di provider beranda-user-cart.ts Unable to insert into database ms_product_cart");
              }
              )
          });
      });
  }

  //ini untuk notif
    JSONNYA;
    datakeranjangpesanandaridatabase;
    tampungdatakeranjangpesanandaridatabase;
    JSONNYAjsonnya;
    notif;
    x=0;
    y=0;
    z=0;
    jumlahbox;
    tampungjumlahbox=[];
  //ini untuk notif
  untuknotifcart(){
    this.showms_product_cart().then((result) => {
      this.datakeranjangpesanandaridatabase = <Array<Object>> result;
      console.log("di provider beranda-user-cart.tsms_product_cart has been Loaded");
      this.tampungdatakeranjangpesanandaridatabase=this.datakeranjangpesanandaridatabase;
      
    //   console.log("ini jsonnya",this.JSONNYA);
        this.tampungjumlahbox=[];
        this.jumlahbox=0;
        if(this.tampungdatakeranjangpesanandaridatabase.length != 0){
            console.log("di provider beranda-user-cart.ts tampungdatakeranjangpesanandaridatabase ada");
            this.JSONNYA=this.tampungdatakeranjangpesanandaridatabase[0].jsonnya;
            this.JSONNYAjsonnya=JSON.parse(this.tampungdatakeranjangpesanandaridatabase[0].jsonnya);
            this.notif=this.JSONNYAjsonnya[0].detail.length;

            if(this.JSONNYAjsonnya[0].detail.length != 0){
                console.log("di provider beranda-user-cart.ts ini intip length notif di",this.JSONNYAjsonnya[0].detail.length);
                for(this.x=0; this.x<this.JSONNYAjsonnya[0].detail.length; this.x++){
                    // console.log("di provider beranda-user-cart.ts ini prodcode"+this.x,this.JSONNYAjsonnya[0].detail[this.x].prodcode);
                    // console.log("di provider beranda-user-cart.ts ini nama"+this.x,this.JSONNYAjsonnya[0].detail[this.x].prodname);
                    // console.log("di provider beranda-user-cart.ts ini qty"+this.x,this.JSONNYAjsonnya[0].detail[this.x].qty);
                    this.tampungjumlahbox[this.x]=this.JSONNYAjsonnya[0].detail[this.x].qty;
                }
                console.log("di provider beranda-user-cart.ts jumlahboxnya tampung",this.tampungjumlahbox);
        
                for(this.y=0; this.y<this.tampungjumlahbox.length; this.y++){
                    this.jumlahbox +=this.tampungjumlahbox[this.y];
                }
        
                console.log("di provider beranda-user-cart.ts jumlahboxnya tampung",this.jumlahbox);
            }else{
                this.notif=0;
                this.jumlahbox=0;
            }

        }else{
            console.log("di provider beranda-user-cart.ts tampungdatakeranjangpesanandaridatabase kosong");
            this.notif=0;
            this.jumlahbox=0;
        }

    },(error) => {
        console.log("di provider beranda-user-cart.ts ERROR: showms_product_cart", error);
    });
  }

}
