import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';


/*
  Generated class for the SliderProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SliderProvider {

  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello SliderProvider Provider');
  }

    public showms_slider(){
      return new Promise((resolve, reject) => {
      this.storage=new SQLite;
      this.storage.create({
          name:"data.db",
          location:"default"
      }).then((storage:SQLiteObject)=>{
          storage.executeSql("SELECT * FROM ms_slider", []).then((data)=>{
              let msslider=[];
              if(data.rows.length > 0) {
                  for(let i = 0; i < data.rows.length; i++) {
                      msslider.push({
                          id:      data.rows.item(i).id,
                          kodeupdate: data.rows.item(i).kodeupdate,
                          jsonnya: data.rows.item(i).jsonnya,
                          imgoffline: data.rows.item(i).imgoffline
                      });
                  }
              }resolve(msslider);
              console.log("Table ms_slider opened");
              },(error)=>{
                  reject(error);
                  console.log("Unable to open database ms_slider");
              }
          )});
      });
    }

    public putms_slider(id,kodeupdate,jsonnya,imgoffline){
        return new Promise((resolve, reject) => {
            this.storage = new SQLite;
            this.storage.create({
                name: "data.db",
                location: "default"
            }).then((storage: SQLiteObject) => {
                storage.executeSql("INSERT OR REPLACE INTO ms_slider (id,kodeupdate,jsonnya,imgoffline) VALUES (?,?,?,?)", [id,kodeupdate,jsonnya,imgoffline]).then((data) => {
                    resolve(data);
                    console.log("di provider slider.ts new slider has been post into ms_slider",data);
                }, (error) => {
                    reject(error);
                    console.log("di provider slider.ts Unable to insert into database ms_slider");
                }
                )
            });
        });
    }

}
