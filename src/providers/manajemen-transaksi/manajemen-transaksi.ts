import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';

/*
  Generated class for the ManajemenTransaksiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ManajemenTransaksiProvider {

  constructor(public http: Http) {
    console.log('Hello ManajemenTransaksiProvider Provider');
  }

  flagAPIPemesanan: boolean = false; //triger tarik API pemesanan dalam proses jika pesanan di tolak/di terima
  flagAPIDitolak: boolean = false;
  flagAPIDikirim: boolean = false;
  //====FUNCTION PUT DATABASE=======================================||

    //====FUNCTION PUT DATABASE PT.TBM===================
        private storage;
        private isOpen;
      //+++++++++++++++++
      public put_transaksi_manajemen(data_id, data_json){
        return new Promise((resolve, reject)=> {
          this.storage = new SQLite;
          this.storage.create({
            name: "data.db",
            location: "default"
          }).then((storage: SQLiteObject)=> {
            storage.executeSql("INSERT OR REPLACE INTO ms_transaksi_TBM(data_id,data_json) VALUES(?,?)", [data_id,data_json]).then((data)=> {
              resolve(data);
              console.log("<<<< (Y)new data has been post into ms_transaksi_TBM >>>>");
            }, (error)=> {
              reject(error);
              console.log("<<<< (ERROR)put_transaksi_manajemen() FROM manajemen-transaksi-provider.ts >>>>");
            });
          });
        });
      }
    //===================================================

    //====FUNCTION PUT DATABASE DISTRIBUTOR==============
      public put_transaksi_distributor(data_id, data_json){

        return new Promise((resolve, reject)=> {

          this.storage = new SQLite;
          this.storage.create({

            name: "data.db",
            location: "default"
            
          }).then((storage: SQLiteObject)=> {

            storage.executeSql("INSERT OR REPLACE INTO ms_transaksi_distributor(data_id,data_json) VALUES(?,?)", [data_id,data_json]).then((data)=> {

              resolve(data);
              console.log("<<<< (Y)new data has been post into ms_transaksi_distributor >>>>");

            }, (error)=> {
              reject(error);
              console.log("<<<< (ERROR)put_transaksi_distributor() FROM manajemen-transaksi-provider.ts >>>>");
            });

          });

        });

      }
    //===================================================

  //================================================================||



  //====FUNCTION SHOW DATA DATABASE=================================

    //====show data dababase PT.TBM==============
        readyDataPesanan;
        readyDataDitolak;
        readyDataDikirim;
        readyDataTerkirim;
        flagTransaksiTab;
        flagTabPT;
      //+++++++++++++++++++
      public show_data_transaksi_manajemen(){
        return new Promise((resolve, reject)=> {
          this.storage = new SQLite;
          this.storage.create({
            name: "data.db",
            location: "default"
          }).then((storage: SQLiteObject)=> {
            storage.executeSql("SELECT * FROM ms_transaksi_TBM", []).then((data)=>{
              let transaksiManajemen=[];
              if(data.rows.length > 0){
                for(let i=0; i < data.rows.length; i++){
                  transaksiManajemen.push({
                    data_id:    data.rows.item(i).data_id,
                    data_json:  data.rows.item(i).data_json
                  });
                }
              }resolve(transaksiManajemen);
              console.log("===(PROVIDERS)Table ms_transaksi_TBM is opened===");
              console.log("===(PROVIDERS)transaksiManajemen: ", transaksiManajemen);
              if(this.flagTabPT == 'pesanan'){
                //====Pemesanan====
                  this.readyDataPesanan = JSON.parse(transaksiManajemen[0].data_json);
                  console.log("===(PROVIDERS)readyData pesanan: ", this.readyDataPesanan);
                  if(this.readyDataPesanan != null){
                    console.log("===(PROVIDERS)Length readyData pesanan: ", this.readyDataPesanan.length);
                  }
                  else{
                    console.log("===(PROVIDERS)Length NULL===");
                  }
                //=================
              }
              else if(this.flagTabPT == 'ditolak'){
                //====Ditolak=====
                  this.readyDataDitolak = JSON.parse(transaksiManajemen[1].data_json);
                  console.log("===(PROVIDERS)readyData ditolak: ", this.readyDataDitolak);
                  if(this.readyDataDitolak != null){
                    console.log("===(PROVIDERS)Length readyData ditolak: ", this.readyDataDitolak.length);
                  }
                  else{
                    console.log("===(PROVIDERS)Length NULL===");
                  }
                //=================
              }
              else if(this.flagTabPT == 'dikirim'){
                 //====Dikirim=====
                  this.readyDataDikirim = JSON.parse(transaksiManajemen[2].data_json);
                  console.log("===(PROVIDERS)readyData dikirim: ", this.readyDataDikirim);
                  if(this.readyDataDikirim != null){
                    console.log("===(PROVIDERS)Length readyData dikirim: ", this.readyDataDikirim.length);
                  }
                  else{
                    console.log("===(PROVIDERS)Length NULL===");
                  }
                //=================
              }
              else{
                //====Terkirim=====
                  this.readyDataTerkirim = JSON.parse(transaksiManajemen[3].data_json);
                  console.log("===(PROVIDERS)readyData terkirim: ", this.readyDataTerkirim);
                  if(this.readyDataTerkirim != null){
                    console.log("===(PROVIDERS)Length readyData terkirim: ", this.readyDataTerkirim.length);
                  }
                  else{
                    console.log("===(PROVIDERS)Length NULL===");
                  }
                //=================
              }
            },(error)=> {
              reject(error);
              console.log("<<<<< (X)Unable to open database ms_transaksi_TBM >>>>>");
            });
          });
        });
      }
    //===========================================

    //====show data database distributor=========
        readyDataDistributor;
      //++++++++++++++++++++++
      public show_data_transaksi_manajemen_distributor(){

        return new Promise((resolve, reject)=> {

          this.storage = new SQLite;
          this.storage.create({

            name: "data.db",
            location: "default"

          }).then((storage: SQLiteObject)=>{

            storage.executeSql("SELECT * FROM ms_transaksi_distributor", []).then((data)=>{

              let transaksiManajemen=[];
              if(data.rows.length > 0){

                for(let i=0; i < data.rows.length; i++){
                  transaksiManajemen.push({
                    data_id: data.rows.item(i).data_id,
                    data_json: data.rows.item(i).data_json
                  });
                }

              }resolve(transaksiManajemen);
              console.log("Table ms_transaksi_distributor is opened");
              console.log("===transaksiManajemenDistributor: ", transaksiManajemen);
              //====readyData===
                this.readyDataDistributor = JSON.parse(transaksiManajemen[0].data_json);
                console.log("===readyData distributor: ", this.readyDataDistributor);
              //================
            },(error) => {
              reject(error);
              console.log("<<<<< (X)Unable to open database ms_transaksi_distributor >>>>>");
            });

          });

        });

      }
    //===========================================

  //================================================================

}
