import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';

/*
  Generated class for the BerandaUserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BerandaUserProvider {


  private storage:SQLite;
  public db:SQLiteObject;
  private isOpen:boolean;

  constructor(public http: Http) {
    console.log('Hello BerandaUserProvider Provider');
  }

    public showms_product(){
      return new Promise((resolve, reject) => {
      this.storage=new SQLite;
      this.storage.create({
          name:"data.db",
          location:"default"
      }).then((storage:SQLiteObject)=>{
          storage.executeSql("SELECT * FROM ms_product", []).then((data)=>{
              let msproduct=[];
              if(data.rows.length > 0) {
                  for(let i = 0; i < data.rows.length; i++) {
                      msproduct.push({
                          id:      data.rows.item(i).id,
                          jsonnya:      data.rows.item(i).jsonnya,
                      });
                  }
              }resolve(msproduct);
              console.log("Table ms_product opened");
              },(error)=>{
                  reject(error);
                  console.log("Unable to open database ms_product");
              }
          )});
      });
    }

    public putms_product(id,jsonnya){
        return new Promise((resolve, reject) => {
            this.storage = new SQLite;
            this.storage.create({
                name: "data.db",
                location: "default"
            }).then((storage: SQLiteObject) => {
                storage.executeSql("INSERT OR REPLACE INTO ms_product (id,jsonnya) VALUES (?,?)", [id,jsonnya]).then((data) => {
                    resolve(data);
                    console.log("di provider beranda-user.ts new product has been post into ms_product");
                }, (error) => {
                    reject(error);
                    console.log("di provider beranda-user.ts Unable to insert into database ms_product");
                }
                )
            });
        });
    }

}
