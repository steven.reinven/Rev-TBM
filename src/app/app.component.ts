import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from "../pages/login/login";
import { TabsPage } from '../pages/tabs/tabs';
import { LoginProvider } from '../providers/login/login';
import { TabledatabaseProvider } from '../providers/tabledatabase/tabledatabase';


@Component({
  templateUrl: 'app.html',
  providers: [LoginProvider]
})
export class MyApp {
  USERNAME;
  PASSWORD;
  TITLEID;
  TOKEN;
  datalogindaridatabase;
  tampungdatalogindaridatabase;
  tokendaridatabase;

  flag: boolean=false;
  // statusBar: StatusBar, splashScreen: SplashScreen
  rootPage:any;
  constructor(platform: Platform, splashScreen: SplashScreen, public tabledatabasenya: TabledatabaseProvider,public databasenya: LoginProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      // statusBar.styleDefault();
      setTimeout(() => {
        splashScreen.hide();
      }, 100);
      

      //untuk validasi kalau sudah ada data login
      this.fetchdaridatabase();
      this.tabledatabasenya.inisemuatable();
      //untuk validasi kalau sudah ada data login
    });
  }


  fetchdaridatabase(){
    console.log("ini fetchdaridatabase start di app.components.ts");
    this.databasenya.showms_user().then((result) => {
      this.datalogindaridatabase = <Array<Object>> result;
      console.log("ms_user has been Loaded di app.components.ts");
      this.tampungdatalogindaridatabase=this.datalogindaridatabase;

      if(this.tampungdatalogindaridatabase != ""){
        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.PASSWORD=this.tampungdatalogindaridatabase[0].password;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;
        this.TOKEN=this.tampungdatalogindaridatabase[0].token;

        console.log("validatorlogin tittleid start di app.components.ts");
          if(this.TITLEID == "4" || this.TITLEID == "5" || this.TITLEID == "8" || this.TITLEID == "9"){
            this.databasenya.flag="user@gmail.com";
            console.log("ini username dari database",this.USERNAME);
            console.log("ini password dari database",this.PASSWORD);
            console.log("cocok titleid untuk user di app.components.ts",this.TITLEID);
            this.rootPage=TabsPage;
          }else if(this.TITLEID == "2"){
            this.databasenya.flag="manajemen@gmail.com";
            // this.databasenya.flag2="distributor@gmail.com";
            this.databasenya.kodeapi=15;
            this.databasenya.kodeapiberandamanajemen=30;
            console.log("ini username dari database",this.USERNAME);
            console.log("ini password dari database",this.PASSWORD);
            console.log("cocok titleid untuk manajemen di app.components.ts",this.TITLEID);
            this.rootPage=TabsPage;
          }else if(this.TITLEID == "3" || this.TITLEID == "6" || this.TITLEID == "7"){
            if(this.TITLEID == "3"){
              this.databasenya.kodeapi=31;
              this.databasenya.kodeapiberandamanajemen=310;
              this.databasenya.flag="distributor@gmail.com";
              this.rootPage=TabsPage;
              console.log("tittleid nya 31"+this.TITLEID);
            }else if(this.TITLEID == "6"){
              this.databasenya.kodeapi=32;
              this.databasenya.kodeapiberandamanajemen=320;
              this.databasenya.flag="agronomis&FA(6)@gmail.com";
              this.rootPage=TabsPage;
              console.log("tittleid nya 32"+this.TITLEID);
              console.log("===flag: ", this.databasenya.flag);
            }else if(this.TITLEID == "7"){
              this.databasenya.kodeapi=33;
              this.databasenya.kodeapiberandamanajemen=330;
              this.databasenya.flag="agronomis&FA(7)@gmail.com";
              this.rootPage=TabsPage;
              console.log("tittleid nya 33"+this.TITLEID);
            }else{
              console.log("Belum terdaftar di app.components.ts");
              this.rootPage = LoginPage;
            }
            
            // this.databasenya.flag2="distributor@gmail.com";
            console.log("cocok titleid untuk distributor di app.components.ts",this.TITLEID);
          }else{
            this.rootPage = LoginPage;
            console.log("ini rootPage nya LoginPage di app.components.ts");
          }
          console.log("validatorlogin titleid finnish di app.components.ts");
      }else{
        this.rootPage = LoginPage;
        console.log("ini rootPage nya LoginPage di app.components.ts");
      }

    },(error) => {
        console.log("ERROR: showms_user di app.components.ts", error);
    });
  }
  

  

 
  
  



}


