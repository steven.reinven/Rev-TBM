import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

// import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProductPage } from "../pages/beranda-user/product/product";
import { HttpModule } from "@angular/http";
import { PesananPage } from "../pages/pesanan/pesanan";
import { KeranjangPesananPage } from "../pages/beranda-user/product/keranjang-pesanan/keranjang-pesanan";
import { KonfirmasiPesananPage } from "../pages/beranda-user/product/keranjang-pesanan/konfirmasi-pesanan/konfirmasi-pesanan";
import { ListPesananPage } from "../pages/pesanan/list-pesanan/list-pesanan";
import { DetailPesananPage } from "../pages/pesanan/list-pesanan/detail-pesanan/detail-pesanan";
import { PengaturanPage } from "../pages/pengaturan/pengaturan";
import { UbahKataKunciPage } from "../pages/pengaturan/ubah-kata-kunci/ubah-kata-kunci";
import { LoginPage } from "../pages/login/login";

import { TransaksiManajemenPage } from "../pages/transaksi-manajemen/transaksi-manajemen";
import { BerandaManajemenPage } from "../pages/beranda-manajemen/beranda-manajemen";
import { DetailTransaksiPage } from "../pages/transaksi-manajemen/detail-transaksi/detail-transaksi";
import { DataProvider } from '../providers/data/data';
import { BerandaUserPage } from "../pages/beranda-user/beranda-user";
import { ProductBrosurPage } from "../pages/beranda-user/product/product-brosur/product-brosur";
import { DetailInformationProductPage } from "../pages/beranda-user/product/detail-information-product/detail-information-product";
// import { LocalNotifications } from "@ionic-native/local-notifications";
import { Network } from '@ionic-native/network';
import { BerandaManajemenProvider } from '../providers/beranda-manajemen/beranda-manajemen';
import { BerandaUserProvider } from '../providers/beranda-user/beranda-user';
import { LoginProvider } from '../providers/login/login';
import { TabledatabaseProvider } from '../providers/tabledatabase/tabledatabase';
import { BerandaUserCartProvider } from '../providers/beranda-user-cart/beranda-user-cart';
import { KeranjangPesananProvider } from '../providers/keranjang-pesanan/keranjang-pesanan';
import { ManajemenTransaksiProvider } from '../providers/manajemen-transaksi/manajemen-transaksi';
import { PesananUserProvider } from '../providers/pesanan-user/pesanan-user';
import { PesananUserListProvider } from '../providers/pesanan-user-list/pesanan-user-list';
import { LihatProfilePage } from '../pages/pengaturan/lihat-profile/lihat-profile';
import { SliderProvider } from '../providers/slider/slider';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';


@NgModule({
  declarations: [
    MyApp,
    BerandaUserPage,
    TabsPage,
    PengaturanPage,
    LoginPage,
    TransaksiManajemenPage,
    BerandaManajemenPage,

    //=Home Child=
    ProductPage,
    KeranjangPesananPage,
    KonfirmasiPesananPage,
    ProductBrosurPage,
    DetailInformationProductPage,

    //=Pesanan Page
    PesananPage,
    ListPesananPage,
    DetailPesananPage,
    DetailTransaksiPage,

    //=Pengaturan
    LihatProfilePage,
    UbahKataKunciPage 
  ],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Kembali'
    }),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BerandaUserPage,
    TabsPage,
    PengaturanPage,
    LoginPage,
    TransaksiManajemenPage,
    BerandaManajemenPage,
    
    //=Home Child=
    ProductPage,
    KeranjangPesananPage,
    KonfirmasiPesananPage,
    ProductBrosurPage,
    DetailInformationProductPage,

    //=Pesanan Page
    PesananPage,
    ListPesananPage,
    DetailPesananPage,
    DetailTransaksiPage,

    //=Pengaturan
    LihatProfilePage,
    UbahKataKunciPage 
  ],
  providers: [
    // StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    


    DataProvider,
    Network,
    File,
    FileTransfer,
    

    // LocalNotifications,
    BerandaManajemenProvider,
    BerandaUserProvider,
    LoginProvider,
    TabledatabaseProvider,
    BerandaUserCartProvider,
    BerandaUserCartProvider,
    KeranjangPesananProvider,
    ManajemenTransaksiProvider,
    PesananUserProvider,
    PesananUserListProvider,
    SliderProvider
  ]
})
export class AppModule {}
