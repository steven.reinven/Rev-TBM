import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { DataProvider } from "../../providers/data/data";
import { Http } from '@angular/http';
import { Network } from "@ionic-native/network";
import { BerandaManajemenProvider } from "../../providers/beranda-manajemen/beranda-manajemen";
import { LoginProvider } from '../../providers/login/login';

declare var io : any;
/**
 * Generated class for the BerandaManajemenPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-beranda-manajemen',
  templateUrl: 'beranda-manajemen.html',
})
export class BerandaManajemenPage {

// ====CONSTRUCTOR====
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: Http,
    public dataProvider: DataProvider, 
    public databasenya: LoginProvider,
    public alertCtrl: AlertController,
    private network: Network,
    public toastCtrl: ToastController,
    public manajemenData: BerandaManajemenProvider,
  ) {
    // this.dataProvider.manajemenTransaksiLocalNotif();

    // this.manajemenData.berandaManajemenData();
  }
// ==================

  ionViewDidEnter(dataid, recordpemesanan, txtbmid,txtbmname,txtbmdate,txtbmstatus,txtbmtotalbox,txtbmnoorder,txtbmsqno,txtbmproductcode,txtbmproductname,txtbmproductqty,txtbmproductqtytype,txtbmcatatan) {
    console.log('ionViewDidLoad beranda-manajemen di beranda-manajemen.ts');
    if(
        this.network.type === 'none'
      ||this.network.type === 'unknown'){
      console.log("ini tidak connect");
      this.showdata(dataid,recordpemesanan);

    }else if(
            this.network.type === 'wifi'
          ||this.network.type === 'cellular'
          ||this.network.type === '4g'
          ||this.network.type === '3g'
          ||this.network.type === '2g'
          ||this.network.type === 'ethernet'){
      console.log("ini connect");
      this.ambilToken(dataid, recordpemesanan);
      
    }else{
      console.log("connect aneh");
      this.showdata(dataid,recordpemesanan);
    }
     
  }

  TOKEN;
  USERNAME;
  datalogindaridatabase;
  tampungdatalogindaridatabase
    ambilToken(dataid, recordpemesanan){
      console.log("ambilToken start di beranda-manajemen.ts");
      this.databasenya.showms_user().then((result)=> {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("ms_user has been Loaded di beranda-manajemen.ts");
        this.tampungdatalogindaridatabase = this.datalogindaridatabase;

        console.log("======this.tampungdatalogindaridatabase[0].token======", this.tampungdatalogindaridatabase[0].token);
        if(this.tampungdatalogindaridatabase[0].token != ""){
          this.TOKEN = this.tampungdatalogindaridatabase[0].token;
          this.USERNAME = this.tampungdatalogindaridatabase[0].username;
          console.log("ini token dari database di beranda-manajemen.ts",this.TOKEN);

          //ambil data dari api
            this.ambildataberandamanajemendariapi(dataid, recordpemesanan);
          //
        }else{
          console.log("anda tidak memiliki token di beranda-manajemen.ts");
        }
      }, (error)=>{
        console.log("ERROR: showns_user di beranda-user.ts", error);
      });
    }


    dataManajemenApi;
    tempDataManajemenApi
    ambildataberandamanajemendariapi(dataid, recordpemesanan){
      this.http.get('http://api.nex.web.id/pestisida/api.php?q='+this.databasenya.kodeapiberandamanajemen+'&q2='+this.USERNAME+'&token='+this.TOKEN+' ').subscribe(res => {
        this.dataManajemenApi = res.json();
        console.log("===dataManajemenApi===: ", this.dataManajemenApi);
        this.tempDataManajemenApi = this.dataManajemenApi;
        console.log("===tempDataManajemenApi===: ", this.tempDataManajemenApi);

        if(this.tempDataManajemenApi[0].f0 == 1){
          let i = 0;
          for(i=0; i<this.tempDataManajemenApi.length; i++){
            this.manajemenData.putberanda_manajemen(
              dataid = 1,
              recordpemesanan = JSON.stringify(this.tempDataManajemenApi)
            ).then((result)=> {
              console.log("putberanda_manajemen finish di beranda-manajemen.ts");
            }, (error)=>{
              console.log("ERROR: putberanda_manajemen di beranda_manajemen.ts", error);
            });
          }
          this.showdata(dataid,recordpemesanan);
        }else{
          let toast = this.toastCtrl.create({
            message: this.tempDataManajemenApi[0].f0,
            duration: 2000,
            position: 'middle'
          });
          toast.present();
          this.manajemenData.putberanda_manajemen(
            dataid = 1,
            recordpemesanan = null
          ).then((result)=> {
            console.log("putberanda_manajemen finish di beranda-manajemen.ts");
          }, (error)=>{
            console.log("ERROR: putberanda_manajemen di beranda_manajemen.ts", error);
          });
        }

        
      }, (err)=>{
        console.log("error httget di beranda-manajemen.ts", err);
        let toast = this.toastCtrl.create({
          message: 'Server Maintenance',
          duration: 2000,
          position: 'middle'
        });
        toast.present();
      });
    }

    dataManajemenDariDatabase;
    tempData=[];
    readyData;
    f1;
    f2;
    f3;
    showdata(dataid, recordpemesanan){
      console.log("show jalan");
      this.manajemenData.showberanda_manajemen().then((result)=> {
        this.dataManajemenDariDatabase= <Array<Object>> result;
         console.log("===dataManajemenDariDatabase[0]===: ", this.dataManajemenDariDatabase[0].recordpemesanan);
          this.readyData = JSON.parse(this.dataManajemenDariDatabase[0].recordpemesanan);
          console.log("==f1==: ", this.readyData[0].f1);
          this.f1 = this.readyData[0].f1;
          this.f2 = this.readyData[0].f2;
          this.f3 = this.readyData[0].f3;
          console.log("==f1 asli==: ", this.f1);
          console.log("==f2 asli==: ", this.f2);
          console.log("==f3 asli==: ", this.f3);
        if(this.dataManajemenDariDatabase != ""){
          let x=0;
          console.log("length===: ", this.dataManajemenDariDatabase.length);
          this.tempData=[];
          
          for(x=0; x<this.dataManajemenDariDatabase.length; x++){
            this.tempData[x] = {
               id: this.dataManajemenDariDatabase[x].dataid,
               recordpemesanan: JSON.parse(this.dataManajemenDariDatabase[x].recordpemesanan),
            }
               console.log("==ini format json==: ", JSON.stringify(this.tempData));
          }
        }else{
          console.log("data manajemen dari database kosong di beranda-manajemen.ts");
          this.ambilToken(dataid, recordpemesanan);
        }
      }, (error)=> {
        console.log("ERROR: showberanda_manajemen di beranda-manajemen.ts", error);
      });
    }


}//END OF CLASS

