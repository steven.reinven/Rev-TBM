import { Component } from '@angular/core';
import { NavController, Platform, ToastController, LoadingController } from 'ionic-angular';
import { ProductPage } from "./product/product";
import { KeranjangPesananPage } from "./product/keranjang-pesanan/keranjang-pesanan";

import { DataProvider } from "../../providers/data/data";
import { BerandaUserProvider } from '../../providers/beranda-user/beranda-user';
import { LoginProvider } from '../../providers/login/login';
import { Http } from '@angular/http';
import { Network } from "@ionic-native/network";
import { KeranjangPesananProvider } from '../../providers/keranjang-pesanan/keranjang-pesanan';
import { BerandaUserCartProvider } from '../../providers/beranda-user-cart/beranda-user-cart';
import { SliderProvider } from '../../providers/slider/slider';
import { FileTransfer,FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";


@Component({
  selector: 'page-beranda-user',
  templateUrl: 'beranda-user.html'
})
export class BerandaUserPage {
  platformStatus;
  public unregisterBackButtonAction: any;
  

  iuntukimgsrc:String = new Date().toISOString();

  //dari api
    dataproductdariapi;
    tampungdataproductdariapi;


    datasliderdariapi;
    tampungdatasliderdariapi;
  //dari api

  //dari database
    datalogindaridatabase;
    tampungdatalogindaridatabase;
    TOKEN;
    NAMAPT;

    //product
      dataproductdaridatabase;
      tampungdataproductdaridatabase;
      products;
      formatJsonProduct=[];
    //product

    //product type
      dataproducttypedaridatabase;
      tampungdataproducttypedaridatabase;
      f6dataproducttypedaridatabase;
      f6tampungdataproducttypedaridatabase;
    //product type

    f6;
    formatJsonF6=[];
    tampungformatJsonF6=[];


    //slider
      datasliderdaridatabase;
      tampungdatasliderdaridatabase;
      sliders;
      formatJsonSlider=[];
      bandingkodeupdate;
    //slider

    datadaridatabaseok;
    sliderdaridatabaseok;

  //dari database


  constructor(
    public navCtrl: NavController,
    public databaselogin: LoginProvider,
    public databasenya: BerandaUserProvider,
    public databasekeranjangpesanan: KeranjangPesananProvider,
    public databasecartnya: BerandaUserCartProvider,
    public databaseslidernya: SliderProvider,
    private http:Http,
    public dataProvider: DataProvider,
    public platform: Platform,
    public toastCtrl: ToastController,
    public loadingCtrl:LoadingController,
    private network: Network,
    public transfer: FileTransfer,
    private file:File) {

      platform.ready().then(()=>{
        if(this.platform.is('ios')){
          console.log("ios ready");
          this.platformStatus = "ios";
        }
        if(this.platform.is('android')){
          console.log("android ready");
          this.platformStatus = "android";
        }
        
      });

      
     
      
  }


  //sqlite data product
    ambiltokendaridatabase(id,kodeupdate,jsonnya,imgoffline){
      console.log("di beranda-user.ts ambiltokendaridatabase start");
      this.databaselogin.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di beranda-user.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        if(this.tampungdatalogindaridatabase[0].token != ""){
          this.TOKEN=this.tampungdatalogindaridatabase[0].token;
          this.NAMAPT=this.tampungdatalogindaridatabase[0].namapt;
          console.log("di beranda-user.ts ini token dari database",this.TOKEN);
          
          this.ambildatasliderdariapi(id,kodeupdate,jsonnya,imgoffline);
          this.ambildataproductdariapi(id,kodeupdate,jsonnya,imgoffline);
          
        }else{
          console.log("di beranda-user.ts anda tidak memiliki token");

        }
      },(error) => {
          console.log("di beranda-user.ts ERROR: showms_user", error);
      });
      console.log("di beranda-user.ts ambiltokendaridatabase finnish");
    }

    ambildataproductdariapi(id,kodeupdate,jsonnya,imgoffline){
      console.log("di beranda-user.ts ambildataproductdariapi start");
      let tempLink: string;
      if(this.databaselogin.flag == 'user@gmail.com'){
        tempLink = "http://api.nex.web.id/pestisida/api.php?q=10&token="+this.TOKEN+" ";
        console.log("===tempLink: ", tempLink);
      }
      else if(this.databaselogin.flag == 'skip'){
        tempLink = "http://api.nex.web.id/pestisida/api.php?q=10";
        console.log("===tempLink: ", tempLink);
      }
      
      this.http.get(tempLink)
      .subscribe(res=>{
        this.dataproductdariapi=res.json();
        this.tampungdataproductdariapi=this.dataproductdariapi;
          if(this.tampungdataproductdariapi.length != 0){
            console.log("di beranda-user.ts data dari api tidak kosong");
            this.databasenya.putms_product(
              id=1,
              jsonnya=JSON.stringify(this.tampungdataproductdariapi)
              
            ).then((result) => {
              console.log("di beranda-user.ts putms_product finnish");
              this.ambildataproductdaridatabase(id,kodeupdate,jsonnya,imgoffline);
            },(error) => {
              console.log("di beranda-user.ts ERROR: putms_product", error);            
            });
          }else{
            console.log("di beranda-user.ts data dari api kosong"); 
            let toast = this.toastCtrl.create({
              message: 'Server Down',
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
          }
      },(err)=>{
        console.log("di beranda-user.ts error httpgetproduct",err);
        let toast = this.toastCtrl.create({
          message: 'Koneksi Terputus',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        this.ambildataproductdaridatabase(id,kodeupdate,jsonnya,imgoffline);
      });
      console.log("di beranda-user.ts ambildataproductdariapi finnish");
    }

    ambildataproductdaridatabase(id,kodeupdate,jsonnya,imgoffline){
      console.log("di beranda-user.ts ambildaraproductdaridatabase start");
      this.databasenya.showms_product().then((result) => {
        this.dataproductdaridatabase = <Array<Object>> result;
        console.log("di beranda-user.ts ms_product has been Loaded");
        this.tampungdataproductdaridatabase=this.dataproductdaridatabase;

        if(this.tampungdataproductdaridatabase != ""){
          this.products=JSON.parse (this.tampungdataproductdaridatabase[0].jsonnya);
          this.datadaridatabaseok="ok";
          // let toast = this.toastCtrl.create({
          //   message: 'Semua produk telah ditampilkan',
          //   duration: 2000,
          //   position: 'bottom'
            
          // });
          // toast.present();
        }else{
          console.log("di beranda-user.ts showms_product kosong");
          this.ambiltokendaridatabase(id,kodeupdate,jsonnya,imgoffline);
        }
      },(error) => {
          console.log("di beranda-user.ts ERROR: showms_product", error);
      });
      console.log("di beranda-user.ts ambildaraproductdaridatabase finnish");
    }
  //sqlite data product

  //sqlite data slider
  

    imglinknya;
    panjangnamaimgonline=[];
    panjangnamaimgoffline=[];
    namaimgofflinedari=39;
    sliderdariofflineok;
    gambarofflineandroid;
    

    x;
    y;
    z;
    public fileTransfer: FileTransferObject = this.transfer.create();
    ambildatasliderdariapi(id,kodeupdate,jsonnya,imgoffline){
      console.log("di beranda-user.ts ambildatasliderdariapi start");

      this.http.get("http://api.nex.web.id/pestisida/api.php?q=9")
      .subscribe(res=>{
        this.datasliderdariapi=res.json();
        this.tampungdatasliderdariapi=this.datasliderdariapi;

          this.gambarofflineandroid=[
            {
              "f0": this.tampungdatasliderdariapi[0].f0,
              "f1": [
                
              ]
            }
          ]

          if(this.tampungdatasliderdariapi.length != 0){

            console.log("length data ada",this.tampungdatasliderdariapi[0].f1.length);
            // console.log("length tulisannya",this.tampungdatasliderdariapi[0].f1[0].image.length);
            this.file.createDir(this.file.dataDirectory,'imgslideroffline',true);
            for(this.x=0; this.x<this.tampungdatasliderdariapi[0].f1.length; this.x++){

              this.gambarofflineandroid[0].f1[this.x]={'image':''};
              // console.log("ini gambaroffline android ke"+this.x,JSON.stringify(this.gambarofflineandroid));

              this.imglinknya="";
              this.imglinknya=this.tampungdatasliderdariapi[0].f1[this.x].image;
              this.panjangnamaimgonline[this.x]=this.tampungdatasliderdariapi[0].f1[this.x].image.length;
              // console.log("intip panjang ke"+this.x,this.panjangnamaimgonline[this.x]);
              this.panjangnamaimgoffline[this.x]=this.panjangnamaimgonline[this.x]-this.namaimgofflinedari;
              // console.log("intip hasil kurang",this.panjangnamaimgoffline[this.x]);
              for(this.y=0; this.y<this.panjangnamaimgoffline[this.x]; this.y++){
                console.log("slider ke"+this.x+" intip karakter ke"+this.y+" "+this.x,this.tampungdatasliderdariapi[0].f1[this.x].image[this.namaimgofflinedari+this.y]);                
              }
              this.fileTransfer.download(this.imglinknya, this.file.externalDataDirectory + ''+'slider'+this.x+'.jpg').then((entry) => {               
                console.log('download complete: ' + entry.toURL());                 
              }, (error) => {
                console.log("ini error bikin file");
              });
              this.gambarofflineandroid[0].f1[this.x].image="file:///storage/emulated/0/Android/data/com.adnetindo.tbmmobile/files/slider"+this.x+".jpg";              
            }
            console.log("ini gambarofflineandroid",JSON.stringify(this.gambarofflineandroid));
            console.log("ini tampungdatasliderdariapi ",JSON.stringify(this.tampungdatasliderdariapi));

            this.databaseslidernya.showms_slider().then((result) => {
              this.datasliderdaridatabase = <Array<Object>> result;
              console.log("di beranda-user.ts ms_product has been Loaded");
              this.tampungdatasliderdaridatabase=this.datasliderdaridatabase;
              // console.log("intip slider dari database",JSON.stringify(this.tampungdatasliderdaridatabase));
      
              if(this.tampungdatasliderdaridatabase != ""){
                if(this.tampungdatasliderdaridatabase[0].kodeupdate != this.tampungdatasliderdariapi[0].f0){
                  console.log("di beranda-user.ts slider dari api tidak kosong");
                  console.log("di beranda-user.ts slider perlu update");
                  this.databaseslidernya.putms_slider(
                    id=1,
                    kodeupdate=this.tampungdatasliderdariapi[0].f0,
                    jsonnya=JSON.stringify(this.tampungdatasliderdariapi),
                    imgoffline=JSON.stringify(this.gambarofflineandroid)
                    
                  ).then((result) => {
                    console.log("di beranda-user.ts putms_slider finnish");
                    this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
                  },(error) => {
                    console.log("di beranda-user.ts ERROR: putms_slider", error);            
                  });
                }else{
                  this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
                  console.log("di beranda-user slider tidak perlu update");
                }
               
              }else{
                console.log("di beranda-user.ts showms_slider kosong isi data baru dari api");
                this.databaseslidernya.putms_slider(
                  id=1,
                  kodeupdate=this.tampungdatasliderdariapi[0].f0,
                  jsonnya=JSON.stringify(this.tampungdatasliderdariapi),
                  imgoffline=JSON.stringify(this.gambarofflineandroid)
                  
                ).then((result) => {
                  console.log("di beranda-user.ts putms_slider finnish");
                  this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
                },(error) => {
                  console.log("di beranda-user.ts ERROR: putms_slider", error);            
                });
              }
            },(error) => {
                console.log("di beranda-user.ts ERROR: showms_slider", error);
            });

            
          }else{
            console.log("di beranda-user.ts slider dari api kosong"); 
            let toast = this.toastCtrl.create({
              message: 'Server Down',
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
          }
      },(err)=>{
        console.log("di beranda-user.ts error httpgetproduct",err);
        let toast = this.toastCtrl.create({
          message: 'Koneksi Terputus',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
      });
      console.log("di beranda-user.ts ambildatasliderdariapi finnish");
    }

    ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline){
      console.log("di beranda-user.ts ambildatasliderdaridatabase start");
      this.databaseslidernya.showms_slider().then((result) => {
        this.datasliderdaridatabase = <Array<Object>> result;
        console.log("di beranda-user.ts ms_product has been Loaded");
        this.tampungdatasliderdaridatabase=this.datasliderdaridatabase;
        console.log("intip slider dari database",this.tampungdatasliderdaridatabase);
        console.log("intip slider dari database",JSON.stringify(this.tampungdatasliderdaridatabase));

        if(this.tampungdatasliderdaridatabase != ""){

          if(
            this.network.type === 'none'
          ||this.network.type === 'unknown'){
          console.log("di beranda-user.ts ini none atau unknown network");
            let toast = this.toastCtrl.create({
              message: 'Tidak Terkoneksi',
              duration: 2000,
              position: 'top'
            });
            toast.present();
            this.sliders=JSON.parse(this.tampungdatasliderdaridatabase[0].imgoffline);
            this.sliderdaridatabaseok="ok";
          }else if(
              this.network.type === 'wifi' 
            ||this.network.type === 'cellular'
            ||this.network.type === '4g'
            ||this.network.type === '3g'
            ||this.network.type === '2g'
            ||this.network.type === 'ethernet'){
            console.log("di beranda-user.ts ini connect");
            this.sliders=JSON.parse(this.tampungdatasliderdaridatabase[0].jsonnya);
            this.sliderdaridatabaseok="ok";
                
          }else{
            console.log("di beranda-user.ts connect aneh");
            let toast = this.toastCtrl.create({
              message: 'Tidak Terkoneksi',
              duration: 2000,
              position: 'top'
            });
            toast.present();
            this.sliders=JSON.parse(this.tampungdatasliderdaridatabase[0].imgoffline);
            this.sliderdaridatabaseok="ok";
          
          }

          // this.sliders=JSON.parse(this.tampungdatasliderdaridatabase[0].jsonnya);
          // console.log("intip slidernya dari database",this.sliders);
          console.log("intip slidernya dari database",JSON.stringify(this.sliders));
          // this.sliderdaridatabaseok="ok";
          // let toast = this.toastCtrl.create({
          //   message: 'Semua produk telah ditampilkan',
          //   duration: 2000,
          //   position: 'bottom'
            
          // });
          // toast.present();
        }else{
          console.log("di beranda-user.ts showms_slider kosong");
          this.ambiltokendaridatabase(id,kodeupdate,jsonnya,imgoffline);
        }
      },(error) => {
          console.log("di beranda-user.ts ERROR: showms_slider", error);
      });
      console.log("di beranda-user.ts ambildatasliderdaridatabase finnish");
    }
  //sqlite data slider
 
  ionViewDidLoad(id,kodeupdate,jsonnya,imgoffline){
    console.log("di beranda-user.ts ionViewDidLoad");
    this.databasecartnya.untuknotifcart();
    if(
      this.network.type === 'none'
    ||this.network.type === 'unknown'){
    console.log("di beranda-user.ts ini none atau unknown network");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    this.ambildataproductdaridatabase(id,kodeupdate,jsonnya,imgoffline);
    this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
    }else if(
        this.network.type === 'wifi' 
      ||this.network.type === 'cellular'
      ||this.network.type === '4g'
      ||this.network.type === '3g'
      ||this.network.type === '2g'
      ||this.network.type === 'ethernet'){
      console.log("di beranda-user.ts ini connect");
      // let toast = this.toastCtrl.create({
      //   message: 'Terkoneksi',
      //   duration: 2000,
      //   position: 'top'
      // });
      // toast.present();
     
      if(this.databaselogin.flag == 'user@gmail.com' || this.databaselogin.flag == 'manajemen@gmail.com'){
        this.ambiltokendaridatabase(id,kodeupdate,jsonnya,imgoffline);
      }
      else if(this.databaselogin.flag == 'skip'){
        this.ambildataproductdariapi(id,kodeupdate,jsonnya,imgoffline);
        this.ambildatasliderdariapi(id,kodeupdate,jsonnya,imgoffline);
      }
      
    }else{
      console.log("di beranda-user.ts connect aneh");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.ambildataproductdaridatabase(id,kodeupdate,jsonnya,imgoffline);
      this.ambildatasliderdaridatabase(id,kodeupdate,jsonnya,imgoffline);
    }
  }

  doRefresh(refresher,id,kodeupdate,jsonnya,imgoffline) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.ionViewDidLoad(id,kodeupdate,jsonnya,imgoffline);
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  showToast(){
    let toast = this.toastCtrl.create({
      message: 'Tekan sekali lagi untuk keluar dari aplikasi',
      duration: 2000,
      position: 'bottom'
    });
    // this.platform.exitApp();

    toast.onDidDismiss(()=>{
      console.log('Dismiss toas');
    });

    toast.present();
  }

  goToProduct(product){
    this.navCtrl.push(ProductPage, {
      namaProduct:    product.f2,
      detailProduct:  product.f3,
      img0Product:    product.f4,
      img1Product:    product.f5,
      f6Product:      product.f6
    });
  }

  cart(){
    this.navCtrl.push(KeranjangPesananPage);
  }

}
