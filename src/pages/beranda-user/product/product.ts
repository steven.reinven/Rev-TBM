import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController, ModalController, Platform } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { KeranjangPesananPage } from "./keranjang-pesanan/keranjang-pesanan";
import { DataProvider } from "../../../providers/data/data";
import { DetailInformationProductPage } from "./detail-information-product/detail-information-product";
import { ProductBrosurPage } from "./product-brosur/product-brosur";
import { ToastController } from 'ionic-angular';
import { LoginProvider } from '../../../providers/login/login';
import { BerandaUserCartProvider } from '../../../providers/beranda-user-cart/beranda-user-cart';
import { TabsPage } from '../../tabs/tabs';


/**
 * Generated class for the ProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  platformStatus;
  qyt: number = 1;

  
  orderForm: FormGroup;
  flag: string; //UNTUK MANAMPUNG CATEGORY databasenya

  lengthOfdata;
  produk: string;
  jumlah: number=1;
  other;
  qtyValidator: boolean = false;
  disableOrderButton:  boolean = false;


  //sqlite
    namaProductdariberandauser;
    detailProductdariberandauser;
    image0Productdariberandauser;
    image1Productdariberandauser;
    f6Productdariberandauser;
    sizeProductdariberandauser;
    packProductdariberandauser;

    //untuk masuk ke keranjang-pesanan
      datalogindaridatabase;
      tampungdatalogindaridatabase;
      TITLEID:string;
      USERNAME:string;
      NAMAPT:string;
      CUSTID:string;
      AGRONOMIS:string;
      FA:string;
      datauntukms_product_cart=[];

      //untuk persiapan ke sqlite  
        tempUkuran;
        tempPack;
        tempId;
        
        dataproductcartdaridatabase;
        tampungdataproductcartdaridatabase;
      
        datajsonnyaproductcartdaridatabase;
        datadetaildarijsonnyaproductcartdatabase;
        formatdetaildatauntukms_product_cart=[];
      //untuk persiapan ke sqlite
    //untuk masuk ke keranjang-pesanan
  //sqlite


  //=====CONSTRUCTOR=====//
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public builder: FormBuilder,
    public databasenya: LoginProvider,
    public app: App,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public dataProvider: DataProvider,
    public databasecartnya: BerandaUserCartProvider,
    public platform: Platform,
    public toastCtrl: ToastController
  ) {

    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    }); 
    

   

    this.namaProductdariberandauser     = this.navParams.get("namaProduct");
    this.detailProductdariberandauser   = this.navParams.get("detailProduct");
    this.image0Productdariberandauser   = this.navParams.get("img0Product");
    this.image1Productdariberandauser   = this.navParams.get("img1Product");
    this.f6Productdariberandauser       = this.navParams.get("f6Product");
    this.sizeProductdariberandauser     = this.navParams.get("sizeProduct");
    this.packProductdariberandauser     = this.navParams.get("packProduct");

    console.log("flag provider: ", this.databasenya.flag);
    this.flag = this.databasenya.flag;

    this.orderForm = builder.group({
      'qty': ['', Validators.required],
      'other': ['', Validators.required]
    });

  }
   //==========END OF CONSTRUCTOR============//

  //=======CARA SET VALUE FORMGROUP LEWAT FORMBUILDER====//
  ngOnInit(){
    this.orderForm = this.builder.group({qty:1, other: 0});
  }
  //====================================================//
 

  
  //ini ionviewall
    ionViewWillEnter(){
      console.log("ionViewWillEnter product!");
      this.databasecartnya.untuknotifcart();
    }
  //ini ionviewall

  toast;
  goToKeranjangPemesanan(id,jsonnya){
    this.jumlah = +this.orderForm.controls.qty.value;
    if(this.jumlah <= 0){
      console.log("jumlah tidak benar");
      this.qtyValidator = true;
    }
    else if(isNaN(this.jumlah)){
      this.qtyValidator = true;
    }
    else{
      this.qtyValidator = false;
      
      this.produk = this.namaProductdariberandauser;
      this.other = this.orderForm.controls.other.value;
      this.putkecart(id,jsonnya);
      
      this.toast = this.toastCtrl.create({
        message: this.orderForm.controls.qty.value + " Box " + this.produk,
        duration: 1000,
        position: "middle",
        dismissOnPageChange: true,
      });
      this.toast.present({
        temp: this.disableOrderButton = true
      });

      this.toast.onDidDismiss(()=> {
        this.disableOrderButton = false;
      });

    }
  }

  cart(){
    this.navCtrl.push(KeranjangPesananPage);
    this.dataProvider.temptTrigger = false;
  }

  
  showDetailInformation(){
    let modal = this.modalCtrl.create(DetailInformationProductPage, {
      namaProduct: this.namaProductdariberandauser,
      imgProduct: this.image0Productdariberandauser
    });
    modal.present();
  }

  showProductBrosur(){
    let modal = this.modalCtrl.create(ProductBrosurPage, {
      namaProduct: this.namaProductdariberandauser
    });
    modal.present();
  }



  //sqlite


    putkecart(id,jsonnya){
      console.log("di product.ts putkecart start");

      this.other = this.orderForm.controls.other.value;
      this.tempUkuran = this.f6Productdariberandauser[this.other].ukuran;// tampung si ukuran buat tembak ke API
      this.tempPack = this.f6Productdariberandauser[this.other].pack;// tampung si pack buat tembak ke API
      this.tempId = this.f6Productdariberandauser[this.other].id;
      console.log("====Pack: ",  this.tempPack);
      console.log("====Ukuran: ",  this.tempUkuran);

      this.databasenya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di product.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;
        this.NAMAPT=this.tampungdatalogindaridatabase[0].namapt;
        this.CUSTID=this.tampungdatalogindaridatabase[0].custid;
        this.AGRONOMIS=this.tampungdatalogindaridatabase[0].agronomis;
        this.FA=this.tampungdatalogindaridatabase[0].fa;

      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart  
        this.databasecartnya.showms_product_cart().then((result) => {
          this.dataproductcartdaridatabase = <Array<Object>> result;
          console.log("di product.ts ms_product_cart has been Loaded");
          this.tampungdataproductcartdaridatabase=this.dataproductcartdaridatabase;
  
          if(this.tampungdataproductcartdaridatabase.length != 0 ){
            this.datajsonnyaproductcartdaridatabase=JSON.parse(this.tampungdataproductcartdaridatabase[0].jsonnya);
            console.log("di product.ts ini ms_product_cart jsonnya tidak kosong");
            this.databasecartnya.untuknotifcart();
            console.log("di product.ts qtynya",this.databasecartnya.jumlahbox);
            this.datauntukms_product_cart=[
              {
                // "sono": "SO/17/1/008",
                "sodate": new Date().toISOString().substring(0, 10),
                "custid": this.CUSTID,
                "email" : this.USERNAME,
                "custname": this.NAMAPT,
                "agronomis": this.AGRONOMIS,
                "fa": this.FA,
                "notes": "",
                "totalqty": this.databasecartnya.jumlahbox+this.jumlah,
                "status": "Pesanan dalam proses",
                "createby": this.USERNAME,
                "createdate": new Date().toISOString(),
                "updateby": this.USERNAME,
                "updatedate": new Date().toISOString(),
                "detail": this.datajsonnyaproductcartdaridatabase[0].detail,
              }
            ];

            this.datauntukms_product_cart[0].detail[this.databasecartnya.notif]={
              "sqno": this.databasecartnya.notif,
              "prodcode": this.tempId,
              "prodname": this.produk,
              "qtytype": this.tempUkuran + " / " + this.tempPack,
              "qty": this.jumlah,
              "notes": ""  
            };
      
            console.log("di product.ts ini datauntukms_product_cart string",JSON.stringify(this.datauntukms_product_cart));
              this.databasecartnya.putms_product_cart(
                id=1,
                jsonnya=JSON.stringify(this.datauntukms_product_cart)
                
              ).then((result) => {
                this.databasecartnya.untuknotifcart();
                console.log("di product.ts putms_product_cart finnish");

                this.app.getRootNav().setRoot(TabsPage);
                console.log("=====(OK)di product.ts tombol keranjang pesanan ok ke beranda user====");

              },(error) => {
                console.log("di product.ts ERROR: putms_product_cart", error);
              });

          }else{
            console.log("di product.ts ini ms_product_cart jsonnya kosong");
            this.datauntukms_product_cart=[
              {
                "sono": "SO/17/1/008",
                "sodate": new Date().toISOString().substring(0, 10),
                "custid": this.CUSTID,
                "email" : this.USERNAME,
                "custname": this.NAMAPT,
                "agronomis": this.AGRONOMIS,
                "fa": this.FA,
                "notes": "",
                "totalqty": 1,
                "status": "Pesanan dalam proses",
                "createby": this.USERNAME,
                "createdate": new Date().toISOString(),
                "updateby": this.USERNAME,
                "updatedate": new Date().toISOString(),
                "detail": [{
                  "sqno": 0,
                  "prodcode": this.tempId,
                  "prodname": this.produk,
                  "qtytype": this.tempUkuran,
                  "qty": this.jumlah,
                  "notes": ""  
                }],
              }
            ];
            console.log("di product.ts ini datauntukms_product_cart string",JSON.stringify(this.datauntukms_product_cart));
              this.databasecartnya.putms_product_cart(
                id=1,
                jsonnya=JSON.stringify(this.datauntukms_product_cart)
                
              ).then((result) => {
                this.databasecartnya.untuknotifcart();
                console.log("di product.ts putms_product_cart finnish");
              },(error) => {
                console.log("di product.ts ERROR: putms_product_cart", error);
              });
          }
         
        },(error) => {
            console.log("di product.ts ERROR: showms_product_cart", error);
        });
      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart
  
        
      },(error) => {
          console.log("di product.ts ERROR: showms_user", error);
      });

    }
    
  //sqlite

}
