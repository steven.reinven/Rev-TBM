import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App } from 'ionic-angular';
import { TabsPage } from "../../../../tabs/tabs";
import { LoginPage } from "../../../../login/login";
import { BerandaUserPage } from '../../../beranda-user';
import { LoginProvider } from '../../../../../providers/login/login';

/**
 * Generated class for the KonfirmasiPesananPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-konfirmasi-pesanan',
  templateUrl: 'konfirmasi-pesanan.html',
})
export class KonfirmasiPesananPage {

  detailnonya;
  sononya;
  x=0;
  y=0;
  rootPage:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public app: App,public databasenya: LoginProvider) {
  
    this.detailnonya= this.navParams.get("detailno");
    console.log("ini detailno",this.detailnonya);
    
    for(this.x=0; this.x<this.detailnonya.length; this.x++){
      this.sononya=this.detailnonya[this.x].sono;
    }

    console.log("ini sono",this.sononya);
    


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KonfirmasiPesananPage');
  }

  goToHome(){
    this.viewCtrl.dismiss();
    // location.reload();
    // this.databasenya.flag="user@gmail.com";
    // this.navCtrl.setRoot(TabsPage);
    // this.rootPage=TabsPage;
    this.app.getRootNav().setRoot(TabsPage);
    console.log("di konfirmasi-pesanan.ts ok");
    // location.reload();
    // this.navCtrl.popToRoot();
  }

}
