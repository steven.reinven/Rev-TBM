import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, App, ViewController, LoadingController } from 'ionic-angular';
import { KonfirmasiPesananPage } from "./konfirmasi-pesanan/konfirmasi-pesanan";
import { DataProvider } from "../../../../providers/data/data";
import { TabsPage } from "../../../tabs/tabs";
import { AlertController, ToastController } from 'ionic-angular';
import { KeranjangPesananProvider } from '../../../../providers/keranjang-pesanan/keranjang-pesanan';
import { LoginProvider } from '../../../../providers/login/login';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BerandaUserCartProvider } from '../../../../providers/beranda-user-cart/beranda-user-cart';
import { Network } from '@ionic-native/network';
import { PesananUserProvider } from "../../../../providers/pesanan-user/pesanan-user";

/**
 * Generated class for the KeranjangPesananPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-keranjang-pesanan',
  templateUrl: 'keranjang-pesanan.html',
})
export class KeranjangPesananPage {

  tempNamaProduk;
  tempJumlah: number;
  lengthData;


  //====dataProvider====//
  totalBox: number;
  //====================//


  
  //sqlite
    tampungi;

      //untuk tembak api
      datalogindaridatabase;
      tampungdatalogindaridatabase;
      TOKEN;
      JSONNYA;

      datakeranjangpesanandaridatabase;
      tampungdatakeranjangpesanandaridatabase;
      JSONNYAjsonnya;
      JSONNYAjsonnyaDETAIL;

      dataproductcartdaridatabase;
      tampungdataproductcartdaridatabase;
      datajsonnyaproductcartdaridatabase;
      
      datauntukms_product_cart=[];
      detailuntukms_product_cart=[];
    
      datadetailuntukms_product_cart;
      tampungdatadetailuntukms_product_cart;
    
      USERNAME;
      TITLEID;
      x=0;
      y=0;
      z=0;
    //untuk tembak api
  //sqlite


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController, 
    public app: App,
    public viewCtrl: ViewController,
    public dataProvider: DataProvider,
    public databasenya: LoginProvider,
    public databasekeranjangpesanan:KeranjangPesananProvider,
    public databasecartnya: BerandaUserCartProvider,
    public pesananUserProvider: PesananUserProvider,
    public http: Http,
    private network: Network,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {


  }

  //===========================UNTUK HITUNG TOTAL BOX==========================//
  // hitungTotal(){
  //   console.log("=================== hitungTotal()====================");
  //   this.dataProvider.totalPemesanan = 0;
  //   this.lengthData = this.dataProvider.pesanan.length
  //   let x;
  //   for(x=0; x < this.lengthData; x++){
  //     console.log("jumlah", this.dataProvider.pesanan[x].jumlah);
  //     this.dataProvider.totalPemesanan += this.dataProvider.pesanan[x].jumlah;
  //   }
  //   console.log("totalpesanan dataProvider: ", this.dataProvider.totalPemesanan);
  //   console.log("=================== end of hitungTotal()================");
  // }
  //==========================================================================//

  ionViewWillEnter() {
    console.log('ionViewWillEnter KeranjangPesananPage');
    this.loaddaridatabase();
  }

  //===========================UNTUK PUSH KE KonfirmasiPesananPage==========================//
  goToKonfirmasiTambahPemesanan(id,jsonnya){
    
    let confirm = this.alertCtrl.create({
      title: "Konfirmasi Pesanan",
      message: `Apakah anda yakin ingin menyelesaikan pesanan?`,
      buttons: [
        {
          text: 'Ya',
          handler: ()=>{

            //ini kirim ke api
              this.databasenya.showms_user().then((result) => {
                this.datalogindaridatabase = <Array<Object>> result;
                console.log("di keranjang-pesanan.ts ms_user has been Loaded");
                this.tampungdatalogindaridatabase=this.datalogindaridatabase;
                this.TOKEN=this.tampungdatalogindaridatabase[0].token;
          
                this.databasekeranjangpesanan.showms_product_cart().then((result) => {
                  this.datakeranjangpesanandaridatabase = <Array<Object>> result;
                  console.log("di keranjang-pesanan.ts ms_product_cart has been Loaded");
                  this.tampungdatakeranjangpesanandaridatabase=this.datakeranjangpesanandaridatabase;
                  this.JSONNYA=this.tampungdatakeranjangpesanandaridatabase[0].jsonnya;
                  // console.log("ini jsonnya",this.JSONNYA);
                
                  if(
                    this.network.type === 'none'
                  ||this.network.type === 'unknown'){
                  console.log("di keranjang-pesanan.ts ini none atau unknown network");
                    let toast = this.toastCtrl.create({
                      message: 'Tidak Terkoneksi',
                      duration: 2000,
                      position: 'top'
                    });
                    toast.present();
                  }else if(
                      this.network.type === 'wifi' 
                    ||this.network.type === 'cellular'
                    ||this.network.type === '4g'
                    ||this.network.type === '3g'
                    ||this.network.type === '2g'
                    ||this.network.type === 'ethernet'){
                    console.log("di keranjang-pesanan.ts ini connect");
                    let loading = this.loadingCtrl.create({
                      content: 'Silahkan menunggu'
                    });
                  
                    loading.present();

                    let body  = this.JSONNYA;
                    console.log("intip",'http://api.nex.web.id/pestisida/api.php?q=20&token='+this.TOKEN,body);
                    this.http.post('http://api.nex.web.id/pestisida/api.php?q=20&token='+this.TOKEN,body).subscribe(res=>{
                      console.log("respon status dari api",res);
                      console.log("respon dari api",JSON.stringify(res.json())); 

                      setTimeout(() => {
                        loading.dismiss();
                        
                        this.pesananUserProvider.flagAPIPesanan = true;
                         //ini untuk triger tarik data pesanan
                        //==========================================================
                          //kirim data ke data base, karena pesanan sudah di konfirmasi
                          let modal = this.modalCtrl.create(KonfirmasiPesananPage,{
                            detailno: res.json()
                          });
                          modal.present();
                          this.navCtrl.pop();
                          //kosongkan semua data json local pemesanan dan semua notif==
                          this.hapussemuadimsproductcart(id,jsonnya);
                          // this.dataProvider.notif = this.dataProvider.pesanan.length;
                        //===========================================================   

                      }, 3000);

                     
                    },(err)=>{
                        console.log("ERROR respon dari api",JSON.stringify(err.json()));
                        console.log("ERROR respon status dari api",err);
                        let toast = this.toastCtrl.create({
                          message: 'Server Down',
                          duration: 2000,
                          position: 'middle'
                        });
                        toast.present();
                    });
                    
                  }else{
                    console.log("di keranjang-pesanan.ts connect aneh");
                    let toast = this.toastCtrl.create({
                      message: 'Tidak Terkoneksi',
                      duration: 2000,
                      position: 'top'
                    });
                    toast.present();
                  }

                },(error) => {
                    console.log("di keranjang-pesanan.ts ERROR: showms_product_cart", error);
                });
                
              },(error) => {
                  console.log("di keranjang-pesanan.ts ERROR: showms_user", error);
              });
            //ini kirim ke api            
          }
        },
        {
          text: 'Tidak'
        }
      ]
    });
    confirm.present();
  
    // console.log("konfirmed temptData at KeranjangPesanan: ", this.dataProvider.temptTrigger);
    // this.dataProvider.totalPemesanan = this.totalBox;
    // console.log("dataProvider.totalPemesanan: ", this.dataProvider.totalPemesanan);
    // console.log("dataprovider: ", this.dataProvider);
  }
  

  // total(){
  //   console.log("======================total()======================");
  //   this.lengthData = this.dataProvider.pesanan.length;//2
  //   console.log("lenght: ", this.lengthData);
  
  //   this.totalBox = null;
  
  //    let x=0;
  //    for(x = 0; x < this.lengthData; x++) {
  //       this.totalBox  = this.totalBox + this.dataProvider.pesanan[x].jumlah;
  //      console.log("total box: ",this.totalBox );
  //     }
  // }

  //===========================REMOVE=========================//
  remove(i,id,jsonnya){
    console.log("index selected: ", i);
    let confirmRemove = this.alertCtrl.create({
      title: 'Hapus pesanan',
      message: 'Apakah anda yakin ingin menghapus pemesanan ' + this.JSONNYAjsonnyaDETAIL[i].prodname + ' sebanyak ' + this.JSONNYAjsonnyaDETAIL[i].qty + ' Box?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
              // this.dataProvider.pesanan.splice(i, 1);
              // console.log("data skr: ", this.dataProvider.pesanan);
              // this.hitungTotal();
              this.tampungi=0;
              this.tampungi=i;
              this.hapus1dimsproductcart(id,jsonnya);

          }
        },
        {
          text: 'Batal'
        }
      ]
    });
    confirmRemove.present();
    
  }
  //=========================================================//
  

  //===========================REMOVE ALL=========================//
  removeAll(id,jsonnya){
    let confirmRemoveAll = this.alertCtrl.create({
      title: 'Hapus semua pesanan?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
           
            this.hapussemuadimsproductcart(id,jsonnya);
             
          }
        },
        {
          text: 'Batal'
        }
      ]
    });
    confirmRemoveAll.present();
  }
  //==============================================================//

  //==============================hapus semua data==================
  hapusSemuaData(){
    console.log("Remove all");
    let length = this.dataProvider.pesanan.length;
    console.log("length: ", length);
    this.dataProvider.pesanan.splice(0, length);
  }


  //sqlite
    loaddaridatabase(){
      console.log("di keranjang-pesanan.ts loaddaridatabase start");
      this.databasecartnya.untuknotifcart();
      console.log("di keranjang-pesanan.ts ini jumlah box nya",this.databasecartnya.jumlahbox);
      this.databasenya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di keranjang-pesanan.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;
        this.TOKEN=this.tampungdatalogindaridatabase[0].token;

        this.databasekeranjangpesanan.showms_product_cart().then((result) => {
          this.datakeranjangpesanandaridatabase = <Array<Object>> result;
          console.log("di keranjang-pesanan.ts ms_product_cart has been Loaded");
          this.tampungdatakeranjangpesanandaridatabase=this.datakeranjangpesanandaridatabase;
          
          if(this.tampungdatakeranjangpesanandaridatabase[0].jsonnya.length != 0){
            this.JSONNYA=this.tampungdatakeranjangpesanandaridatabase[0].jsonnya;
            console.log("di keranjang-pesanan.ts ini jsonnya",this.JSONNYA);
            this.JSONNYAjsonnya=JSON.parse(this.tampungdatakeranjangpesanandaridatabase[0].jsonnya);
            this.JSONNYAjsonnyaDETAIL=this.JSONNYAjsonnya[0].detail;
            console.log("di keranjang-pesanan.ts ada data jsonnya");
          }else{

            console.log("di keranjang-pesanan.ts tidak ada data jsonnya");
          }
        
        
          // console.log("ini detail semua",this.JSONNYAjsonnyaDETAIL);
          // console.log("ini detail semua stringify",JSON.stringify(this.JSONNYAjsonnyaDETAIL));

        },(error) => {
            console.log("di keranjang-pesanan.ts ERROR: showms_product_cart", error);
        });
        
      },(error) => {
          console.log("di keranjang-pesanan.ts ERROR: showms_user", error);
      });
    }

    hapus1dimsproductcart(id,jsonnya){
      console.log("di keranjang-pesanan.ts hapus1dimsproductcart start");
      this.databasenya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di keranjang-pesanan.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;

      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart  
        this.databasecartnya.showms_product_cart().then((result) => {
          this.dataproductcartdaridatabase = <Array<Object>> result;
          console.log("di keranjang-pesanan.ts ms_product_cart has been Loaded");
          this.tampungdataproductcartdaridatabase=this.dataproductcartdaridatabase;

          if(this.tampungdataproductcartdaridatabase.length != 0 ){
            this.datajsonnyaproductcartdaridatabase=JSON.parse(this.tampungdataproductcartdaridatabase[0].jsonnya);
            console.log("di keranjang-pesanan.ts ini ms_product_cart jsonnya tidak kosong");
            this.databasecartnya.untuknotifcart();
            console.log("di keranjang-pesanan.ts qtynya",this.databasecartnya.jumlahbox);

            this.datadetailuntukms_product_cart=this.datajsonnyaproductcartdaridatabase[0].detail;

            console.log("ini intip indeknya",this.tampungi);
            console.log("di keranjang-pesanan.ts ini pilihan hapus",JSON.stringify(this.datadetailuntukms_product_cart[this.tampungi]));

            for(this.x=0; this.x<this.datadetailuntukms_product_cart.length; this.x++){
              
              if(this.datadetailuntukms_product_cart[this.tampungi].sqno == this.datadetailuntukms_product_cart[this.x].sqno){
                this.datadetailuntukms_product_cart.splice(this.tampungi,1);

                this.detailuntukms_product_cart=[];
                for(this.y=0; this.y<this.datadetailuntukms_product_cart.length; this.y++){
                  this.detailuntukms_product_cart[this.y]={
                      "sqno": this.y,
                      "prodcode": this.datadetailuntukms_product_cart[this.y].prodcode,
                      "prodname": this.datadetailuntukms_product_cart[this.y].prodname,
                      "qtytype": this.datadetailuntukms_product_cart[this.y].qtytype,
                      "qty": this.datadetailuntukms_product_cart[this.y].qty,
                      "notes": this.datadetailuntukms_product_cart[this.y].notes 
                    };
                }
                // console.log("amburamesino string",JSON.stringify(this.detailuntukms_product_cart));
              }else{
                console.log("di keranjang-pesanan.ts aneh tidak ada data yang cocok buat di delete");
              }

            }
            console.log("di keranjang-pesanan.ts ini data detail untuk string",JSON.stringify( this.datadetailuntukms_product_cart));
            this.databasecartnya.untuknotifcart();
            this.datauntukms_product_cart=[
              {
                "sono": this.datajsonnyaproductcartdaridatabase[0].sono,
                "sodate": new Date().toISOString().substring(0, 10),
                "custid": this.TITLEID,
                "email" : this.USERNAME,
                "custname": this.datajsonnyaproductcartdaridatabase[0].custname,
                "agronomis": this.datajsonnyaproductcartdaridatabase[0].agronomis,
                "fa": this.datajsonnyaproductcartdaridatabase[0].fa,
                "notes": this.datajsonnyaproductcartdaridatabase[0].notes,
                "totalqty": this.databasecartnya.jumlahbox,
                "status": this.datajsonnyaproductcartdaridatabase[0].status,
                "createby": this.USERNAME,
                "createdate": new Date().toISOString().substring(0, 10),
                "updateby": this.USERNAME,
                "updatedate": new Date().toISOString().substring(0, 10),
                "detail": this.detailuntukms_product_cart,
              }
            ];

            console.log("di keranjang-pesanan.ts ini datauntukms_product_cart string",JSON.stringify(this.datauntukms_product_cart));
              this.databasecartnya.putms_product_cart(
                id=1,
                jsonnya=JSON.stringify(this.datauntukms_product_cart)
                
              ).then((result) => {
                this.loaddaridatabase();
                this.databasecartnya.untuknotifcart();
                console.log("di keranjang-pesanan.ts putms_product_cart finnish");
              },(error) => {
                console.log("di keranjang-pesanan.ts ERROR: putms_product_cart", error);
              });

          }else{
            console.log("di keranjang-pesanan.ts ini ms_product_cart jsonnya kosong tidak mungkin button hapus 1 1 ada");
          }
        
        },(error) => {
            console.log("di keranjang-pesanan.ts ERROR: showms_product_cart", error);
        });
      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart

        
      },(error) => {
          console.log("di keranjang-pesanan.ts ERROR: showms_user", error);
      });
    }

    hapussemuadimsproductcart(id,jsonnya){
      console.log("di keranjang-pesanan.ts hapusemuadimsproductcart start");
      this.databasenya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di keranjang-pesanan.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;

      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart  
        this.databasecartnya.showms_product_cart().then((result) => {
          this.dataproductcartdaridatabase = <Array<Object>> result;
          console.log("di keranjang-pesanan.ts ms_product_cart has been Loaded");
          this.tampungdataproductcartdaridatabase=this.dataproductcartdaridatabase;

          if(this.tampungdataproductcartdaridatabase.length != 0 ){
            this.datajsonnyaproductcartdaridatabase=JSON.parse(this.tampungdataproductcartdaridatabase[0].jsonnya);
            console.log("di keranjang-pesanan.ts ini ms_product_cart jsonnya tidak kosong");
            this.databasecartnya.untuknotifcart();
            console.log("di keranjang-pesanan.ts qtynya",this.databasecartnya.jumlahbox);

            this.datadetailuntukms_product_cart=this.datajsonnyaproductcartdaridatabase[0].detail;

            console.log("ini intip indeknya",this.tampungi);
            console.log("di keranjang-pesanan.ts ini pilihan hapus",JSON.stringify(this.datadetailuntukms_product_cart[this.tampungi]));

            for(this.x=0; this.x<this.datadetailuntukms_product_cart.length; this.x++){
                this.datadetailuntukms_product_cart.splice(0,this.datadetailuntukms_product_cart.length);
                this.detailuntukms_product_cart=[];
            }
            this.datauntukms_product_cart=[
              {
                "sono": this.datajsonnyaproductcartdaridatabase[0].sono,
                "sodate": new Date().toISOString().substring(0, 10),
                "custid": this.TITLEID,
                "email" : this.USERNAME,
                "custname": this.datajsonnyaproductcartdaridatabase[0].custname,
                "agronomis": this.datajsonnyaproductcartdaridatabase[0].agronomis,
                "fa": this.datajsonnyaproductcartdaridatabase[0].fa,
                "notes": this.datajsonnyaproductcartdaridatabase[0].notes,
                "totalqty": 0,
                "status": this.datajsonnyaproductcartdaridatabase[0].status,
                "createby": this.USERNAME,
                "createdate": new Date().toISOString().substring(0, 10),
                "updateby": this.USERNAME,
                "updatedate": new Date().toISOString().substring(0, 10),
                "detail": this.detailuntukms_product_cart,
              }
            ];

            console.log("di keranjang-pesanan.ts ini datauntukms_product_cart string",JSON.stringify(this.datauntukms_product_cart));
              this.databasecartnya.putms_product_cart(
                id=1,
                jsonnya=JSON.stringify(this.datauntukms_product_cart)
                
              ).then((result) => {
                this.loaddaridatabase();
                this.databasecartnya.untuknotifcart();
                console.log("di keranjang-pesanan.ts putms_product_cart finnish");
              },(error) => {
                console.log("di keranjang-pesanan.ts ERROR: putms_product_cart", error);
              });

          }else{
            console.log("di keranjang-pesanan.ts ini ms_product_cart jsonnya kosong tidak mungkin button hapus 1 1 ada");
          }
        
        },(error) => {
            console.log("di keranjang-pesanan.ts ERROR: showms_product_cart", error);
        });
      //ini untuk ambil data lama trus masukin ke datauntukms_product_cart

        
      },(error) => {
          console.log("di keranjang-pesanan.ts ERROR: showms_user", error);
      });

    }
  //sqlite


}
