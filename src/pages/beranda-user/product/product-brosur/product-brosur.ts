import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProductBrosurPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-brosur',
  templateUrl: 'product-brosur.html',
})
export class ProductBrosurPage {

  namaProduct;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.namaProduct = this.navParams.get("namaProduct");
    console.log("nama Product: ", this.namaProduct);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductBrosurPage');
  }

}
