import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailInformationProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-information-product',
  templateUrl: 'detail-information-product.html',
})
export class DetailInformationProductPage {

  namaProduct;
  imgProduct;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.namaProduct = this.navParams.get("namaProduct");
    this.imgProduct = this.navParams.get("imgProduct");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailInformationProductPage');
  }

}
