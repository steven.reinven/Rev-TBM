import { Component } from '@angular/core';


import { PesananPage } from "../pesanan/pesanan";
import { PengaturanPage } from "../pengaturan/pengaturan";
import { AlertController } from "ionic-angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { BerandaManajemenPage } from "../beranda-manajemen/beranda-manajemen";
import { TransaksiManajemenPage } from "../transaksi-manajemen/transaksi-manajemen";
import { DataProvider } from "../../providers/data/data";
import { BerandaUserPage } from "../beranda-user/beranda-user";
import { LoginProvider } from '../../providers/login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  //===user, guest==//
  tab1Root = BerandaUserPage;
  tab2Root = PesananPage;
  tab3Root = PengaturanPage;
  badgeValue: any;

  //===manajemen===/
  tab1RootManajemen = BerandaManajemenPage;
  tab2RootManajemen = TransaksiManajemenPage;
  setting: boolean;

  constructor(public databasenya: LoginProvider, public alertCtrl: AlertController, public dataProvider: DataProvider) {
     this.setting=true;
      
    if(this.dataProvider.notificationManajemenTransaksi != 0){
      this.dataProvider.notifTab = 0;
      this.dataProvider.notifTab = this.dataProvider.notificationManajemenTransaksi;
    }
    else{
      this.dataProvider.notifTab = "";
    }
     
  }


  
}
