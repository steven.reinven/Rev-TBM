import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailTransaksiPage } from "./detail-transaksi/detail-transaksi";
import { DataProvider } from "../../providers/data/data";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { Http } from '@angular/http';
import { ManajemenTransaksiProvider } from "../../providers/manajemen-transaksi/manajemen-transaksi";
import { Network } from "@ionic-native/network";
import { LoginProvider } from "../../providers/login/login";
import { LoadingController, ToastController } from 'ionic-angular';

/**
 * Generated class for the TransaksiManajemenPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-transaksi-manajemen',
  templateUrl: 'transaksi-manajemen.html',
})
export class TransaksiManajemenPage {

// ====DECLARE VARIABEL====
  transaksiTab: string;
  statusTab: string="pesanan";
  statusTab2: string="pesanan";
  tabForm: FormGroup;
  searchForm: FormGroup;

  segmentvalidasi:string;
  
  datalogindaridatabase;
  tampungdatalogindaridatabase;

  USERNAME;
//=========================

//====CONSTRUCTOR===
  constructor(
    //INJECT
      public navCtrl: NavController, 
      public navParams: NavParams,
      public dataProvider: DataProvider, 
      public manajemenTransaksiService: ManajemenTransaksiProvider,
      public builder: FormBuilder,
      private network: Network,
      public databasenya: LoginProvider,
      public loadingCtrl: LoadingController,
      public toastCtrl: ToastController,
      public http: Http
    //======
  ) {

      this.segmentvalidasi="";

      this.databasenya.showms_user().then((result) => {
      this.datalogindaridatabase = <Array<Object>> result;
      console.log("di transaksi-manajemen.ts ms_user has been Loaded");
      this.tampungdatalogindaridatabase=this.datalogindaridatabase;

      this.USERNAME=this.tampungdatalogindaridatabase[0].username;
      console.log("===construktor USERNAME: ", this.USERNAME);
      
    },(error) => {
        console.log("di transaksi-manajemen.ts ERROR: showms_user", error);
    });

  //====ISI CONSTRUCTOR====
    if(this.databasenya.flag == "manajemen@gmail.com"){
      console.log("ini manajemen");
      this.segmentvalidasi="manajemen@gmail.com";
      this.transaksiTab="pt";
      this.manajemenTransaksiService.flagTransaksiTab = this.transaksiTab;
    }else if(this.databasenya.flag == "agronomis&FA(6)@gmail.com"){
      console.log("ini agro");
      this.segmentvalidasi="agronomis&FA(6)@gmail.com";
      this.transaksiTab="pt";
      this.manajemenTransaksiService.flagTransaksiTab = this.transaksiTab;
    }else if(this.databasenya.flag == "agronomis&FA(7)@gmail.com"){
      console.log("ini agro");
      this.segmentvalidasi="agronomis&FA(7)@gmail.com";
      this.transaksiTab="pt";
      this.manajemenTransaksiService.flagTransaksiTab = this.transaksiTab;
    }else if(this.databasenya.flag == "distributor@gmail.com"){
      console.log("ini distributor");
      this.segmentvalidasi="distributor@gmail.com";
      this.transaksiTab="distributor";
      this.manajemenTransaksiService.flagTransaksiTab = this.transaksiTab;
    }else{
      console.log("di transaksi-manajemen.ts belum terdaftar");
      console.log("ini amburadul");
    }
  
    console.log("status tab: ", this.statusTab);// UNTUK DETEKSI SEGMENT MANA YANG TERPILIH

    //FORM UNTUK PEMBAGIAN SEGMENT pt BERDASARKAN STATUS
    this.tabForm = builder.group({
      'tabValue': ['', Validators.required] 
    });

    this.searchForm = builder.group({
      'searchBar': ['']
    });

    // DECLARE NILAI SI SEGMENT pt BERDASARKAN STATUS
    this.tabForm = this.builder.group({tabValue: 'pesanan'});
    console.log("tabForm: ", this.tabForm.controls.tabValue.value);

    // }
  //=======================
  }
// =================

  //TEMP UNTUK DATA JSON SEGMENT pt YANG SUDAH DI PECAH

  //====FUNCTION PUSH KE DETAIL TRANSAKSI PAGE====
  tempStatus: string;
    goToDetailTransaksi(y, i){
      if(this.transaksiTab == 'pt'){
        this.tempStatus = this.tabForm.controls.tabValue.value;
        console.log("===tempStatus: ", this.tempStatus);
      }
      else{
        this.tempStatus = "kosong";
        console.log("===tempStatus: ", this.tempStatus);
      }
      this.navCtrl.push(DetailTransaksiPage, {
        pt: y.f4,
        tanggalTransaksi: y.f2,
        order: y.f7,
        catatan: y.f6,
        index: i, 
        id: y.f1,
        tglKirim: y.f9,
        noSuratJalan: y.f8,
        noSo: y.f10, 
        noInvoice: y.f11,
        status: this.tempStatus,
        transaksiTab: this.transaksiTab,
        segmentvalidasi: this.segmentvalidasi,
      });

      console.log("nama pt: ", y.f4);
      console.log("tanggal transaksi: ", y.f2);
      console.log("berikut data order: ", y.f7);
      console.log("catatan: ", y.f6);
      console.log("index: ", i);
      console.log("id: ", y.f1);
      console.log("tglKirim: ", y.f9);
      console.log("noSutratJalan: ", y.f8);
      console.log("noInvoice: ", y.f10);
      console.log("status: ", this.tempStatus);
      console.log("id: ", y.f1);
      console.log("transaksiTab: ", y.this.transaksiTab);
      console.log("segmentvalidasi: ", this.segmentvalidasi);
    } 
  //==============================================

  ionViewDidLoad(data_id, data_json) {
    console.log('ionViewDidLoad beranda-manajemen di beranda-manajemen.ts');
    this.reset_load_more();
    if(
        this.network.type === 'none'
      ||this.network.type === 'unknown'){
      console.log("ini tidak connect");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      if(this.segmentvalidasi == "manajemen@gmail.com" || this.segmentvalidasi == "agronomis&FA(6)@gmail.com" || this.segmentvalidasi == "agronomis&FA(7)@gmail.com"){
        this.get_token(data_id, data_json);
        this.changeTabs(data_id, data_json);
      }else if(this.segmentvalidasi =="distributor@gmail.com"){
        this.get_token(data_id, data_json);
        this.triger_get_distributor(data_id, data_json);
      }else{
        console.log("di transaksi-manajemen.ts belum terdaftar");
      }

    }else if(
            this.network.type === 'wifi'
          ||this.network.type === 'cellular'
          ||this.network.type === '4g'
          ||this.network.type === '3g'
          ||this.network.type === '2g'
          ||this.network.type === 'ethernet'){
      console.log("ini connect");
        if(this.segmentvalidasi == "manajemen@gmail.com" || this.segmentvalidasi == "agronomis&FA(6)@gmail.com" || this.segmentvalidasi == "agronomis&FA(6)@gmail.com"){
          this.get_token(data_id, data_json);
          // this.changeTabs(data_id, data_json);
        }else if(this.segmentvalidasi =="distributor@gmail.com"){
          this.get_token(data_id, data_json);
          this.triger_get_distributor(data_id, data_json);
        }else{
          console.log("di transaksi-manajemen.ts belum terdaftar");
        }
      
    }else{
      console.log("connect aneh");
      let toast = this.toastCtrl.create({
        message: 'Koneksi bermasalah',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.changeTabs(data_id, data_json);
      this.triger_get_distributor(data_id, data_json);
      if(this.segmentvalidasi == "manajemen@gmail.com" || this.segmentvalidasi == "agronomis&FA(6)@gmail.com" || this.segmentvalidasi == "agronomis&FA(7)@gmail.com"){
        this.changeTabs(data_id, data_json);
      }else if(this.segmentvalidasi =="distributor@gmail.com"){
        this.triger_get_distributor(data_id, data_json);
      }else{
        console.log("di transaksi-manajemen.ts belum terdaftar");
      }
    }
  }

  ionViewDidEnter(data_id, data_json){
    console.log("============ionViewDidEnter===========");
    if(
            this.network.type === 'wifi'
          ||this.network.type === 'cellular'
          ||this.network.type === '4g'
          ||this.network.type === '3g'
          ||this.network.type === '2g'
          ||this.network.type === 'ethernet'){
            console.log("===(OK)connection succes===");
            this.changeTabs(data_id, data_json);
          }
    else{
      console.log("===We lost internet connection===");
    }
    
  }

  //====CHANGES TAB====
    	changeTabs(data_id, data_json){
        if(this.tabForm.controls.tabValue.value == "pesanan" ){
          this.manajemenTransaksiService.flagTabPT = this.tabForm.controls.tabValue.value; //flag provider
          console.log("===readyDataPemesanan: ", this.readyDataPemesanan);
          console.log("====this.manajemenTransaksiService.flagAPIPemesanan", this.manajemenTransaksiService.flagAPIPemesanan);
          console.log("===flagRefresh: ", this.flagRefresh);
          if(this.readyDataPemesanan == undefined || this.manajemenTransaksiService.flagAPIPemesanan == true || this.flagRefresh == true){
            console.log("tarik API");
            this.get_data_pemesanan_from_API(data_id, data_json);
            this.manajemenTransaksiService.flagAPIPemesanan = false;
            console.log("====this.manajemenTransaksiService.flagAPIPemesanan", this.manajemenTransaksiService.flagAPIPemesanan);
          } 
          else{
            console.log("Show data");
            this.show_data_pemesanan(data_id, data_json);
          }
          
          //====UNTUK NOTIFIKASI SEGMENT PESANAN DALAM PROSES===
            this.dataProvider.notificationManajemenTransaksi = this.dataProvider.tempPesananDalamProses.length;
          // ===================================================
        }
      //===DIKIRIM====
        else if(this.tabForm.controls.tabValue.value == "dikirim"){
          this.manajemenTransaksiService.flagTabPT = this.tabForm.controls.tabValue.value;
          console.log("===this.manajemenTransaksiService.flagAPIDikirim: ", this.manajemenTransaksiService.flagAPIDikirim);
          if(this.readyDataDikirim == undefined || this.manajemenTransaksiService.flagAPIDikirim == true || this.flagRefresh == true){
            this.get_data_dikirim_from_API(data_id, data_json);
            this.manajemenTransaksiService.flagAPIDikirim = false;
            console.log("===this.manajemenTransaksiService.flagAPIDikirim: ", this.manajemenTransaksiService.flagAPIDikirim);
          } 
          else{
           this.show_data_dikirim(data_id, data_json);
          }
          
          // this.pesananDikirim = this.dataProvider.filterPesananDikirim(this.searchInput);
          // console.log("isi dikirim: ", this.pesananDikirim);
        }
      //==============

      //====DITOLAK====
        else if(this.tabForm.controls.tabValue.value == "ditolak"){
          this.manajemenTransaksiService.flagTabPT = this.tabForm.controls.tabValue.value;
          console.log("===this.manajemenTransaksiService.flagAPIDitolak: ", this.manajemenTransaksiService.flagAPIDitolak);
          if(this.readyDataDitolak == undefined || this.manajemenTransaksiService.flagAPIDitolak == true || this.flagRefresh == true){
            this.get_data_ditolak_from_API(data_id, data_json);
            this.manajemenTransaksiService.flagAPIDitolak = false;
            console.log("===this.manajemenTransaksiService.flagAPIDitolak: ", this.manajemenTransaksiService.flagAPIDitolak);
          } 
          else{
           this.show_data_ditolak(data_id, data_json);
          }
          // this.pesananDitolak = this.dataProvider.filterPesananDitolak(this.searchInput);
          // console.log("isi ditolak: ", this.pesananDitolak);
        }
      //===============

      //====TERKIRIM====
        else{
          this.manajemenTransaksiService.flagTabPT = this.tabForm.controls.tabValue.value;
          if(this.readyDataTerkirim == undefined || this.flagRefresh == true){
            this.get_data_terkirim_from_API(data_id, data_json);
          } 
          else{
           this.show_data_terkirim(data_id, data_json);
          }
          // this.pesananTerkirim = this.dataProvider.filterPesananTerkirim(this.searchInput);
          // console.log("isi terkirim: ", this.pesananTerkirim);
        }
      //===============
    }
  //===================================
  
  //====GET DISTRIBUTOR====
    triger_get_distributor(data_id, data_json){
      if(this.readyDataDistributor == undefined || this.flagRefresh == true){
        this.get_data_distributor_from_API(data_id, data_json);
      }
      else{
        this.show_data_distributor(data_id, data_json);
      }
    }
  //======================

  //====FUNCTION DATABASE============================||
  
    //====token====
        tempToken;
        dataLogin;
        tempDataLogin;
      //+++++++++++++++
      get_token(data_id, data_json){
        console.log("****get TOKEN(manajemen_tansaksi)....READY");
        this.databasenya.showms_user().then((result) => {

          this.dataLogin = <Array<Object>> result;
          console.log("<<< (Y)ms_transaksi_TBM success loaded");

          this.tempDataLogin = this.dataLogin;
          console.log("===SUCCES GET TOKEN, this is the TOKEN (" + this.tempDataLogin[0].token + ")");

          if(this.tempDataLogin[0].token != ""){
            this.tempToken = this.tempDataLogin[0].token;
            let userName = this.tempDataLogin[0].username;
            console.log("****insert TOKEN to tempToken......(" + this.tempDataLogin[0].token + ")");
        
            if(this.segmentvalidasi == "manajemen@gmail.com" || this.segmentvalidasi == "agronomis&FA(6)@gmail.com" || this.segmentvalidasi == "agronomis&FA(7)@gmail.com"){
              if(this.transaksiTab == 'pt'){
                this.changeTabs(data_id, data_json);
              }
            }else if(this.segmentvalidasi == "distributor@gmail.com"){
              // ++++GET DATA FROM API SEGMENT DISTRIBUTOR++++
              this.get_data_distributor_from_API(data_id, data_json);
              // +++++++++++++++++++++++++++++++++++++++++++++
            }else{
              console.log("tidak ada token yang cocok");
              let toast = this.toastCtrl.create({
                message: 'Tidak dikenali',
                duration: 2000,
                position: 'top'
              });
              toast.present();
            }
            
          }else{
            console.log("<<< *(manajemenTransaksi)_Token not Found >>>");
            let toast = this.toastCtrl.create({
              message: 'Token bermasalah',
              duration: 2000,
              position: 'top'
            });
            toast.present();
          }
        }, (error) => {
          console.log("<<< (X)ERROR GET TOKEN: FROM menajem-transaksi.ts", error);
        });
      }
    //=============

    //====FUNCTION DATABASE PT.TBM======||

      //====database pemesanan=========
          dataPemesananAPI;
          tempPemesananDataAPI;
          // username;
          //+++++++++++
        get_data_pemesanan_from_API(data_id, data_json){
          // this.username = this.tempDataLogin[0].username;
          // console.log("ini temp datalogin",this.tempDataLogin[0].username);
          let API;
          if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=41'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=51'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else{
            API = 'http://api.nex.web.id/pestisida/api.php?q=11'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          // console.log("intip",'http://api.nex.web.id/pestisida/api.php?q=11'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken);
          this.http.get(API).subscribe(res => {


            this.dataPemesananAPI = res.json();
            console.log("===DATA pemesanan FROM API: ", this.dataPemesananAPI);

            if(this.dataPemesananAPI != ''){

              console.log("***DATA FROM API FOUND.....");

              this.tempPemesananDataAPI = this.dataPemesananAPI;
              console.log("****INSERT TO tempPemesananDataAPI....(check data)", this.tempPemesananDataAPI);

              if(this.tempPemesananDataAPI[0].f0 == 1){
                let i =0;
                for(i=0; i < this.tempPemesananDataAPI.length; i++){
  
                  this.manajemenTransaksiService.put_transaksi_manajemen(
                    data_id = 1,
                    data_json = JSON.stringify(this.tempPemesananDataAPI)
                  ).then((result) => {
                    console.log("<<<< (OK)put_transaksi_manajemen >>>>");
                  }, (error) => {
                    console.log("<<<< (ERROR)put_transaksi_manajemen >>>>(check)", error);
                  });
  
                }
                //++++SHOW DATA
                  this.show_data_pemesanan(data_id, data_json);
                //+++++++++++++
              }else{
                // let toast = this.toastCtrl.create({
                //   message: this.tempPemesananDataAPI[0].f0,
                //   duration: 2000,
                //   position: 'middle'
                // });
                // toast.present();
    
                this.manajemenTransaksiService.put_transaksi_manajemen(
                  data_id = 1,
                  data_json = null
                ).then((result) => {
                  console.log("<<<< (OK)put_transaksi_manajemen >>>>");
                  //++++SHOW DATA
                  this.show_data_pemesanan(data_id, data_json);
                  //+++++++++++++
                }, (error) => {
                  console.log("<<<< (ERROR)put_transaksi_manajemen >>>>(check)", error);
                });
                
              }
              
            } else{
              console.log("<<< (x)DATA FROM API NOT FOUND.....");
              let toast = this.toastCtrl.create({
                message: 'Data tidak tersedia',
                duration: 2000,
                position: 'top'
              });
              toast.present();
            }

            
          }, (err) => {
            console.log("<<< (ERROR)get_data_pemesanan_from_API() FROM manajemen-transaksi.ts >>>(check)", err);
          });
        }

        

          dataPemesananDatabase;
          tempDataPemesananDatabase = [];
          f2Pemesanan;
          f4Pemesanan;
          readyDataPemesanan;
        //++++++++++++++++++++++++++++++++
        show_data_pemesanan(data_id, data_json){
          console.log("****show_data_pemesanan..... (READY)");
          
          this.manajemenTransaksiService.show_data_transaksi_manajemen().then((result) => {

            this.dataPemesananDatabase = <Array<Object>> result;
            console.log("===data_pemesanan===(check)", this.dataPemesananDatabase);

            //====AMBIL DATA JSON
              this.readyDataPemesanan = JSON.parse(this.dataPemesananDatabase[0].data_json);
              console.log("***INSERT IN TO readyDataPemesanan....");
            //===================

            if(this.readyDataPemesanan != null){
              console.log("===STATUS: FINISH(CHECK): ", this.readyDataPemesanan);
            }
            // else if(this.readyDataPemesanan[1].data_json.f0 == "0"){
            //   console.log("===(f0 = 0) => null");
            //   this.readyDataPemesanan = null;
            // }
            else {
              console.log("===STATUS: NULL(CHECK): ", this.readyDataPemesanan);
            }
          },(error) => {
            console.log("<<< (ERROR)show_data_pemesanan() FROM transaksi_manajemen : ", error);
          });

        }
      //===============================

      //====database ditolak===========
          dataDitolakAPI;
          tempDataDitolakAPI;
        //++++++++++++++++++++
        get_data_ditolak_from_API(data_id, data_json){

          let API;
          if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=42'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=52'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else{
            API = 'http://api.nex.web.id/pestisida/api.php?q=12'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }

          this.http.get(API).subscribe(res => {

            this.dataDitolakAPI = res.json();
            console.log("===DATA ditolak FROM API: ", this.dataDitolakAPI);

            if(this.dataDitolakAPI != ""){

              this.tempDataDitolakAPI = this.dataDitolakAPI;
              console.log("****INSERT TO tempDataDitolakAPI....(check data)", this.tempDataDitolakAPI);

              if(this.tempDataDitolakAPI[0].f0 == 1){
                let i=0;
                for(i=0; i < this.tempDataDitolakAPI.length; i++){
  
                  this.manajemenTransaksiService.put_transaksi_manajemen(
                    data_id = 2,
                    data_json = JSON.stringify(this.tempDataDitolakAPI)
                  ).then((result) => {
                    console.log("<<<< (OK)put_transaksi_manajemen FROM get_data_ditolak_from_API() >>>>");
                  }, (error) => {
                    console.log("<<<< (ERROR)put_transaksi_manajemen FROM get_data_ditolak_from_API() >>>>(check)", error);
                  });
                  
                }
                //++++SHOW DATA
                  this.show_data_ditolak(data_id, data_json);
                //+++++++++++++
              }else{
                // let toast = this.toastCtrl.create({
                //   message: this.tempDataDitolakAPI[0].f0,
                //   duration: 2000,
                //   position: 'middle'
                // });
                // toast.present();
    
                this.manajemenTransaksiService.put_transaksi_manajemen(
                  data_id = 2,
                  data_json = null
                ).then((result) => {
                  console.log("<<<< (OK)put_transaksi_manajemen FROM get_data_ditolak_from_API() >>>>");
                }, (error) => {
                  console.log("<<<< (ERROR)put_transaksi_manajemen FROM get_data_ditolak_from_API() >>>>(check)", error);
                });
              }

            } else{
              console.log("<<< (x)DATA FROM API NOT FOUND FROM get_data_ditolak_from_API().....");
              let toast = this.toastCtrl.create({
                message: 'Data tidak tersedia',
                duration: 2000,
                position: 'top'
              });
              toast.present();
            }

          }, (err) => {
            console.log("<<< (ERROR)get_data_ditolak_from_API() FROM manajemen-transaksi.ts >>>(check)", err);
          });
        }

          dataDitolakDatabase;
          tempDataDitolakDatabase   = [];
          readyDataDitolak;
        //++++++++++++++++++++++++++++++ 
        show_data_ditolak(data_id, data_json){
          console.log("****show_data_ditolak..... (READY)");

          this.manajemenTransaksiService.show_data_transaksi_manajemen().then((result) => {

            this.dataDitolakDatabase = <Array<Object>> result;
            console.log("===data_ditolak===(check)", this.dataDitolakDatabase);

            //====AMBIL DATA JSON
              console.log("************************************");
              console.log("====TESTING====");
              let i;
              console.log("===length: ", this.dataDitolakDatabase.length);
              for(i=0; i < this.dataDitolakDatabase.length; i++){
                // console.log("===data: ", this.dataDitolakDatabase[i].data_id);
                if(this.dataDitolakDatabase[i].data_id == '2'){
                  console.log("===data found: ", this.dataDitolakDatabase[i]);

                  this.readyDataDitolak = JSON.parse(this.dataDitolakDatabase[i].data_json);
                  console.log("***INSERT IN TO readyDataDitolak...",);
                }
                // else{
                //   console.log("===(X)NOT FOUND DATA===");
                // }
              }
            //===================

            if(this.readyDataDitolak != null){
              console.log("===STATUS: FINISH(CHECK): ", this.readyDataDitolak);
            }else {
              console.log("===STATUS: NULL(CHECK): ", this.readyDataDitolak);
            }
          }, (error) => {
            console.log("<<< (ERROR)show_data_ditolak() FROM transaksi_manajemen : ", error);
          });        
        }   
      //===============================
      
      //====database dikirim===========
          dataDikirimAPI;
          tempDataDikirimAPI;
        //++++++++++++++++++++
        get_data_dikirim_from_API(data_id, data_json){
          
          let API;
          if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=43'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=53'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else{
            API = 'http://api.nex.web.id/pestisida/api.php?q=13'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }

          this.http.get(API).subscribe(res => {

            this.dataDikirimAPI = res.json();
            console.log("===DATA dikirim FROM API: ", this.dataDikirimAPI);

            if(this.dataDikirimAPI != ""){

              this.tempDataDikirimAPI = this.dataDikirimAPI;
              console.log("****INSERT TO tempDataDikirimAPI....(check data)", this.tempDataDikirimAPI);

              if(this.tempDataDikirimAPI[0].f0 == 1){
                let i =0;
                for(i=0; i < this.tempDataDikirimAPI.length; i++){
  
                  this.manajemenTransaksiService.put_transaksi_manajemen(
                    data_id = 3,
                    data_json = JSON.stringify(this.tempDataDikirimAPI)
                  ).then((result) => {
                    console.log("<<<< (OK)put_transaksi_manajemen get_data_dikirim_from_API()>>>>");
                  }, (error) => {
                    console.log("<<<< (ERROR)put_transaksi_manajemen get_data_dikirim_from_API()>>>>(check)", error);
                  });
  
                }
                //++++SHOW DATA
                  this.show_data_dikirim(data_id, data_json);
                //+++++++++++++
              }else{
                // let toast = this.toastCtrl.create({
                //   message: this.tempDataDikirimAPI[0].f0,
                //   duration: 2000,
                //   position: 'middle'
                // });
                // toast.present();
    
                this.manajemenTransaksiService.put_transaksi_manajemen(
                  data_id = 3,
                  data_json = null
                ).then((result) => {
                  console.log("<<<< (OK)put_transaksi_manajemen get_data_dikirim_from_API()>>>>");
                }, (error) => {
                  console.log("<<<< (ERROR)put_transaksi_manajemen get_data_dikirim_from_API()>>>>(check)", error);
                });
              }

             
            } else{
              console.log("<<< (x)DATA FROM API NOT FOUND get_data_dikirim_from_API().....");
              let toast = this.toastCtrl.create({
                message: 'Data tidak tersedia',
                duration: 2000,
                position: 'top'
              });
              toast.present();
            }

          }, (err) => {
            console.log("<<< (ERROR)get_data_dikirim_from_API() FROM manajemen-transaksi.ts >>>(check)", err);
          });

        }

          dataDikirimDatabase;
          tempDataDikirimDatabase = [];
          readyDataDikirim;
        //+++++++++++++++++++++++++
        show_data_dikirim(data_id, data_json){
          console.log("****show_data_dikirim..... (READY)");

          this.manajemenTransaksiService.show_data_transaksi_manajemen().then((result) => {

            this.dataDikirimDatabase = <Array<Object>> result;
            console.log("===data_dikirim===(check)", this.dataDikirimDatabase);

            //====AMBIL DATA JSON
              let i;
              for(i=0; i < this.dataDikirimDatabase.length; i++){
                if(this.dataDikirimDatabase[i].data_id == '3'){
                  this.readyDataDikirim = JSON.parse(this.dataDikirimDatabase[i].data_json);
                  console.log("***INSERT IN TO readyDataDikirim...");
                }
              }
            //===================

            if(this.readyDataDikirim != null){
              console.log("===STATUS: FINISH(CHECK): ", this.readyDataDikirim);
            }else {
              console.log("===STATUS: NULL(CHECK): ", this.readyDataDikirim);
            }

          }, (error) => {
            console.log("<<< (ERROR)show_data_dikirim() FROM transaksi_manajemen : ", error);
          });

        }
      //===============================

      //===database terkirim===========
          dataTerkirimAPI;
          tempDataTerkirimAPI;
        //+++++++++++++++++++++
        get_data_terkirim_from_API(data_id, data_json){

          let API;
          if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=44'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=54'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else{
            API = 'http://api.nex.web.id/pestisida/api.php?q=14'+'&q2='+this.USERNAME+'&p=0&token='+this.tempToken;
            console.log("===API: ", API);
          }

          this.http.get(API).subscribe(res => {

            this.dataTerkirimAPI = res.json();
            console.log("===DATA terkirim FROM API: ", this.dataTerkirimAPI);

            if(this.dataTerkirimAPI != ""){

              this.tempDataTerkirimAPI = this.dataTerkirimAPI;
              console.log("****INSERT TO tempPemesananDataAPI....(check data)", this.tempDataTerkirimAPI);

              if(this.tempDataTerkirimAPI[0].f0 == 1){
                let i =0;
                for(i=0; i < this.tempDataTerkirimAPI.length; i++){
  
                  this.manajemenTransaksiService.put_transaksi_manajemen(
                    data_id = 4,
                    data_json = JSON.stringify(this.tempDataTerkirimAPI)
                  ).then((result) => {
                    console.log("<<<< (OK)put_transaksi_manajemen get_data_terkirim_from_API() >>>>");
                  }, (error) => {
                    console.log("<<<< (ERROR)put_transaksi_manajemen get_data_terkirim_from_API() >>>>(check)", error);
                  });
  
                }
                //++++SHOW DATA
                  this.show_data_terkirim(data_id, data_json);
                //+++++++++++++
              }else{
                // let toast = this.toastCtrl.create({
                //   message: this.tempDataTerkirimAPI[0].f0,
                //   duration: 2000,
                //   position: 'middle'
                // });
                // toast.present();

                this.manajemenTransaksiService.put_transaksi_manajemen(
                  data_id = 4,
                  data_json = null
                ).then((result) => {
                  console.log("<<<< (OK)put_transaksi_manajemen get_data_terkirim_from_API() >>>>");
                }, (error) => {
                  console.log("<<<< (ERROR)put_transaksi_manajemen get_data_terkirim_from_API() >>>>(check)", error);
                });
              }

              
            } else{
              console.log("<<< (x)DATA FROM API NOT FOUND.....");
              let toast = this.toastCtrl.create({
                message: 'Data tidak tersedia',
                duration: 2000,
                position: 'top'
              });
              toast.present();   
            }

          }, (err) => {
            console.log("<<< (ERROR)get_data_terkirim_from_API() FROM manajemen-transaksi.ts >>>(check)", err);
          });

        }

          dataTerkirimDatabase;
          tempDataTerkirimDatabase = [];
          readyDataTerkirim;
        //++++++++++++++++++++++++++++++++
        show_data_terkirim(data_id, data_json){

          this.manajemenTransaksiService.show_data_transaksi_manajemen().then((result) => {

            this.dataTerkirimDatabase = <Array<Object>> result;
            console.log("===data_terkirim===(check)", this.dataTerkirimDatabase);

            //====AMBIL DATA JSON
              let i;
              for(let i=0; i < this.dataTerkirimDatabase.length; i++){
                if(this.dataTerkirimDatabase[i].data_id == '4'){
                  console.log("===data found: ", this.dataTerkirimDatabase[i]);
                  this.readyDataTerkirim = JSON.parse(this.dataTerkirimDatabase[i].data_json);
                  console.log("===json parse ", JSON.parse(this.dataTerkirimDatabase[i].data_json));
                  console.log("***INSERT IN TO readyDataTerkirim...");
                }
              }
            //==================
            
            if(this.readyDataTerkirim != null){
              console.log("===STATUS: FINISH(CHECK): ", this.readyDataTerkirim);
            } else {
              console.log("===STATUS: NULL(CHECK): ", this.readyDataTerkirim);
            }

          }, (error) => {
            console.log("<<< (ERROR)show_data_terkirim() FROM transaksi_manajemen : ", error);
          });

        }
      //===============================
    
    //==================================||

    //====FUNCTION DATABASE DISTRIBUTOR=||
        dataDistributorAPI;
        tempDataDistributorAPI;
      //++++++++++++++++++++++++
      get_data_distributor_from_API(data_id, data_json){
        // console.log('http://api.nex.web.id/pestisida/api.php?q='+this.databasenya.kodeapi+'&q2='+this.USERNAME+' &token='+this.tempToken+' ');

        let API;
        if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
          API = 'http://api.nex.web.id/pestisida/api.php?q=45&q2='+this.USERNAME+' &token='+this.tempToken+' ';
          console.log("===API: ", API);
        }
        else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
          API = 'http://api.nex.web.id/pestisida/api.php?q=55&q2='+this.USERNAME+' &token='+this.tempToken+' ';
          console.log("===API: ", API);
        }
        else{
          API = 'http://api.nex.web.id/pestisida/api.php?q='+this.databasenya.kodeapi+'&q2='+this.USERNAME+' &token='+this.tempToken+' ';
          console.log("===API: ", API);
        }

        this.http.get(API).subscribe(res => {

          this.dataDistributorAPI = res.json();
          console.log("===DATA distributor FROM API: ", this.dataDistributorAPI);

          if(this.dataDistributorAPI != ""){

            this.tempDataDistributorAPI = this.dataDistributorAPI;
            console.log("****INSERT TO tempPemesananDataAPI....(check data)", this.tempDataDistributorAPI);

            if(this.tempDataDistributorAPI[0].f0 == 1){
              let i =0;
              for(i=0; i < this.tempDataDistributorAPI.length; i++){
  
                this.manajemenTransaksiService.put_transaksi_distributor(
                  data_id = 1,
                  data_json = JSON.stringify(this.tempDataDistributorAPI)
                ).then((result) => {
                  console.log("<<<< (OK)put_transaksi_distributor >>>>");
                }, (error) => {
                  console.log("<<<< (ERROR)put_transaksi_distributor >>>>(check)", error);
                });
  
              }
              //++++SHOW DATA
                this.show_data_distributor(data_id, data_json);
              //+++++++++++++
            }else{
              // let toast = this.toastCtrl.create({
              //   message: this.tempDataDistributorAPI[0].f0,
              //   duration: 2000,
              //   position: 'middle'
              // });
              // toast.present();
              this.manajemenTransaksiService.put_transaksi_distributor(
                data_id = 1,
                data_json = null
              ).then((result) => {
                console.log("<<<< (OK)put_transaksi_distributor >>>>");
              }, (error) => {
                console.log("<<<< (ERROR)put_transaksi_distributor >>>>(check)", error);
              });
            }

            
          } else{
            console.log("<<< (x)DATA FROM API NOT FOUND get_data_distributor_from_API.....");
            let toast = this.toastCtrl.create({
              message: 'Data tidak tersedia',
              duration: 2000,
              position: 'middle'
            });
            toast.present();
            // this. get_token(data_id, data_json, searchInput, searchInputDistributor);
          }

        }, (err) => {
          console.log("<<< (ERROR)get_data_distributor_from_API() FROM manajemen-transaksi.ts >>>(check)", err);
        });
     
      }


        dataDistributorDatabase;
        tempDataDistributorDatabase = [];
        readyDataDistributor;
      //+++++++++++++++++++++++++++++
      show_data_distributor(data_id, data_json){

        this.manajemenTransaksiService.show_data_transaksi_manajemen_distributor().then((result) => {
          
          this.dataDistributorDatabase = <Array<Object>> result;
          console.log("===data_distributor===(check)", this.dataDistributorDatabase);

           //====AMBIL DATA JSON
            this.readyDataDistributor = JSON.parse(this.dataDistributorDatabase[0].data_json);
            console.log("***INSERT IN TO readyDataDistributor...");
          //===================

          if(this.readyDataDistributor != null){
            console.log("===STATUS: FINISH(CHECK): ", this.readyDataDistributor);
            //====FUNCTION FILTER UNTUK LOAD DATA DARI PROVIDER==
              // this.distributor = this.manajemenTransaksiService.filterDistributor(this.searchInputDistributor);
            //===================================================

          }else {
            console.log("===STATUS: NULL(CHECK): ", this.readyDataDistributor);
          }
          
        }, (error)=>{
          console.log("<<< (ERROR)show_data_distributor() FROM transaksi_manajemen : ", error);
        });

      }
    //==================================||

    //====FUNCTION LOAD MORE=============
      tempJsonP;
      p: number=0;
      temp=[];
      count;
      loadingTrigger: boolean = false;
      load_more(){
        this.loadingTrigger = true;
        let username;
        let token;
        console.log("===LOAD_MORE : ", this.tempDataLogin);

        username = this.tempDataLogin[0].username;
        // console.log("===USERNAME: ", username);

        token = this.tempDataLogin[0].token;
        // console.log("===TOKEN: ", this.tempDataLogin[0].token);

        this.p++;
        console.log("===p: ", this.p);
        this.http.get('http://api.nex.web.id/pestisida/api.php?q=11'+'&q2='+username+'&p='+this.p+'&token='+token).subscribe(res => {
          this.tempJsonP = res.json();
          
          if(this.tempJsonP != ""){
            
            console.log("===P"+this.p+" : ", this.tempJsonP);
            this.temp[this.p] = this.tempJsonP;
            console.log("=== temp: ", this.temp);
            console.log("==lengthOfTemp: ", this.temp.length);
            this.count = this.temp.length;
          } else{
             console.log("===P"+this.p+" : DATA KOSONG");
             this.p = -1;//
          }
         console.log("==length: ", this.temp.length);
        });
        this.loadingTrigger=false;
      }

      reset_load_more(){
        this.p = 0;
        this.temp.splice(0, this.temp.length);
      }
    //===================================

    //====DO SEARCH================================
      tempDataSearch;
      flagHtml: number = 0;
      doSearch(){
        console.log("===search input: ", this.searchForm.controls.searchBar.value);
        if(this.transaksiTab == 'pt'){
          console.log("pt");
          if(this.tabForm.controls.tabValue.value == "pesanan"){
            console.log("===pesanan");

            let API;
            if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=41'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=51'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else{
              API = 'http://api.nex.web.id/pestisida/api.php?q=11'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }

            //========
              this.http.get(API).subscribe(res => {
                this.tempDataSearch = res.json();
                
                if(this.tempDataSearch[0].f0 == "0"){
                  console.log("Data yang dicari tidak ada");
                  this.flagHtml = -1;
                }
                else{
                  console.log("===DATA pemesanan FROM SEARCH API: ", this.tempDataSearch);
                  this.flagHtml = 1;
                }

              },(error)=>{
                console.log("===(X)SEARCH PEMESANAN FAILURE===");
              });
            //========
          }
          else if(this.tabForm.controls.tabValue.value == "ditolak"){
            console.log("===ditolak");

            let API;
            if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=42'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=52'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else{
              API = 'http://api.nex.web.id/pestisida/api.php?q=12'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }

            //========
            console.log("===LINK: ", API);
              this.http.get(API).subscribe(res => {
                this.tempDataSearch = res.json();
                
                if(this.tempDataSearch[0].f0 == "0"){
                  console.log("Data yang dicari tidak ada");
                  this.flagHtml = -1;
                }
                else{
                  console.log("===DATA ditolak FROM SEARCH API: ", this.tempDataSearch);
                  this.flagHtml = 1;
                }
              },(error)=>{
                console.log("===(X)SEARCH PEMESANAN FAILURE===");
              });
            //========
          }
          else if(this.tabForm.controls.tabValue.value == "dikirim"){
            console.log("===dikirim");

            let API;
            if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=43'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=53'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else{
              API = 'http://api.nex.web.id/pestisida/api.php?q=13'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }

            //========
              this.http.get(API).subscribe(res => {
                this.tempDataSearch = res.json();
                
                if(this.tempDataSearch[0].f0 == "0"){
                  console.log("Data yang dicari tidak ada");
                  this.flagHtml = -1;
                }
                else{
                  console.log("===DATA dikirim FROM SEARCH API: ", this.tempDataSearch);
                  this.flagHtml = 1;
                }

              },(error)=>{
                console.log("===(X)SEARCH dikirim FAILURE===");
              });
            //========
          }
          else{
            console.log("===terkirim");

            let API;
            if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=44'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
              API = 'http://api.nex.web.id/pestisida/api.php?q=54'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }
            else{
              API = 'http://api.nex.web.id/pestisida/api.php?q=14'+'&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&p=0&token='+this.tempToken;
              console.log("===API: ", API);
            }

            //========
              this.http.get(API).subscribe(res => {
                this.tempDataSearch = res.json();
                
                if(this.tempDataSearch[0].f0 == "0"){
                  console.log("Data yang dicari tidak ada");
                  this.flagHtml = -1;
                }
                else{
                  console.log("===DATA terkirim FROM SEARCH API: ", this.tempDataSearch);
                  this.flagHtml = 1;
                }

              },(error)=>{
                console.log("===(X)SEARCH terkirim FAILURE===");
              });
            //========
          }
        }
        else{
          console.log("distributor");

          let API;
          if(this.segmentvalidasi == 'agronomis&FA(6)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=45&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else if(this.segmentvalidasi == 'agronomis&FA(7)@gmail.com'){
            API = 'http://api.nex.web.id/pestisida/api.php?q=55&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&token='+this.tempToken;
            console.log("===API: ", API);
          }
          else{
            API = 'http://api.nex.web.id/pestisida/api.php?q=15&q2='+this.USERNAME+'|'+this.searchForm.controls.searchBar.value+'&token='+this.tempToken;
            console.log("===API: ", API);
          }

          //========
          console.log("===kode API= ", this.databasenya.kodeapi);
          this.http.get(API).subscribe(res => {
            this.tempDataSearch = res.json();
            console.log("===tempDataSearch: ", this.tempDataSearch);
            
            if(this.tempDataSearch[0].f0 == "0"){
              console.log("Data yang dicari tidak ada");
              this.flagHtml = -1;
            }
            else{
              console.log("===DATA distributor FROM SEARCH API: ", this.tempDataSearch);
              this.flagHtml = 1;
            }

          },(error)=>{
            console.log("===(X)SEARCH distributor FAILURE===");
          });
        //========
        }
      }

      clearSearch(){
        this.searchForm.reset();
        console.log("reset searchForm");
        this.flagHtml = 0;
      }

    //=============================================
    
    //===DO REFRESH================================
      flagRefresh: boolean = false;
      doRefresh(data_id, data_json){
        this.flagRefresh = true;
        console.log("===flagRefresh: ", this.flagRefresh);
        if(this.transaksiTab == 'pt'){
          this.changeTabs(data_id, data_json);
        }
        else{
          this.triger_get_distributor(data_id, data_json);
        }
        this.flagRefresh = false;
        console.log("===flagRefresh: ", this.flagRefresh);
      }
    //=============================================
}
