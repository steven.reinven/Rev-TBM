import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from "../../../providers/data/data";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginProvider } from "../../../providers/login/login";
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { ManajemenTransaksiProvider } from '../../../providers/manajemen-transaksi/manajemen-transaksi';

/**
 * Generated class for the DetailTransaksiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-transaksi',
  templateUrl: 'detail-transaksi.html',
})
export class DetailTransaksiPage {

  // status: string= "diterima";
  noteValidator: boolean; //TRIGGER UNTUK ON/OFF NOTE BERDASARKAN STATUS PESANAN

  //VARIABEL NAVPRAMS.GET
    pt;
    tanggalTransaksi;
    order;
    status;
    catatan;
    totalBox: number;
    catatanForm: FormGroup;
    index;
    id;
    tglKirim;
    noSuratJalan;
    noInvoice;
    transaksiTab;
    noSo;
    segmentvalidasi;
  //==================

  constructor(
    public navCtrl: NavController, 
    public http: Http, 
    public databasenya: LoginProvider, 
    public navParams: NavParams, 
    public dataProvider: DataProvider,
    public toastCtrl: ToastController,
    public manajemenTransaksiService: ManajemenTransaksiProvider, 
    public builder: FormBuilder) {

    this.pt = this.navParams.get("pt");
    this.tanggalTransaksi = this.navParams.get("tanggalTransaksi");
    this.order = this.navParams.get("order");
    this.status = this.navParams.get("status");
    this.catatan = this.navParams.get("catatan");
    this.index = this.navParams.get("index");
    this.id = this.navParams.get("id");
    this.tglKirim = this.navParams.get("tglKirim");
    this.noSuratJalan = this.navParams.get("noSuratJalan");
    this.noInvoice = this.navParams.get("noInvoice");
    this.noSo = this.navParams.get("noSo");
    this.transaksiTab = this.navParams.get("transaksiTab");
    this.segmentvalidasi = this.navParams.get("segmentvalidasi");

    this.noteValidator = false;//variable validasi catatan

    this.catatanForm = this.builder.group({
      'catatan': ['', Validators.required]
    })

    console.log("get pt: ", this.pt);//
    console.log("get tanggalTransaksi: ", this.tanggalTransaksi);//
    console.log("get order: ", this.order);//
    console.log("get catatan: ", this.catatan);//
    console.log("get index: ", this.index);//
    console.log("get id: ", this.id);//
    console.log("get tglKirim: ", this.tglKirim);//
    console.log("get noSuratJalan: ", this.noSuratJalan);//
    console.log("get noInvoice: ", this.noInvoice);//
    console.log("get status: ", this.status);// ???
    console.log("get transaksiTab: ", this.transaksiTab);//
    console.log("get noSo: ", this.noSo);
    console.log("get segmentvalidasi: ", this.segmentvalidasi);

    // console.log("no.order: ", this.order[0].noOrder);
    this.hitungTotalBox()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTransaksiPage');
  }



//====================FUNCTION UNTUK HITUNG TOTAL BOX==================//
  hitungTotalBox(){
    console.log("lenght: ", this.order.length);
    this.totalBox = 0;
    let temp: number = 0;
    let x;
    for(x=0; x < this.order.length; x++){
      console.log("jumlah: ", this.order[x].qty);
      console.log("type of: ", typeof(this.order[x].qty));
      temp = +this.order[x].qty;
      this.totalBox += temp;
    }
    console.log("total box: ", this.totalBox);
  }
//====================================================================//


//=====================FUNCTION BUTTON "PESAN DITERIMA"==================//
    dataLogin;
    username;
    f3;
  buttonPesananDiterima(){
  //old
    // console.log("no Po: ", this.order[0].noOrder);
    // console.log("pesanan diterima");
    // console.log("catatan: ", this.catatanForm.controls.catatan.value);
    // let x;
    // for(x=0; x < this.dataProvider.ptTBM.length; x++){
    //   if(this.id == this.dataProvider.ptTBM[x].id){
    //     // console.log("nama pt: ", this.dataProvider.ptTBM[x].nama);
    //     this.dataProvider.ptTBM[x].status = "Pesanan dikirim";
    //     this.dataProvider.ptTBM[x].catatan = this.catatanForm.controls.catatan.value; 
    //   }
    // }
    // this.dataProvider.resetData();
    // this.dataProvider.jsonPtTBMProceddByStatus();
    // this.dataProvider.notificationManajemenTransaksi -= 1;
    // if(this.dataProvider.notificationManajemenTransaksi != 0){
    //   this.dataProvider.notifTab -= 1;
    // }
    // else{
    //   this.dataProvider.notifTab = "";
    // }
    // this.navCtrl.pop();
  //

    this.databasenya.showms_user().then((result) => {

      this.dataLogin = <Array<Object>> result;
      this.username = this.dataLogin[0].username;
      console.log("=== username: ", this.username);
      console.log("=== SO. No: ", this.id);

      this.f3 = "1";
      console.log("=== f3: ", this.f3);

      let catatan = this.catatanForm.controls.catatan.value;
      console.log("=== catatan: ", catatan);

      console.log("<<< CREATING BODY...... >>>");
      let body = [{f1: this.username,f2: this.id,f3: this.f3,f4: catatan}];
      console.log("=== (OK)CREATE BODY SUCCESS: ", body);

      console.log("===dataLogin[0].token: ", this.dataLogin[0].token);
      let token = this.dataLogin[0].token;
      console.log("===token : ", token);
      console.log("=== http POST LINK: " + "http://api.nex.web.id/pestisida/api.php?q=7&token=" + token,JSON.stringify(body));



      this.http.post("http://api.nex.web.id/pestisida/api.php?q=7&token="+token,body).subscribe(res=> {
        console.log("<<< POST THAT LINK.... >>>");
        // console.log("=== (OK)LINK HAS BEEN POSTED: ", res.json());

        let toast  = this.toastCtrl.create({
          message: "Kode esanan " + this.id + " sudah di konfirmasi",
          duration: 3000,
          position: "middle" 
        });
        toast.present();
        this.manajemenTransaksiService.flagAPIPemesanan = true;
        console.log("====this.manajemenTransaksiService.flagAPIPemesanan", this.manajemenTransaksiService.flagAPIPemesanan);

        this.manajemenTransaksiService.flagAPIDikirim = true;
        console.log("===this.manajemenTransaksiService.flagAPIDitolak: ", this.manajemenTransaksiService.flagAPIDikirim);
        this.navCtrl.pop();
      }, (err)=> {
        console.log("<<< (x)CAN'T POST THAT LINK >>>");
      });

    }, (error)=>{
      console.log("<<< (x)CAN'T OPEN showms_user: ", error);
    });

  }
  
//=============================================================================//
  


//===========================FUNCTION BUTTON "PESAN DITOLAK"============================//
  buttonPesananDitolak(){
  //old===
      // console.log("no Po: ", this.order[0].noOrder);
      // console.log("pesanan ditolak");
      // console.log("catatan: ", this.catatanForm.controls.catatan.value);
      // let x;
      // for(x=0; x < this.dataProvider.ptTBM.length; x++){
      //   if(this.id == this.dataProvider.ptTBM[x].id){
      //     // console.log("nama pt: ", this.dataProvider.ptTBM[x].nama);
      //     this.dataProvider.ptTBM[x].status = "Pesanan ditolak"; 
      //     if(this.catatanForm.controls.catatan.value == ""){
      //       this.noteValidator = true;
      //     }
      //     else{
      //       this.dataProvider.ptTBM[x].catatan = this.catatanForm.controls.catatan.value;
      //       this.dataProvider.resetData();
      //       this.dataProvider.jsonPtTBMProceddByStatus();
      //       this.dataProvider.notificationManajemenTransaksi -= 1;
            
      //       if(this.dataProvider.notificationManajemenTransaksi != 0){
      //         this.dataProvider.notifTab -= 1;
      //       }
      //       else{
      //         this.dataProvider.notifTab = "";
      //       }
      //       this.navCtrl.pop();
      //     }
      //   }
      // }
  //=======
  
    if(this.catatanForm.controls.catatan.value == ""){

      this.noteValidator = true;

    } else{
      //====POST TO API
        this.databasenya.showms_user().then((result) => {

          this.dataLogin = <Array<Object>> result;
          this.username = this.dataLogin[0].username;
          console.log("=== username: ", this.username);
          console.log("=== SO. No: ", this.id);

          this.f3 = "0";
          console.log("=== f3: ", this.f3);

          let catatan = this.catatanForm.controls.catatan.value;
          console.log("=== catatan: ", catatan);

          console.log("<<< CREATING BODY...... >>>");
          let body = [{f1: this.username,f2: this.id,f3: this.f3,f4: catatan}];

          console.log("=== (OK)CREATE BODY SUCCESS: ", body);

          console.log("===dataLogin[0].token: ", this.dataLogin[0].token);
          let token = this.dataLogin[0].token;
          console.log("===token : ", token);
          console.log("=== http POST LINK: " + "http://api.nex.web.id/pestisida/api.php?q=7&token=" + token,body);



          this.http.post("http://api.nex.web.id/pestisida/api.php?q=7&token="+token,body).subscribe(res=> {
            console.log("<<< POST THAT LINK.... >>>");
            // console.log("=== (OK)LINK HAS BEEN POSTED: ", res.json());

            let toast  = this.toastCtrl.create({
              message: "Kode pesanan " + this.id + " telah di tolak",
              duration: 3000,
              position: "middle" 
            });
            toast.present();
            this.manajemenTransaksiService.flagAPIPemesanan = true;
            console.log("====this.manajemenTransaksiService.flagAPIPemesanan", this.manajemenTransaksiService.flagAPIPemesanan);

            this.manajemenTransaksiService.flagAPIDitolak = true;
            console.log("===this.manajemenTransaksiService.flagAPIDitolak: ", this.manajemenTransaksiService.flagAPIDitolak);
            this.navCtrl.pop();
          }, (err)=> {
            console.log("<<< (x)CAN'T POST THAT LINK >>>");
          });

        }, (error)=>{
          console.log("<<< (x)CAN'T OPEN showms_user: ", error);
        });
      //===============
    }

  }
//=======================================================================================//

}
