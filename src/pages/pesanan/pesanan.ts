import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform, ToastController } from 'ionic-angular';

import { ListPesananPage } from "./list-pesanan/list-pesanan";
import { DataProvider } from "../../providers/data/data";
import { LoginProvider } from '../../providers/login/login';
import { Network } from '@ionic-native/network';
import { Http } from '@angular/http';
import { PesananUserProvider } from '../../providers/pesanan-user/pesanan-user';
import { LoginPage } from '../login/login';

/**
 * Generated class for the PesananPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-pesanan',
  templateUrl: 'pesanan.html',
})
export class PesananPage {

  //===SQLite========//
    //dari api
      datapesanandariapi;
      tampungdatapesanandariapi;
    //dari api

    //dari database
      datalogindaridatabase;
      tampungdatalogindaridatabase;
      USERNAME;
      TOKEN;
      TITTLEID;

      datapesanandaridatabase;
      tampungdatapesanandaridatabase;

      pesanan;
      formatJsonPesanan=[];

    //dari database
  //=================//

  platformStatus;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController, 
    public app: App,
    public dataProvider: DataProvider,
    public platform: Platform,
    public toastCtrl: ToastController,

    
    private http:Http,
    public databaseloginnya: LoginProvider,
    public databasepesanannya:PesananUserProvider,
    private network: Network
  ) {

    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    });   
  
  }

    pesananMenu=[
      {
        "icon": "ios-archive",
        "color": "buttonPesan",
        "name": "Pesanan dalam proses",
        "jumlah": 0
      },
      {
        "icon": "md-close",
        "color": "danger",
        "name": "Pesanan ditolak",
        "jumlah": 0
      },
      {
        "icon": "ios-paper-plane",
        "color": "secondary",
        "name": "Pesanan dikirim",
        "jumlah": 0
      },
      {
        "icon": "md-checkmark-circle-outline",
        "color": "primary",
        "name": "Pesanan terkirim",
        "jumlah": 0
      }
    ];

  //sqlite
    ambiltokendaridatabase(id,jsonnya){
      console.log("di pesanan.ts ambiltokendaridatabase start");
      this.databaseloginnya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di pesanan.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        if(this.tampungdatalogindaridatabase[0].token != ""){
          this.USERNAME=this.tampungdatalogindaridatabase[0].username;
          this.TOKEN=this.tampungdatalogindaridatabase[0].token;
          this.TITTLEID=this.tampungdatalogindaridatabase[0].titleid;
          // console.log("di pesanan.ts ini username dari database",this.USERNAME);
          console.log("di pesanan.ts ini token dari database",this.TOKEN);

          this.ambildatapesanandariapi(id,jsonnya);
        }else{
          console.log("di pesanan.ts anda tidak memiliki token");
        }

      },(error) => {
          console.log("di pesanan.ts ERROR: showms_user", error);
      });
    }

    ambildatapesanandariapi(id,jsonnya){
      this.http.get('http://api.nex.web.id/pestisida/api.php?q=40&q2='+this.USERNAME+'&token='+this.TOKEN)
      .subscribe(res=>{

        this.datapesanandariapi=res.json();
        this.tampungdatapesanandariapi=this.datapesanandariapi;
        console.log("andeca andeci ",[JSON.stringify(this.tampungdatapesanandariapi[0])]);

        if(this.tampungdatapesanandariapi.length != 0){

          if(this.tampungdatapesanandariapi[0].f0 == 1){

            if(this.TITTLEID == "5" || this.TITTLEID == "9"){
              this.databasepesanannya.putms_pesanan(
                id=1,
                jsonnya='['+JSON.stringify(this.tampungdatapesanandariapi[0])+','+JSON.stringify(this.tampungdatapesanandariapi[1])+','+JSON.stringify(this.tampungdatapesanandariapi[2])+','+JSON.stringify(this.tampungdatapesanandariapi[3])+']'
                
              ).then((result) => {
                console.log("di pesanan.ts putms_pesanan finnish");
                this.ambildatapesanandaridatabase(id,jsonnya);
              },(error) => {
                console.log("di pesanan.ts ERROR: putms_pesanan", error);    
                let toast = this.toastCtrl.create({
                  message: 'Database lokal bermasalah',
                  duration: 2000,
                  position: 'middle'
                });
                toast.present();        
              });
            }else{  
              this.databasepesanannya.putms_pesanan(
                id=1,
                jsonnya=JSON.stringify(this.tampungdatapesanandariapi)
                
              ).then((result) => {
                console.log("di pesanan.ts putms_pesanan finnish");
                this.ambildatapesanandaridatabase(id,jsonnya);
              },(error) => {
                console.log("di pesanan.ts ERROR: putms_pesanan", error);    
                let toast = this.toastCtrl.create({
                  message: 'Database lokal bermasalah',
                  duration: 2000,
                  position: 'middle'
                });
                toast.present();        
              });
            }

            // this.databasepesanannya.putms_pesanan(
            //   id=1,
            //   jsonnya=JSON.stringify(this.tampungdatapesanandariapi)
              
            // ).then((result) => {
            //   console.log("di pesanan.ts putms_pesanan finnish");
            //   this.ambildatapesanandaridatabase(id,jsonnya);
            // },(error) => {
            //   console.log("di pesanan.ts ERROR: putms_pesanan", error);    
            //   let toast = this.toastCtrl.create({
            //     message: 'Database lokal bermasalah',
            //     duration: 2000,
            //     position: 'middle'
            //   });
            //   toast.present();        
            // });
          }else{
            if(this.databaseloginnya.flag == 'user@gmail.com'){
              let toast = this.toastCtrl.create({
                message: this.tampungdatapesanandariapi[0].f0,
                duration: 2000,
                position: 'middle'
              });
              toast.present();
  
              this.databasepesanannya.putms_pesanan(
                id=1,
                jsonnya=null
                
              ).then((result) => {
                console.log("di pesanan.ts putms_pesanan finnish");
                this.ambildatapesanandaridatabase(id,jsonnya);
              },(error) => {
                console.log("di pesanan.ts ERROR: putms_pesanan", error);    
                 
              });
            }
            else if(this.databaseloginnya.flag == 'skip'){
              console.log("===GUEST MODE==")
            }
          }

        }else{
          console.log("di pesanan.ts data api kosong");
          let toast = this.toastCtrl.create({
            message: 'Tidak Ada Pesanan',
            duration: 2000,
            position: 'middle'
          });
          toast.present();
        }
       
      },(err)=>{
        console.log("di pesanan.ts error httpgetpesanan",err);
        let toast = this.toastCtrl.create({
          message: 'Server Down',
          duration: 2000,
          position: 'middle'
        });
        toast.present();
        // this.ambildatapesanandaridatabase(id,jsonnya);
      });
    
    }

    ambildatapesanandaridatabase(id,jsonnya){
      
      this.databasepesanannya.showms_pesanan().then((result) => {
        this.datapesanandaridatabase = <Array<Object>> result;
        console.log("di pesanan.ts ms_pesanan has been Loaded");
        this.tampungdatapesanandaridatabase=this.datapesanandaridatabase;

        if(this.tampungdatapesanandaridatabase != ""){
          this.pesanan=JSON.parse(this.tampungdatapesanandaridatabase[0].jsonnya);

        }else{
          console.log("di pesanan.ts showms_pesanan kosong");
          this.ambiltokendaridatabase(id,jsonnya);
        }

      },(error) => {
          console.log("di pesanan.ts ERROR: showms_pesanan", error);
      });

    }
  //sqlite

    ionViewDidLoad(id,jsonnya){
      console.log("di pesanan.ts ionViewDidLoad");
      if(
          this.network.type === 'none'
        ||this.network.type === 'unknown'){
        console.log("di pesanan.ts ini none atau unknown network");
        this.ambildatapesanandaridatabase(id,jsonnya);
      }else if(
          this.network.type === 'wifi' 
        ||this.network.type === 'cellular'
        ||this.network.type === '4g'
        ||this.network.type === '3g'
        ||this.network.type === '2g'
        ||this.network.type === 'ethernet'){
        console.log("di pesanan.ts ini connect");
        this.ambiltokendaridatabase(id,jsonnya);
        
      }else{
        console.log("di pesanan.ts connect aneh");
        this.ambildatapesanandaridatabase(id,jsonnya);
      }
    }

    doRefresh(refresher,id,jsonnya) {
      console.log('Begin async operation', refresher);
  
      setTimeout(() => {
        this.ionViewDidLoad(id,jsonnya);
        console.log('Async operation has ended');
        refresher.complete();
      }, 2000);
    }

    ionViewDidEnter(id, jsonnya){
      console.log("===ionViewDidEnter===");
      if(
          this.network.type === 'wifi' 
        ||this.network.type === 'cellular'
        ||this.network.type === '4g'
        ||this.network.type === '3g'
        ||this.network.type === '2g'
        ||this.network.type === 'ethernet'){
          if(this.databasepesanannya.flagAPIPesanan == true){
            console.log("===Pesanan Baru terdeteksi===");
            console.log("===databasepesanannya.flagAPIPesanan from pemesanan.ts(check): ", this.databasepesanannya.flagAPIPesanan);
            this.ambildatapesanandariapi(id, jsonnya);
            console.log("===(OK)Tarik Data===");

            this.databasepesanannya.flagAPIPesanan = false;
            console.log("===databasepesanannya.flagAPIPesanan from pemesanan.ts(check): ", this.databasepesanannya.flagAPIPesanan);
          }
          else{
            console.log("===databasepesanannya.flagAPIPesanan from pemesanan.ts(check): ", this.databasepesanannya.flagAPIPesanan);
            console.log("===Belum ada tambahan pesanan===");
          }
        }
        else{
          console.log("===(X)We lost internet connection===");
        }
    }

  goToListPesanan(i){
    this.navCtrl.push(ListPesananPage, {
      pilihi: i,
      iconi: this.pesananMenu[i].icon,
      colori: this.pesananMenu[i].color,
      namai : this.pesananMenu[i].name
    });
    console.log(i, " Selected!");
  }

  goToLoginPage(){
    this.app.getRootNav().setRoot(LoginPage);
  }
  
}
