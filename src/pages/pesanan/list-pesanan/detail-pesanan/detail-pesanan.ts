import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { PesananUserListProvider } from '../../../../providers/pesanan-user-list/pesanan-user-list';

/**
 * Generated class for the DetailPesananPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-detail-pesanan',
  templateUrl: 'detail-pesanan.html',
})
export class DetailPesananPage {

  // soCode;
  // detailSoCode;
  // catatan;
  // totalBox: number; 
  platformStatus;

  f1yangdipilih;
  f2yangdipilih;
  f3yangdipilih;
  f4yangdipilih;
  f5yangdipilih;
  f6yangdipilih;
  f7yangdipilih;
  f8yangdipilih;
  f9yangdipilih;
  f10yangdipilih;
  f11yangdipilih;
  categoryPesanan;


  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public platform: Platform,
      public databasepesananlistnya: PesananUserListProvider,
      private network: Network
    ) {
    // this.soCode = this.navParams.get("soCode");
    // this.detailSoCode = this.navParams.get("detailSoCode");
    // this.catatan = this.navParams.get("catatan");
    // console.log("get SO-CODE: ", this.soCode);
    // console.log("And the detail: ", this.detailSoCode);

    // console.log("lenght: ", this.detailSoCode.length);
    // this.hitungTotalBox();

    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    });  
    

    this.f1yangdipilih = this.navParams.get("f1pilihnya");
    console.log("ini f1nya",this.f1yangdipilih);
    
    this.f2yangdipilih = this.navParams.get("f2pilihnya");
    console.log("ini f2nya",this.f2yangdipilih);

    this.f3yangdipilih = this.navParams.get("f3pilihnya");
    console.log("ini f3nya",this.f3yangdipilih);

    this.f4yangdipilih = this.navParams.get("f4pilihnya");
    console.log("ini f4nya",this.f4yangdipilih);

    this.f5yangdipilih = this.navParams.get("f5pilihnya");
    console.log("ini f5nya",this.f5yangdipilih);

    this.f6yangdipilih = this.navParams.get("f6pilihnya");
    console.log("ini f6nya",this.f6yangdipilih);

    this.f7yangdipilih = this.navParams.get("f7pilihnya");
    // console.log("ini f7nya",this.f7yangdipilih);
    console.log("ini f7nya stringnya",JSON.stringify(this.f7yangdipilih));

    this.f8yangdipilih = this.navParams.get("f8pilihnya");
    console.log("ini f8nya",this.f8yangdipilih);

    this.f9yangdipilih = this.navParams.get("f9pilihnya");
    console.log("ini f9nya",this.f9yangdipilih);

    this.f10yangdipilih = this.navParams.get("f10pilihnya");
    console.log("ini f10nya",this.f10yangdipilih);

    this.f11yangdipilih = this.navParams.get("f11pilihnya");
    console.log("ini f11nya",this.f11yangdipilih);

    this.categoryPesanan = this.navParams.get("categoryPesanan");
    console.log("ini categoryPesanan", this.categoryPesanan);
    
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter DetailPesananPage');
  }

  
  // hitungTotalBox(){
    //   let y;
    //   console.log("hitungTotalBox()");
    //   this.totalBox = 0;
    //   for(y=0; y < this.detailSoCode.length; y++){
    //     console.log("jumlah: ", this.detailSoCode[y].jumlah);
    //     this.totalBox += this.detailSoCode[y].jumlah;
    //   }

    //   console.log("Total Box pesanan: ", this.totalBox);
  // }
}
