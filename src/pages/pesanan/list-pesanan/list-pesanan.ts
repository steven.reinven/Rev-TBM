import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, LoadingController } from 'ionic-angular';
import { DetailPesananPage } from "./detail-pesanan/detail-pesanan";
import { DataProvider } from "../../../providers/data/data";
import { LoginProvider } from '../../../providers/login/login';
import { Http } from '@angular/http';
import { PesananUserListProvider } from '../../../providers/pesanan-user-list/pesanan-user-list';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the ListPesananPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-list-pesanan',
  templateUrl: 'list-pesanan.html',
})
export class ListPesananPage {

  // flagPemesananMenu;
  // totalBox;
  // totalBoxPesananTerkirim= [];
  // totalBoxPesananDikirim= [];
  // totalBoxPesananDalamProses= [];
  // totalBoxPesananDitolak= [];
  platformStatus;

    //persiapan tembak api trus masuk database
      tampungiconi;
      tampungcolori;
      tampungnamai;

      tampungiyangdipilih;
      tampungiuntuktembak;

      datalogindaridatabase;
      tampungdatalogindaridatabase;
      USERNAME;
      TOKEN;
      
      datapesananlistdariapi;
      tampungdatapesananlistdariapi
    
      datapesananlistdaridatabase;
      tampungdatapesananlistdaridatabase;
      pesananlist=[];

    
    //persiapan tembak api trus masuk database

    //untuk load more
      page=0;
      jsondariapi;
      jsondaridatabase;
      jsonapidandatabase;
      tampungpageke=1;

      tampungsementara=[];
      tampungsementarake=0;
    
      jsontampungansementara=[];
      jsonnyak;
    
      x=0;
      y=0;
      z=0;
    //untuk load more

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public dataProvider: DataProvider,
      
      public databaseloginnya: LoginProvider,
      public databasepesananlistnya: PesananUserListProvider,
      private http:Http,
      private network: Network,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      public platform: Platform
    ) {
    // this.flagPemesananMenu = this.navParams.get("menuPesananName");
    // console.log("get pesananMenu: ", this.flagPemesananMenu);

    // console.log("provider data: ", this.dataProvider.pesananDalamProses);


    // this.hitungTotalBoxPesananDalamProses();
    // this.hitungTotalBoxPesananTerkirim();
    // this.hitungTotalBoxPesananDikirim();
    // this. hitungTotalBoxPesananDitolak();
    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    });  

    this.tampungiuntuktembak=0;
    
    this.tampungiconi = this.navParams.get("iconi");
    console.log("di list-pesanan.ts ini tampung iconi",this.tampungiconi);
    this.tampungcolori = this.navParams.get("colori");
    console.log("di list-pesanan.ts ini tampung colori",this.tampungcolori);
    this.tampungnamai = this.navParams.get("namai");
    console.log("di list-pesanan.ts ini tampung colori",this.tampungnamai);

    this.tampungiyangdipilih= this.navParams.get("pilihi");
    console.log("di list-pesanan.ts tampungiyangdipilih",this.tampungiyangdipilih);

    if(this.tampungiyangdipilih == 0){
      this.tampungiuntuktembak=1;
      console.log("di ist-pesanan.ts tampungiuntuktembak",this.tampungiuntuktembak);
    }else if(this.tampungiyangdipilih == 1){
      this.tampungiuntuktembak=2;
      console.log("di list-pesanan.ts tampungiuntuktembak",this.tampungiuntuktembak);
    }else if(this.tampungiyangdipilih == 2){
      this.tampungiuntuktembak=3;
      console.log("di list-pesanan.ts tampungiuntuktembak",this.tampungiuntuktembak);
    }else if(this.tampungiyangdipilih == 3){
      this.tampungiuntuktembak=4;
      console.log("di list-pesanan.ts tampungiuntuktembak",this.tampungiuntuktembak);
    }else{
      console.log("di list-pesanan.ts kelebihan 1 2 3 4 ?");
    }

      

    
  }

  ionViewDidLoad(id,jsonnya) {
    console.log('ionViewDidLoad ListPesananPage');
      
    if(
        this.network.type === 'none'
      ||this.network.type === 'unknown'){
      console.log("di list-pesanan.ts ini none atau unknown network");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.ambildatapesananlistdaridatabase(id,jsonnya);
    }else if(
        this.network.type === 'wifi' 
      ||this.network.type === 'cellular'
      ||this.network.type === '4g'
      ||this.network.type === '3g'
      ||this.network.type === '2g'
      ||this.network.type === 'ethernet'){
      console.log("di list-pesanan.ts ini connect");
      // let toast = this.toastCtrl.create({
      //   message: 'Terkoneksi',
      //   duration: 2000,
      //   position: 'top'
      // });
      // toast.present();
      this.ambiltokendaridatabase(id,jsonnya);
      
    }else{
      console.log("di list-pesanan.ts connect aneh");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.ambildatapesananlistdaridatabase(id,jsonnya);
    }
  }
  
  //sqlite
    ambiltokendaridatabase(id,jsonnya){
      console.log("di list-pesanan.ts ambiltokendaridatabase start");
      this.databaseloginnya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di list-pesanan.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        if(this.tampungdatalogindaridatabase[0].token != ""){
          this.USERNAME=this.tampungdatalogindaridatabase[0].username;
          this.TOKEN=this.tampungdatalogindaridatabase[0].token;
          console.log("di list-pesanan.ts ini username dari database",this.USERNAME);
          console.log("di list-pesanan.ts ini token dari database",this.TOKEN);

          this.ambildatapesananlistdariapi(id,jsonnya);
        }else{
          console.log("di list-pesanan.ts anda tidak memiliki token");

        }
      },(error) => {
          console.log("di list-pesanan.ts ERROR: showms_user", error);
      });
      console.log("di list-pesanan.ts ambiltokendaridatabase finnish");
    }

    ambildatapesananlistdariapi(id,jsonnya){
      console.log("di list-pesanan.ts ambildatapesananlistdariapi start");
      this.http.get('http://api.nex.web.id/pestisida/api.php?q='+this.tampungiuntuktembak+'&q2='+this.USERNAME+'&token='+this.TOKEN)
      .subscribe(res=>{

        this.datapesananlistdariapi=res.json();
        this.tampungdatapesananlistdariapi=this.datapesananlistdariapi;
        console.log("===api: ", this.tampungdatapesananlistdariapi);

        if(this.tampungdatapesananlistdariapi.length != 0){
          console.log("di list-pesanan.ts data api tidak kosong");
          this.databasepesananlistnya.putms_pesanan_list(
            id=this.tampungiuntuktembak,
            jsonnya=JSON.stringify(this.tampungdatapesananlistdariapi)
            
          ).then((result) => {
            console.log("di list-pesanan.ts putms_pesanan finnish");
            this.ambildatapesananlistdaridatabase(id,jsonnya);
            // this.untukloadmore(id,jsonnya);
            
          },(error) => {
            console.log("di list-pesanan.ts ERROR: putms_pesanan", error);       
            let toast = this.toastCtrl.create({
              message: 'Database Lokal Bermasalah',
              duration: 2000,
              position: 'top'
            });
            toast.present();     
          });
  
        }else{
          console.log("di list-pesanan.ts data api kosong"); 
          // this.ambildatapesananlistdaridatabase(id,jsonnya);
          // console.log("Load dari database lokal");
          let toast = this.toastCtrl.create({
            message: 'Data tidak tersedia',
            duration: 2000,
            position: 'top'
          });
          toast.present();

        }
       
      },(err)=>{
        console.log("di list-pesanan.ts error httpgetpesanan",err);
        // this.ambildatapesananlistdaridatabase(id,jsonnya);
        let toast = this.toastCtrl.create({
          message: 'Cek kembali koneksi internet anda',
          duration: 2000,
          position: 'top'
        });
        toast.present();
      });
      console.log("di list-pesanan.ts ambildatapesananlistdariapi finnish");
    }

    ambildatapesananlistdaridatabase(id,jsonnya){
      console.log("di list-pesanan.ts ambildatapesananlistdaridatabase start");
      this.databasepesananlistnya.showms_pesanan_list_list(id=this.tampungiuntuktembak).then((result) => {
        this.datapesananlistdaridatabase = <Array<Object>> result;
        console.log("di list-pesanan.ts ms_pesanan has been Loaded");
        this.tampungdatapesananlistdaridatabase=this.datapesananlistdaridatabase;

        if(this.tampungdatapesananlistdaridatabase != ""){
          this.tampungsementara[0]=JSON.parse (this.tampungdatapesananlistdaridatabase[0].jsonnya);
          if(this.pesananlist.length != 0){
            console.log("di list-pesanan.ts kog ada isi");
          }else{
            // this.pesananlist.push(this.tampungsementara[0]); aneh
            this.pesananlist=this.tampungsementara;
            console.log("di list-pesanan.ts ini lengthnya pesanan",this.pesananlist.length);
          }
         
         
        }else{
          console.log("di list-pesanan.ts showms_pesanan kosong");
          // this.ambiltokendaridatabase(id,jsonnya);
          let toast = this.toastCtrl.create({
            message: 'Database Lokal Bermasalah',
            duration: 2000,
            position: 'top'
          });
          toast.present();
         
        }
      },(error) => {
          console.log("di list-pesanan.ts ERROR: showms_pesanan", error);
          let toast = this.toastCtrl.create({
            message: 'Database Lokal Bermasalah',
            duration: 2000,
            position: 'top'
          });
          toast.present();   
      });
      console.log("di list-pesanan.ts ambildatapesananlistdaridatabase finnish");
    }
  //sqlite


  doInfinite(infiniteScroll,id,jsonnya) {
    console.log('di list-pesanan.ts Begin async operation doinfinite');
    setTimeout(() => {
      this.validasipageke(id,jsonnya);
      console.log('di list-pesanan.ts Async operation has ended doinfinite');
      infiniteScroll.complete();
    }, 500);
  }

  // doLoadmore(id,jsonnya){
  //   let loading = this.loadingCtrl.create({
  //     content: 'Please wait...'
  //   });
  
  //   loading.present();
  //   this.untukloadmore(id,jsonnya);
  
  //   setTimeout(() => {
  //     loading.dismiss();
  //   }, 1000);
  // }

  validasipageke(id,jsonnya){
    console.log("di list-pesanan.ts validasipageke start");
    this.page++;
    if(this.tampungpageke != 0){
      console.log("di list-pesanan.ts tampungpageke tidak 0");
      console.log("di list-pesanan.ts error di page ke",this.tampungpageke);
      // let loading = this.loadingCtrl.create({
      //       content: 'Please wait...'
      //     });
      //     loading.present();
      //     setTimeout(() => {
      //       loading.dismiss();
      //       this.untukloadmore(id,jsonnya);
      //     }, 1000);

      this.untukloadmore(id,jsonnya);
    }else{
      console.log("di list-pesanan.ts tampungpageke 0");
      this.tampungpageke=this.page;
      console.log("di list-pesanan.ts 0 jadi ",this.tampungpageke);
    }
    console.log("di list-pesanan.ts validasipageke finnish");
  }

  flagLoadData: boolean = true;
  untukloadmore(id,jsonnya){
    console.log("di list-pesanan.ts untukloadmore start");
    if(
      this.network.type === 'none'
      ||this.network.type === 'unknown'){
      console.log("di list-pesanan.ts ini none atau unknown network");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.ambildatapesananlistdaridatabase(id,jsonnya);
    }else if(
      this.network.type === 'wifi' 
      ||this.network.type === 'cellular'
      ||this.network.type === '4g'
      ||this.network.type === '3g'
      ||this.network.type === '2g'
      ||this.network.type === 'ethernet'){
        console.log("di list-pesanan.ts ini connect");
        // let toast = this.toastCtrl.create({
        //   message: 'Load More',
        //   duration: 2000,
        //   position: 'top'
        // });
        // toast.present();
        this.pesananlist=[];
          this.http.get('http://api.nex.web.id/pestisida/api.php?q='+this.tampungiuntuktembak+'&q2='+this.USERNAME+'&p='+this.tampungpageke+'&token='+this.TOKEN)
          .subscribe(res=>{
            this.datapesananlistdariapi=res.json();
            this.tampungdatapesananlistdariapi=this.datapesananlistdariapi;
    
            this.databasepesananlistnya.showms_pesanan_list_list(id=this.tampungiuntuktembak).then((result) => {
              this.datapesananlistdaridatabase = <Array<Object>> result;
              console.log("di list-pesanan.ts ms_pesanan_list has been Loaded");
              this.tampungdatapesananlistdaridatabase=this.datapesananlistdaridatabase;
              //database
              if(this.tampungdatapesananlistdaridatabase != ""){
                this.jsondaridatabase=JSON.parse(this.tampungdatapesananlistdaridatabase[0].jsonnya);
                this.tampungsementara[0]=this.jsondaridatabase;
                console.log("di list-pesanan.ts ms_pesanan_list tidak kosong databasenya");
    
                //api
                if(this.tampungdatapesananlistdariapi.length != 0){
                  this.tampungpageke++;
                  this.tampungsementarake++;
                  console.log("di list-pesanan.ts data api tidak kosong");
                  this.jsondariapi=this.tampungdatapesananlistdariapi;
                  this.tampungsementara[this.tampungsementarake]=this.jsondariapi;
                  this.jsonnyak=[];

                  console.log("ini length tampungsementara",this.tampungsementara.length);

                  console.log("tampungsementara ke0",this.tampungsementara[0]);
                  for(this.x=0; this.x<this.tampungsementara.length; this.x++){
                    console.log("tampungsementara ke"+this.x,this.tampungsementara[this.x]);
                    // this.jsonnyak=this.tampungsementara[0].concat(this.tampungsementara[this.x]);
                    // this.pesananlist.push(this.tampungsementara[this.x]);
                    // this.pesananlist=this.tampungsementara[this.x];
                    this.jsonnyak=this.tampungsementara;
                  }
                  // this.jsonnyak=this.pesananlist;
                  this.pesananlist=this.jsonnyak;
                  this.flagLoadData = true; //trigger tombol load
                  // this.pesananlist.push(this.jsonnyak);
                  // this.view=true;
                  
                }else{
                  console.log("di list-pesanan.ts data api kosong");
                  let toast = this.toastCtrl.create({
                    message: 'Semua pesanan telah ditampilkan',
                    duration: 2000,
                    position: 'top'
                  });
                  toast.present();
                  this.pesananlist=this.tampungsementara;
                  this.flagLoadData = false; //trigger tombol load
                  console.log("===else flagLoadData: ", this.flagLoadData);
                }
    
              }else{
                console.log("di list-pesanan.ts showms_pesanan kosong");
                this.ambiltokendaridatabase(id,jsonnya);
              }
      
            },(error) => {
                console.log("di list-pesanan.ts ERROR: showms_pesanan", error);
            });
    
          },(err)=>{
            console.log("di list-pesanan.ts error httpgetpesanan loadmore",err);
            let toast = this.toastCtrl.create({
              message: 'Server Down',
              duration: 2000,
              position: 'top'
            });
            toast.present();
            this.tampungpageke=this.page;
            this.pesananlist=this.jsonnyak;
            
            console.log("di list-pesanan.ts tampungpageke yang macet",this.tampungpageke);
          });

    }else{
      console.log("di list-pesanan.ts connect aneh");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.ambildatapesananlistdaridatabase(id,jsonnya);
    }
    console.log("di list-pesanan.ts untukloadmore finnish");
  }



  goToDetailPesanan(y,z){
    this.navCtrl.push(DetailPesananPage, {
      f1pilihnya: z.f1,
      f2pilihnya: z.f2,
      f3pilihnya: z.f3,
      f4pilihnya: z.f4,
      f5pilihnya: z.f5,
      f6pilihnya: z.f6,
      f7pilihnya: z.f7,
      f8pilihnya: z.f8,
      f9pilihnya: z.f9,
      f10pilihnya: z.f10,
      f11pilihnya: z.f11,
      categoryPesanan: this.tampungiuntuktembak
    });
    
  }


  //ini old


    // hitungTotalBoxPesananDalamProses(){
    //   let x;
    //   let y;
    //   let temp = 0;
    //   for(x = 0; x < this.dataProvider.pesananDalamProses[0].SO.length; x ++){
      
    //     for(y=0; y < this.dataProvider.pesananDalamProses[0].SO[x].detail.length; y++){
    //       console.log("jumlah: ", this.dataProvider.pesananDalamProses[0].SO[x].detail[y].jumlah);
    //       temp += this.dataProvider.pesananDalamProses[0].SO[x].detail[y].jumlah;
    //     }
    //     console.log("temp2: ", temp);
    //     this.totalBoxPesananDalamProses[x] = temp;
    //     temp = 0;
    //     console.log("isi temp[]: ", this.totalBoxPesananDalamProses);
    //   }
    // }

    // hitungTotalBoxPesananTerkirim(){
    //   let x;
    //   let y;
    //   let temp = 0;
    //   for(x = 0; x < this.dataProvider.pesananTerkirim[0].SO.length; x ++){
      
    //     for(y=0; y < this.dataProvider.pesananTerkirim[0].SO[x].detail.length; y++){
    //       console.log("jumlah: ", this.dataProvider.pesananTerkirim[0].SO[x].detail[y].jumlah);
    //       temp += this.dataProvider.pesananTerkirim[0].SO[x].detail[y].jumlah;
    //     }
    //     console.log("temp2: ", temp);
    //     this.totalBoxPesananTerkirim[x] = temp;
    //     temp = 0;
    //     console.log("isi temp[]: ", this.totalBoxPesananTerkirim);
    //   }
    // }

    // hitungTotalBoxPesananDikirim(){
    //   let x;
    //   let y;
    //   let temp = 0;
    //   for(x = 0; x < this.dataProvider.pesananDikirim[0].SO.length; x ++){
      
    //     for(y=0; y < this.dataProvider.pesananDikirim[0].SO[x].detail.length; y++){
    //       console.log("jumlah: ", this.dataProvider.pesananDikirim[0].SO[x].detail[y].jumlah);
    //       temp += this.dataProvider.pesananDikirim[0].SO[x].detail[y].jumlah;
    //     }
    //     console.log("temp2: ", temp);
    //     this.totalBoxPesananDikirim[x] = temp;
    //     temp = 0;
    //     console.log("isi temp[]: ", this.totalBoxPesananDikirim);
    //   }
    // }

    // hitungTotalBoxPesananDitolak(){
    //   let x;
    //   let y;
    //   let temp = 0;
    //   for(x = 0; x < this.dataProvider.pesananDitolak[0].SO.length; x ++){
      
    //     for(y=0; y < this.dataProvider.pesananDitolak[0].SO[x].detail.length; y++){
    //       console.log("jumlah: ", this.dataProvider.pesananDitolak[0].SO[x].detail[y].jumlah);
    //       temp += this.dataProvider.pesananDitolak[0].SO[x].detail[y].jumlah;
    //     }
    //     console.log("temp2: ", temp);
    //     this.totalBoxPesananDitolak[x] = temp;
    //     temp = 0;
    //     console.log("isi temp[]: ", this.totalBoxPesananDitolak);
    //   }
    // }
  //ini old


}
