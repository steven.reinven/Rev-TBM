import { Component} from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TabsPage } from "../tabs/tabs";
import { Http } from '@angular/http';
import { LoginProvider } from '../../providers/login/login';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  notes: string=" ";
  note: boolean=false;
  flag: boolean=false;
  // flag2: boolean=false;


  datalogindariapi;
  tampungdatalogindariapi;
  tokendariapi;
  
  USERNAME;
  PASSWORD;
  TITLEID;
  TOKEN;
  datalogindaridatabase;
  tampungdatalogindaridatabase;
  tokendaridatabase;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public builder: FormBuilder,
    private http:Http,
    private network: Network,
    public toastCtrl: ToastController,
    public databasenya: LoginProvider,
    public loadingCtrl:LoadingController) {
  
      this.loginForm = builder.group({
        'username':['', Validators.required],
        'password': ['', Validators.required]
      })

      
      
  }
  //=========END OF constructor================//

  ionViewDidEnter(userid,username,titleid,password,custid,namapt,agronomis,fa,token,updateby,updatedate){
    console.log("di login.ts ionViewDidEnter");
    if(
      this.network.type === 'none'
    ||this.network.type === 'unknown'){
    console.log("di login.ts ini none atau unknown network");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }else if(
        this.network.type === 'wifi' 
      ||this.network.type === 'cellular'
      ||this.network.type === '4g'
      ||this.network.type === '3g'
      ||this.network.type === '2g'
      ||this.network.type === 'ethernet'){
      console.log("di login.ts ini connect");
    }else{
      console.log("di login.ts connect aneh");
      let toast = this.toastCtrl.create({
        message: 'Tidak Terkoneksi',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }
  }
  
  //sqlite
    validatorloginapi(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate){
      if(
        this.network.type === 'none'
      ||this.network.type === 'unknown'){
      console.log("di login.ts ini none atau unknown network");
        let toast = this.toastCtrl.create({
          message: 'Tidak Terkoneksi',
          duration: 2000,
          position: 'top'
        });
        toast.present();
      }else if(
          this.network.type === 'wifi' 
        ||this.network.type === 'cellular'
        ||this.network.type === '4g'
        ||this.network.type === '3g'
        ||this.network.type === '2g'
        ||this.network.type === 'ethernet'){
        console.log("di login.ts ini connect");
        console.log("di login.ts ini validatorloginapi start");
        if(this.loginForm.controls.username.value != "" && this.loginForm.controls.password.value != ""){
          this.http.get('http://api.nex.web.id/pestisida/api.php?q=99&q2='+this.loginForm.controls.username.value+'|'+this.loginForm.controls.password.value+' ')
          .subscribe(res=>{
      
            this.datalogindariapi=res.json();
            this.tampungdatalogindariapi=this.datalogindariapi;
            console.log("anc",this.tampungdatalogindariapi);

            if(this.tampungdatalogindariapi[0].f0 == 1){
              this.databasenya.putms_user(
                userid=0,
                username=this.tampungdatalogindariapi[0].f1,
                titleid=this.tampungdatalogindariapi[0].f2,
                password=this.loginForm.controls.password.value,
                custid=this.tampungdatalogindariapi[0].f5,
                namapt=this.tampungdatalogindariapi[0].f4,
                agronomis=this.tampungdatalogindariapi[0].f6,
                fa=this.tampungdatalogindariapi[0].f7,
                sejak=this.tampungdatalogindariapi[0].f8,
                pemakai=this.tampungdatalogindariapi[0].f9,
                jabatan=this.tampungdatalogindariapi[0].f10,
                lokasi=this.tampungdatalogindariapi[0].f11,
                distributor=this.tampungdatalogindariapi[0].f13,
                tokenExp=this.tampungdatalogindariapi[0].f12,
                token=this.tampungdatalogindariapi[0].f3,
                updateby,
                updatedate).then((result) => {
                this.fetchdaridatabase(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate);
              },(error) => {
                console.log("di login.ts ERROR: putms_user", error);
              });
            }else{
              let toast = this.toastCtrl.create({
                message: this.tampungdatalogindariapi[0].f0,
                duration: 2000,
                position: 'middle'
              });
              toast.present();
              this.note=true;
              this.notes="* Login Gagal";
            }
          
            
      
          },(err)=>{
            this.note=true;
            this.notes="* Login Gagal";
            console.log("di login.ts error Login",err);
          });
      
        }else{
          this.note=true;
          this.notes="*username dan password kosong";
        }
        console.log("di login.ts ini validatorloginapi finnish");  
      }else{
        console.log("di login.ts connect aneh");
        let toast = this.toastCtrl.create({
          message: 'Tidak Terkoneksi',
          duration: 2000,
          position: 'top'
        });
        toast.present();
      }
      
     

      
    }

    fetchdaridatabase(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate){
      console.log("di login.ts ini fetchdaridatabase");
      this.databasenya.showms_user().then((result) => {
        this.datalogindaridatabase = <Array<Object>> result;
        console.log("di login.ts ms_user has been Loaded");
        this.tampungdatalogindaridatabase=this.datalogindaridatabase;

        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.PASSWORD=this.tampungdatalogindaridatabase[0].password;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;
        this.TOKEN=this.tampungdatalogindaridatabase[0].token;

        if(this.TITLEID != ""){
          this.validatorlogin();
          console.log("di login.ts validatorlogin finish");
        }else{
          this.validatorloginapi(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate);
          console.log("di login.ts validatorloginapi finish");
        }
        
      },(error) => {
          console.log("di login.ts ERROR: showms_user", error);
      });
    }

    validatorlogin(){
      console.log("di login.ts ini validatorlogin start");
      // if(this.loginForm.controls.username.value != "" && this.loginForm.controls.password.value != ""){

          if( this.TITLEID == "4" || this.TITLEID == "5" || this.TITLEID == "8" || this.TITLEID == "9"){
            if(this.loginForm.controls.password.value != ""){
              if(this.loginForm.controls.password.value != this.PASSWORD){
                this.note=true;
                this.flag=true;
                this.notes="*password tidak benar";
                this.loginForm.controls.password.reset();
              }else{
                this.databasenya.flag="user@gmail.com";
                console.log("di login.ts cocok titleid untuk user",this.TITLEID);
                this.navCtrl.setRoot(TabsPage);
                
              }
            }else{
              this.notes="*password kosong";
            }
          }else if(this.TITLEID == "2"){
            if(this.loginForm.controls.password.value != ""){
              if(this.loginForm.controls.password.value != this.PASSWORD){
                this.note=true;
                this.flag=true;
                this.notes="*password tidak benar";
                this.loginForm.controls.password.reset();
              }else{
                this.databasenya.flag="manajemen@gmail.com";
                // this.databasenya.flag2="distributor@gmail.com";
                this.databasenya.kodeapi=15;
                this.databasenya.kodeapiberandamanajemen=30;
                console.log("di login.ts cocok titleid untuk manajemen",this.TITLEID);
                this.navCtrl.setRoot(TabsPage);
              }
            }else{
              this.note=true;
              this.flag=true;
              this.notes="*password kosong";
            }
          }else if(this.TITLEID == "3" || this.TITLEID == "6" || this.TITLEID == "7"){
            if(this.loginForm.controls.password.value != ""){
              if(this.loginForm.controls.password.value != this.PASSWORD){
                this.note=true;
                this.flag=true;
                // this.flag2=true;
                this.notes="*password tidak benar";
                this.loginForm.controls.password.reset();
              }else{
                if(this.TITLEID == "3"){
                  this.databasenya.kodeapi=31;
                  this.databasenya.kodeapiberandamanajemen=310;
                  this.databasenya.flag="distributor@gmail.com";
                  this.navCtrl.setRoot(TabsPage);
                  console.log("di login.ts cocok titleid 3 untuk Distributor",this.TITLEID);
                }else if(this.TITLEID == "6"){
                  this.databasenya.kodeapi=32;
                  this.databasenya.kodeapiberandamanajemen=320;
                  this.databasenya.flag="agronomis&FA(6)@gmail.com";
                  this.navCtrl.setRoot(TabsPage);
                  console.log("di login.ts cocok titleid 6 untuk Distributor",this.TITLEID);
                  console.log("===flag(LOGIN.TS): ", this.databasenya.flag);
                }else if(this.TITLEID == "7"){
                  this.databasenya.kodeapi=33;
                  this.databasenya.kodeapiberandamanajemen=330;
                  this.databasenya.flag="agronomis&FA(7)@gmail.com";
                  this.navCtrl.setRoot(TabsPage);
                  console.log("di login.ts cocok titleid 7 untuk Distributor",this.TITLEID);
                  console.log("===flag(LOGIN.TS): ", this.databasenya.flag);
                }else{
                  console.log("Belum terdaftar");
                  let toast = this.toastCtrl.create({
                    message: 'Title ID tidak cocok',
                    duration: 2000,
                    position: 'top'
                  });
                  toast.present();
                }
                
               
              }
            }else{
              this.note=true;
              this.flag=true;
              this.notes="*password kosong";
            }
          }else{
            this.note=true;
            this.flag=true;
            this.notes="*username tidak benar";
            this.loginForm.reset();

          }
        
    }

  //sqlite

  doSkip(){
    this.databasenya.flag = "skip";
    console.log("di login.ts doSkip flag provider: ", this.databasenya.flag);
    this.navCtrl.setRoot(TabsPage);
  }

}
