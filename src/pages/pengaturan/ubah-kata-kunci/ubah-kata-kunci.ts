import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, ToastController, App, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginProvider } from '../../../providers/login/login';
import { Http } from '@angular/http';
import { LoginPage } from '../../login/login';

/**
 * Generated class for the UbahKataKunciPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-ubah-kata-kunci',
  templateUrl: 'ubah-kata-kunci.html',
})
export class UbahKataKunciPage {

  //sqlite
  datalogindaridatabase;
  tampungdatalogindaridatabase;

  USERNAME;
  TOKEN;
  PASSWORDDARIDATABASE;

  //sqlite

  //api
  datalogindariapi;
  tampungdatalogindariapi;
  //api

  platformStatus;
  changePassForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public platform: Platform, 
    public databasenya: LoginProvider,  
    public builder: FormBuilder,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private http:Http,
    public app: App,
    public loadingCtrl:LoadingController,
  ) {

    this.changePassForm = this.builder.group({
      'oldPassword': ['', Validators.required],
      'newPassword': ['', Validators.required],
      'confirmNewPassword': ['', Validators.required]
    });


    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UbahKataKunciPage');
  }

  //sqlite
   ambilpasswordlamadaridatabase(changePassForm,userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate){
    this.databasenya.showms_user().then((result) => {
      this.datalogindaridatabase = <Array<Object>> result;
      console.log("ms_user has been Loaded");
      this.tampungdatalogindaridatabase=this.datalogindaridatabase;

      this.USERNAME=this.tampungdatalogindaridatabase[0].username;
      this.TOKEN=this.tampungdatalogindaridatabase[0].token;
      this.PASSWORDDARIDATABASE=this.tampungdatalogindaridatabase[0].password;
     
      console.log("ini username dari database",this.USERNAME);
      console.log("ini token dari database",this.TOKEN);
      console.log("ini password dari database",this.PASSWORDDARIDATABASE);
     
      this.ubahpassworddiapi(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate);
            
    },(error) => {
        console.log("ERROR: showms_user", error);
    });
   }
  //sqlite

  //api
  ubahpassworddiapi(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate){
    if(this.changePassForm.controls.oldPassword.value != this.PASSWORDDARIDATABASE){
      this.validator = "*password yang anda masukkan salah";
      this.changePassForm.controls.oldPassword.reset();
      this.validatorStatus = 1;
      this.colorPassword = "danger";
    }else{
      let alert = this.alertCtrl.create();
      alert.setTitle('Apakah anda yakin ingin mengganti password?');
      alert.addButton({
        text: "Ok",
        handler: data=>{

          let body  = ''+this.USERNAME+'|'+this.changePassForm.controls.confirmNewPassword.value+'&token='+this.TOKEN;
          console.log("intip",'http://api.nex.web.id/pestisida/api.php?q=98&q2='+body);
          this.http.get('http://api.nex.web.id/pestisida/api.php?q=98&q2='+body).subscribe(res=>{
            console.log("respon status dari api",res);
            console.log("respon dari api",JSON.stringify(res.json()));    

            let loading = this.loadingCtrl.create({
              content: 'Silahkan tunggu..'
            });
            loading.present();

            
          
            setTimeout(() => {
              loading.dismiss();
              this.databasenya.putms_user(userid=0,username=null,titleid=null,password=null,custid=null,namapt=null,agronomis=null,fa=null,sejak=null,pemakai=null,jabatan=null,lokasi=null,distributor=null,tokenExp=null,token=null,updateby=null,updatedate=null).then((result) => {
                this.app.getRootNav().setRoot(LoginPage);
                this.databasenya.flag="";
                console.log("putms_user finnish");

                let toast = this.toastCtrl.create({
                  message: 'Silahkan melakukan Login Ulang',
                  duration: 4000,
                  position: 'middle'
                });
                toast.present();

                },(error) => {
                  console.log("ERROR: putms_user", error);
                });
            }, 5000);
     

          },(err)=>{
              console.log("ERROR respon dari api",JSON.stringify(err.json()));
              console.log("ERROR respon status dari api",err);
              let toast = this.toastCtrl.create({
                message: 'Server Down',
                duration: 2000,
                position: 'middle'
              });
              toast.present();
          });
  
        }
      });
      alert.addButton('Batal');
      alert.present();
    }
    
  }
  //api

//============================================CHANGE PASSWORD==================================================//
  validatorStatus: number=0;
  validator;
  colorPassword: string="dark";
  colorNewPassword: string="dark";
  colorConfirmNewPassword: string="dark";
  // changePassword(userid,username,titleid,password,startdate,status,notes,token,updateby,updatedate){
  //   this.ambilpasswordlamadaridatabase();

  //   if(this.changePassForm.controls.oldPassword.value != ""){
  //     if(this.changePassForm.controls.oldPassword.value != this.PASSWORDLAMA){
  //       this.validator = "*password yang anda masukkan salah";
  //       this.changePassForm.controls.oldPassword.reset();
  //       this.validatorStatus = 1;
  //       this.colorPassword = "danger";
  //     }else if(this.changePassForm.controls.oldPassword.value != this.changePassForm.controls.newPassword.value){
  //       this.validator = "*sandi tersebut tidak cocok";
  //       this.validatorStatus = 4;
  //     }else{
        
  //     }
  //   }else{

  //   }
  isDisabled: boolean= true;
  changePassword(userid,username,titleid,password,startdate,status,notes,token,updateby,updatedate){
    
      //ini bukan sqlite
        console.log("password input: ", this.changePassForm.controls.oldPassword.value);
        console.log("new password: ", this.changePassForm.controls.newPassword.value);
        console.log("confirm new password: ", this.changePassForm.controls.confirmNewPassword.value);
        if(this.changePassForm.controls.oldPassword.value != this.databasenya.password){
          this.validator = "*password yang anda masukkan salah";
          this.changePassForm.controls.oldPassword.reset();
          this.validatorStatus = 1;
          this.colorPassword = "danger";
        }
        else {
          this.colorPassword = "greenValidator";
          if(this.changePassForm.controls.newPassword.value == ""){
            this.validator = "*password baru harus di isi";
            this.changePassForm.controls.newPassword.reset();
            this.validatorStatus = 2;
            this.colorNewPassword = "danger";
          }
          else{
            this.colorNewPassword = "buttonPesan";
            if(this.changePassForm.controls.confirmNewPassword.value == ""){
              this.validator = "*konfimarsi password baru harus di isi";
              this.changePassForm.controls.confirmNewPassword.reset();
              this.validatorStatus = 3;
              this.colorConfirmNewPassword = "danger";
            }
            else{
              this.colorConfirmNewPassword = "buttonPesan";
              if(this.changePassForm.controls.newPassword.value != this.changePassForm.controls.confirmNewPassword.value){
                this.validator = "*sandi tersebut tidak cocok";
                this.validatorStatus = 4;
              }
              else{
                this.colorNewPassword = "greenValidator";
                this.colorConfirmNewPassword = "greenValidator";
                this.validatorStatus = 0;
                
                

                let alert = this.alertCtrl.create();
                alert.setTitle('Apakah anda yakin ingin mengganti password?');
                alert.addButton({
                  text: "Ok",
                  handler: data=>{
                    this.databasenya.password = this.changePassForm.controls.newPassword.value;
                    this.navCtrl.pop();
                  }
                });
                alert.addButton('Batal');
                alert.present();
              }
            }
          }
          
        }
      //ini bukan sqlite
      
  }



//==========================================================================================================================//

}
