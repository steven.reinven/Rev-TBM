import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App, Platform, ModalController } from 'ionic-angular';
import { UbahKataKunciPage } from "./ubah-kata-kunci/ubah-kata-kunci";
import { LoginPage } from "../login/login";
import { LoginProvider } from '../../providers/login/login';
import { LihatProfilePage } from './lihat-profile/lihat-profile';


/**
 * Generated class for the PengaturanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-pengaturan',
  templateUrl: 'pengaturan.html',
})
export class PengaturanPage {

  platformStatus;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public app: App,
    public databasenya: LoginProvider,
    public platform: Platform,
  ) {

    
    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    });  

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PengaturanPage');
  }

  goToLihatProfile(){
    // let modal = this.modalCtrl.create(LihatProfilePage);
    // modal.present();
    this.navCtrl.push(LihatProfilePage);
  }

  goToUbahPengaturan(){
    this.navCtrl.push(UbahKataKunciPage);
  }

  promptLogout(userid,username,titleid,password,custid,namapt,agronomis,fa,sejak,pemakai,jabatan,lokasi,distributor,tokenExp,token,updateby,updatedate){
    let prompt = this.alertCtrl.create({
      title: 'Keluar dari Akun',
      message: 'Anda yakin ingin keluar dari Akun?',

      buttons:[
        {
          text: 'Ya',
          
          handler: data=>{

            //untuk logout jadi semua data user di null tanpa harus hapus ms_user
            this.databasenya.putms_user(userid=0,username=null,titleid=null,password=null,custid=null,namapt=null,agronomis=null,fa=null,sejak=null,pemakai=null,jabatan=null,lokasi=null,distributor=null,tokenExp=null,token=null,updateby=null,updatedate=null).then((result) => {

              this.databasenya.flag="";
              this.app.getRootNav().setRoot(LoginPage);
              
            },(error) => {
              console.log("ERROR: putms_user", error);
            });

             
          }
        },
        {
          text: 'Tidak'
        }

      ]
    });
    prompt.present();
  }

  goToLoginPage(){
    this.app.getRootNav().setRoot(LoginPage);
  }



}
