import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { LoginProvider } from '../../../providers/login/login';

/**
 * Generated class for the LihatProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-lihat-profile',
  templateUrl: 'lihat-profile.html',
})
export class LihatProfilePage {

  datalogindaridatabase;
  tampungdatalogindaridatabase

  USERID;
  USERNAME;
  TITLEID;
  CUSTID;
  NAMAPT;
  AGRONOMIS;
  FA;
  SEJAK;
  PEMAKAI;
  JABATAN;
  LOKASI;
  TOKEN;
  DISTRIBUTOR;
  TOKENEXP;

  platformStatus;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public platform: Platform,
    public databaselogin:LoginProvider) {
  
    platform.ready().then(()=>{
      if(this.platform.is('ios')){
        console.log("ios ready");
        this.platformStatus = "ios";
      }
      if(this.platform.is('android')){
        console.log("android ready");
        this.platformStatus = "android";
      }
    }); 
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LihatProfilePage');
  
    this.databaselogin.showms_user().then((result) => {
      this.datalogindaridatabase = <Array<Object>> result;
      console.log("di lihat-profile.ts ms_user has been Loaded", this.datalogindaridatabase);
      this.tampungdatalogindaridatabase=this.datalogindaridatabase;

      if(this.tampungdatalogindaridatabase[0].token != ""){
        this.USERID=this.tampungdatalogindaridatabase[0].userid;
        this.USERNAME=this.tampungdatalogindaridatabase[0].username;
        this.TITLEID=this.tampungdatalogindaridatabase[0].titleid;
        this.CUSTID=this.tampungdatalogindaridatabase[0].custid;
        this.NAMAPT=this.tampungdatalogindaridatabase[0].namapt;
        this.AGRONOMIS=this.tampungdatalogindaridatabase[0].agronomis;
        this.FA=this.tampungdatalogindaridatabase[0].fa;
        this.SEJAK=this.tampungdatalogindaridatabase[0].sejak;
        this.PEMAKAI=this.tampungdatalogindaridatabase[0].pemakai;
        this.JABATAN=this.tampungdatalogindaridatabase[0].jabatan;
        this.LOKASI=this.tampungdatalogindaridatabase[0].lokasi;
        this.DISTRIBUTOR=this.tampungdatalogindaridatabase[0].distributor;
        this.TOKENEXP=this.tampungdatalogindaridatabase[0].tokenExp;
        this.TOKEN=this.tampungdatalogindaridatabase[0].token;
        
        console.log("===distributor: ", this.DISTRIBUTOR);
        console.log("===JABATAN: ", this.JABATAN);
        console.log("===LOKASI: ", this.LOKASI);
      }else{
        console.log("di lihat-profile.ts anda tidak memiliki token");

      }
    },(error) => {
        console.log("di lihat-profile.ts ERROR: showms_user", error);
    });

  }



}
